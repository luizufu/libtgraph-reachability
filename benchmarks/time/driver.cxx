#include <array>

#include <libgraph-structures/adj-matrix.hxx>
#include <libtgraph-reachability/ttc.hxx>
#include <libtgraph-reachability/rttree.hxx>

#include <iomanip>
#include <iostream>
#include <random>
#include <thread>
#include <tuple>
#include <chrono>

using namespace tgraph_reachability;
using namespace graph_structures;

using icontact = std::tuple<uint32_t, uint32_t, interval>;

auto make_emeg(uint32_t n, uint32_t t, float bp, float dp)
    -> std::vector<icontact>;
auto make_query(uint32_t n, uint32_t tau) -> icontact;
auto generator() -> std::mt19937&;

void dont_optimize(auto&& x)
{
    static auto ttid = std::this_thread::get_id();
    if(ttid == std::thread::id())
    {
        const auto* p = &x;
        putchar(*reinterpret_cast<const char*>(p));

        std::abort();
    }
};

auto main() -> int
{
    std::cout << "contacts;delta;add_contact;can_reach;reconstruct_journey"
              << std::endl;

    uint32_t n = 512;
    uint32_t tau = 512;
    std::array<uint32_t, 3> deltas = {1, 64, 128};
    uint32_t n_queries = 100;

    auto now = []()
    {
        return std::chrono::high_resolution_clock::now();
    };

    auto diffnow = [&now](const auto& start)
    {
        std::chrono::duration<double, std::milli> diff = now() - start;
        return diff.count();
    };

    using graph_structures::adj_matrix;
    using tgraph_reachability::ttc;

    auto icontacts = make_emeg(n, tau, .1F, .3F);
    std::vector<contact> contacts;
    for(const auto& [u, v, interv]: icontacts)
    {
        for(uint32_t t = interv.left; t < interv.right; ++t)
        {
            contacts.push_back({u, v, t});
        }
    }
    std::shuffle(contacts.begin(), contacts.end(), generator());

    for(uint32_t delta: deltas)
    {
        ttc<adj_matrix, rttree> ttc(n, tau, delta);

        uint32_t c = 0;
        double res1 = 0;
        double res2 = 0;
        double res3 = 0;
        for(auto contact: contacts)
        {
            auto start = now();
            ttc.add_contact(contact);
            res1 += diffnow(start);
            c++;

            start = now();
            for(uint32_t i = 0; i < n_queries; ++i)
            {
                auto [u, v, interv] = make_query(n, tau);
                dont_optimize(ttc.can_reach(u, v, interv));
            }
            res2 += diffnow(start) / n_queries;

            start = now();
            for(uint32_t i = 0; i < n_queries; ++i)
            {
                auto [u, v, interv] = make_query(n, tau);
                dont_optimize(ttc.reconstruct_journey(u, v, interv));
            }
            res3 += diffnow(start) / n_queries;

            if(c % 1000 == 0)
            {
                std::cout << (c - 500) << ";" << delta << ";" << (res1 / 1000)
                          << ";" << (res2 / 1000) << ";" << (res3 / 1000)
                          << std::endl;

                res1 = 0;
                res2 = 0;
                res3 = 0;
            }
        }
    }
}

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r()};
    static std::mt19937 gen(seed);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

auto make_emeg(uint32_t n, uint32_t t, float bp, float dp)
    -> std::vector<icontact>
{
    std::vector<icontact> icontacts;

    uint32_t nil = std::numeric_limits<uint32_t>::max();
    std::vector<uint32_t> g(n * n, nil);

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = u + 1; v < n; ++v)
        {
            if(next_probability() < bp / (bp + dp))
            {
                g[u * n + v] = 0;
            }
        }
    }

    for(uint32_t t1 = 1; t1 < t; ++t1)
    {
        for(uint32_t u = 0; u < n; ++u)
        {
            for(uint32_t v = u + 1; v < n; ++v)
            {
                if(g[u * n + v] != nil)
                {
                    if(next_probability() < dp)
                    {
                        icontacts.emplace_back(u, v,
                                               interval {g[u * n + v], t1});
                        g[u * n + v] = nil;
                    }
                }
                else
                {
                    if(next_probability() < bp)
                    {
                        g[u * n + v] = t1;
                    }
                }
            }
        }
    }

    for(int u = 0; u < n; ++u)
    {
        for(int v = u + 1; v < n; ++v)
        {
            if(g[u * n + v] != nil)
            {
                icontacts.emplace_back(u, v, interval {g[u * n + v], t});
            }
        }
    }

    return icontacts;
}

inline auto make_query(uint32_t n, uint32_t tau) -> icontact
{
    uint32_t u = next_probability() * n;
    if(u == n)
    {
        --u;
    }

    uint32_t v = next_probability() * n;
    if(v == n)
    {
        --v;
    }
    interval interv = {static_cast<uint32_t>(next_probability() * tau),
                       static_cast<uint32_t>(next_probability() * tau)};

    if(interv.right < interv.left)
    {
        std::swap(interv.left, interv.right);
    }
    else if(interv.left == interv.right)
    {
        interv.right += 1;
    }

    return {u, v, interv};
}
