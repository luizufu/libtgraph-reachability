#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
library(ggpmisc)
library(scales)
library(grid)
library(ff)
library(ffbase)
library(matrixStats)
library(gridExtra)


theme_Publication <- function(base_size = 14, base_family = "sans") {
    library(grid)
    library(ggthemes)
    (theme_foundation(base_size = base_size, base_family = base_family)
    + theme(
            plot.title = element_text(
                face = "bold",
                size = rel(1.2),
                hjust = 0.5
            ),
            plot.subtitle = element_text(
                face = "bold",
                size = rel(0.7),
                hjust = 0.5
            ),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold", size = rel(1.6)),
            axis.title.y = element_text(angle = 90, vjust = 2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(size = rel(1.2)),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour = "#f0f0f0"),
            panel.grid.minor = element_blank(),
            # panel.spacing.y = unit(-1, "lines"),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.box = "vertical",
            legend.margin = margin(),
            legend.direction = "horizontal",
            legend.key.size = unit(0.6, "cm"),
            legend.text = element_text(margin = margin(r = 10)),
            legend.spacing = unit(0, "cm"),
            legend.title = element_text(face = "italic", size = rel(0.8)),
            plot.margin = unit(c(5, 10, 2, 2), "mm"),
            strip.background = element_blank(),
            strip.text = element_text(face = "bold", hjust = 0, vjust = 0.3)
        ))
}

scientific_10 <- function(x) {
    parse(
        text = gsub(
            "e\\+*", " %*% 10^",
            scales::scientific_format()(x)
        )
    )
}

get_averages <- function(m, cnames) {
    colnames(m) <- cnames
    do.call(
        data.frame,
        aggregate(
            . ~ contacts + delta,
            data.frame(m),
            function(x) c(mean = mean(x), sd = sd(x))
        )
    )
}

input_file <- "benchmarks/lifetime-space-simple/results-sorted.csv"
output_file <- "benchmarks/lifetime-space-simple/results-aggregated.csv"
output_dir <- "benchmarks/lifetime-space-simple/"


agg <- NULL

if (file.exists(output_file)) {
    agg <- read.csv2.ffdf(
        file = output_file,
        colClasses = rep("numeric", 4)
    )
} else {
    rest <- NULL
    cnames <- c("contacts", "delta", "remaining")
    con <- file(input_file, open = "r")
    while (length(lines <- readLines(con, n = 100000, warn = FALSE)) > 0) {
        m <- rbind(rest, t(sapply(strsplit(lines, ";"), as.numeric)))

        i <- nrow(m)
        last <- m[i, c(1, 2)]
        while (all(m[i, c(1, 2)] == last)) {
            i <- i - 1
        }
        rest <- m[(i + 1):nrow(m), ]
        m <- m[1:i, ]

        agg <- ffdfappend(
            agg,
            as.ffdf(get_averages(m, cnames)),
            adjustvmode = FALSE
        )
        print(m[i, c(1, 2)])
    }
    agg <- ffdfappend(
        agg,
        as.ffdf(get_averages(rest, cnames)),
        adjustvmode = FALSE
    )
    close(con)

    write.csv2.ffdf(agg, file = output_file)
}


g1 <- ggplot(
    agg[seq(1, nrow(agg), by = 1000), ],
    aes(
        x = contacts,
        y = remaining.mean,
        color = factor(delta),
        linetype = factor(delta)
    )
) +
    scale_x_continuous(labels = number) +
    scale_y_continuous(labels = number) +
    geom_errorbar(aes(
        ymin = remaining.mean - remaining.sd,
        ymax = remaining.mean + remaining.sd
    ), width = 10, position = position_dodge(10), linetype = "solid") +
    geom_line(size = 1) +
    geom_abline(slope = 1, intercept = 0) +
    scale_color_manual(values = c("red", "blue")) +
    scale_linetype_manual(values = c("dotted", "dashed")) +
    labs(
        x = "Number of contacts",
        y = "Average Number of R-tuples"
    ) +
    guides(color = "none", linetype = "none") +
    theme_Publication()

tmp <- agg[agg$contacts <= 50, ]
g2 <- ggplot(
    tmp[seq(1, nrow(tmp), by = 2), ],
    aes(
        x = contacts,
        y = remaining.mean,
        color = factor(delta),
        linetype = factor(delta)
    )
) +
    scale_x_continuous(labels = number) +
    scale_y_continuous(labels = number) +
    geom_errorbar(aes(
        ymin = remaining.mean - remaining.sd,
        ymax = remaining.mean + remaining.sd
    ), width = .8, position = position_dodge(.8), linetype = "solid") +
    geom_line(size = 1) +
    geom_abline(slope = 1, intercept = 0) +
    scale_color_manual(values = c("red", "blue")) +
    scale_linetype_manual(values = c("dotted", "dashed")) +
    labs(
        x = "Number of contacts",
        y = "Average Number of R-tuples"
    ) +
    guides(color = "none", linetype = "none") +
    theme_Publication()

tmp <- agg[agg$contacts <= 200, ]
g3 <- ggplot(
    tmp[seq(1, nrow(tmp), by = 8), ],
    aes(
        x = contacts,
        y = remaining.mean,
        color = factor(delta),
        linetype = factor(delta)
    )
) +
    scale_x_continuous(labels = number) +
    scale_y_continuous(labels = number) +
    geom_errorbar(aes(
        ymin = remaining.mean - remaining.sd,
        ymax = remaining.mean + remaining.sd
    ), width = 3, position = position_dodge(3), linetype = "solid") +
    geom_line(size = 1) +
    geom_abline(slope = 1, intercept = 0) +
    scale_color_manual(values = c("red", "blue")) +
    scale_linetype_manual(values = c("dotted", "dashed")) +
    labs(
        x = "Number of contacts",
        y = "Average Number of R-tuples"
    ) +
    guides(color = "none", linetype = "none") +
    theme_Publication()


ggsave(paste0(output_dir, "average-tuples1.pdf"), g1, width = 8)
ggsave(paste0(output_dir, "average-tuples2.pdf"), g2, width = 8)
ggsave(paste0(output_dir, "average-tuples3.pdf"), g3, width = 8)
