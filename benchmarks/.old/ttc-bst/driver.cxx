#include <array>

#include <libgraph-structures/adj-matrix.hxx>
#include <libtgraph-reachability/journey.hxx>
#include <libtgraph-reachability/ttc-bst.hxx>
#include <libtgraph-reachability/ttc.hxx>

#include <vector>
#include <algorithm>
#include <benchmarks/ttc-bst/type_list.hxx>
#include <iomanip>
#include <iostream>
#include <random>
#include <thread>
#include <tuple>
#include <type_traits>
#include <chrono>

struct configuration
{
    uint32_t n = 0;
    uint32_t tau = 0;
    uint32_t delta = 1;
    uint32_t contacts = 0;
    uint32_t interval_contacts = 0;
    uint32_t bulk_insertions = 0;
    uint32_t single_insertions = 0;
    uint32_t queries = 0;
    uint32_t n_experiments = 0;
    uint32_t n_trials = 0;
};

struct benchmark
{
    std::string data_structure;
    std::string operation;
    configuration config = {};
    double time = 0;
    double improvement = 1;
    double stack_usage = 0;
    double heap_usage = 0;
    double binary_searches = 0;
    double sequential_searches = 0;
    double tree_inserts = 0;
    double tree_erases = 0;
};

using namespace tgraph_reachability;
using namespace graph_structures;

using structures = type_list<ttc<adj_matrix>>;

using icontact = std::tuple<uint32_t, uint32_t, interval>;

auto make_emeg(uint32_t n, uint32_t t, float bp, float dp)
    -> std::vector<icontact>;
auto make_query(uint32_t n, uint32_t tau) -> icontact;

template<typename Index>
auto run(const configuration& config, std::vector<contact>* contacts,
         const std::vector<icontact>& queries) -> std::array<benchmark, 4>;

void add_to(std::array<benchmark, 4>* lhs, const std::array<benchmark, 4>& rhs);

auto main() -> int
{
    std::cout << "data_structure;"
              << "operation;"
              << "n;"
              << "tau;"
              << "delta;"
              << "contacts;"
              << "interval_contacts;"
              << "bulk_insertions;"
              << "single_insertions;"
              << "queries;"
              << "exps;"
              << "trials;"
              << "time;"
              << "improv;"
              << "stack;"
              << "heap;"
              << "binary_searches;"
              << "sequential_searches;"
              << "tree_insertions;"
              << "tree_erases" << std::endl;

    std::vector<std::pair<uint32_t, uint32_t>> parameters = {
        {32, 4},     {8, 32},     {32, 8},     {16, 32},    {32, 16},
        {32, 32},    {4, 64},     {64, 4},     {8, 64},     {64, 8},
        {16, 64},    {64, 16},    {32, 64},    {64, 32},    {64, 64},
        {4, 128},    {128, 4},    {8, 128},    {128, 8},    {16, 128},
        {128, 16},   {32, 128},   {128, 32},   {64, 128},   {128, 64},
        {128, 128},  {4, 256},    {256, 4},    {8, 256},    {256, 8},
        {16, 256},   {256, 16},   {32, 256},   {256, 32},   {64, 256},
        {256, 64},   {128, 256},  {256, 128},  {256, 256},  {4, 512},
        {512, 4},    {8, 512},    {512, 8},    {16, 512},   {512, 16},
        {32, 512},   {512, 32},   {64, 512},   {512, 64},   {128, 512},
        {512, 128},  {256, 512},  {512, 256},  {512, 512},  {4, 1024},
        {1024, 4},   {8, 1024},   {1024, 8},   {16, 1024},  {1024, 16},
        {32, 1024},  {1024, 32},  {64, 1024},  {1024, 64},  {128, 1024},
        {1024, 128}, {256, 1024}, {1024, 256}, {512, 1024}, {1024, 512},
        {1024, 1024}};

    for(auto parameter: parameters)
    {
        for(uint32_t d = 1; d < parameter.second / 2; ++d)
        {
            configuration config = {
                .n = parameter.first,
                .tau = parameter.second,
                .delta = d,
                .queries = 1000,
                .n_experiments = 1,
                .n_trials = 1,
            };

            std::array<std::array<benchmark, 4>, size_v<structures>> bs = {};

            for(uint32_t exp = 0; exp < config.n_experiments; ++exp)
            {
                std::cerr << "exp: " << exp + 1 << std::endl;
                std::cerr << "make_emeg" << std::endl;
                auto icontacts = make_emeg(config.n, config.tau, .1F, .3F);
                config.interval_contacts = icontacts.size();

                std::vector<contact> contacts;
                for(const auto& [u, v, interv]: icontacts)
                {
                    for(uint32_t t = interv.left; t < interv.right; ++t)
                    {
                        contacts.push_back({u, v, t});
                    }
                }
                config.contacts = contacts.size();

                std::vector<icontact> queries;
                for(uint32_t i = 0; i < config.queries; ++i)
                {
                    queries.push_back(make_query(config.n, config.tau));
                }

                config.single_insertions = std::min(1000U, config.contacts);
                config.bulk_insertions =
                    config.contacts - config.single_insertions;

                for_<0, size_v<structures>>(
                    [&](auto i)
                    {
                        using st = typename get<i, structures>::type;
                        std::cerr << type_name<st>() << std::endl;
                        add_to(&bs[i], run<st>(config, &contacts, queries));
                    });
            }

            for(uint32_t j = 0; j < 4; ++j)
            {
                for_<0, size_v<structures>>(
                    [&](auto i)
                    {
                        auto& b = bs[i][j];
                        b.improvement = bs[0][j].time / b.time;

                        std::cout
                            << b.data_structure << ';' << b.operation << ';'
                            << b.config.n << ';' << b.config.tau << ';'
                            << b.config.delta << ';' << b.config.contacts << ';'
                            << b.config.interval_contacts << ';'
                            << b.config.bulk_insertions << ';'
                            << b.config.single_insertions << ';'
                            << b.config.queries << ';' << b.config.n_experiments
                            << ';' << b.config.n_trials << ';' << b.time << ';'
                            << b.improvement << ';' << b.stack_usage << ';'
                            << b.heap_usage << ';' << b.binary_searches << ';'
                            << b.sequential_searches << ';' << b.tree_inserts
                            << ';' << b.tree_erases << std::endl;
                    });
            }
        }
    }
}

template<typename Index>
auto make_result(const Index& index, const configuration& config, double time,
                 const std::string& operation, const benchmark& last)
    -> benchmark
{
    benchmark b {
        .data_structure = std::is_same_v<Index, ttc<adj_matrix>>
                              ? "ttc<adj_matrix>"
                          : std::is_same_v<Index, ttc_bst<adj_matrix>>
                              ? "tcc_bst<adj_matrix>"
                              : "invalid_data_structure",
        .operation = operation,
        .config = config,
    };

    double n_runs = config.n_experiments * config.n_trials;

    b.time = time / n_runs;
    b.stack_usage = (index.stack_usage() - last.stack_usage) / n_runs;
    b.heap_usage = (index.heap_usage() - last.heap_usage) / n_runs;
    b.binary_searches = index.binary_searches() / n_runs;
    b.sequential_searches = index.sequential_searches() / n_runs;
    b.tree_inserts = index.tree_inserts() / n_runs;
    b.tree_erases = index.tree_erases() / n_runs;
    index.reset_benchmark();

    return b;
}

auto generator() -> std::mt19937&;

template<typename Index>
auto run(const configuration& config, std::vector<contact>* contacts,
         const std::vector<icontact>& queries) -> std::array<benchmark, 4>
{
    std::array<benchmark, 4> bs = {};

    auto now = []()
    {
        return std::chrono::high_resolution_clock::now();
    };

    auto diffnow = [&now](const auto& start)
    {
        std::chrono::duration<double, std::milli> diff = now() - start;
        return diff.count();
    };

    auto dont_optimize = [](auto&& x)
    {
        static auto ttid = std::this_thread::get_id();
        if(ttid == std::thread::id())
        {
            const auto* p = &x;
            putchar(*reinterpret_cast<const char*>(p));

            std::abort();
        }
    };

    auto get_usage_state = [](const auto& index)
    {
        benchmark b;
        b.stack_usage = index.stack_usage();
        b.heap_usage = index.heap_usage();

        return b;
    };

    for(uint32_t i = 0; i < config.n_trials; ++i)
    {
        std::shuffle(contacts->begin(), contacts->end(), generator());
        auto bulk_end = contacts->begin() + config.bulk_insertions;

        benchmark last = {};
        std::cerr << "Index::create" << std::endl;
        auto start = now();
        Index index(config.n, config.tau, config.delta, contacts->begin(),
                    bulk_end);
        bs[0] = make_result(index, config, diffnow(start), "bulkload_creation",
                            last);

        last = get_usage_state(index);
        std::cerr << "Index::add_contact" << std::endl;
        start = now();
        for(auto& [u, v, t]: *contacts)
        {
            index.add_contact({u, v, t});
        }
        bs[1] = make_result(index, config, diffnow(start), "add_contact", last);

        last = get_usage_state(index);
        std::cerr << "Index::can_reach" << std::endl;
        start = now();
        for(const auto& [u, v, interv]: queries)
        {
            dont_optimize(index.can_reach(u, v, interv));
        }
        bs[2] = make_result(index, config, diffnow(start), "can_reach", last);

        last = get_usage_state(index);
        std::cerr << "Index::reconstruct_journey" << std::endl;
        start = now();
        for(const auto& [u, v, interv]: queries)
        {
            dont_optimize(index.reconstruct_journey(u, v, interv));
        }
        bs[3] = make_result(index, config, diffnow(start), "get_journey", last);
    }

    return bs;
}

void add_to(std::array<benchmark, 4>* lhs, const std::array<benchmark, 4>& rhs)
{
    for(uint32_t i = 0; i < 4; ++i)
    {
        (*lhs)[i].data_structure = rhs[i].data_structure;
        (*lhs)[i].operation = rhs[i].operation;
        (*lhs)[i].config.n = rhs[i].config.n;
        (*lhs)[i].config.tau = rhs[i].config.tau;
        (*lhs)[i].config.delta = rhs[i].config.delta;
        (*lhs)[i].config.contacts = rhs[i].config.contacts;
        (*lhs)[i].config.interval_contacts = rhs[i].config.interval_contacts;
        (*lhs)[i].config.bulk_insertions = rhs[i].config.bulk_insertions;
        (*lhs)[i].config.single_insertions = rhs[i].config.single_insertions;
        (*lhs)[i].config.queries = rhs[i].config.queries;
        (*lhs)[i].config.n_experiments = rhs[i].config.n_experiments;
        (*lhs)[i].config.n_trials = rhs[i].config.n_trials;
        (*lhs)[i].time += rhs[i].time;
        (*lhs)[i].stack_usage += rhs[i].stack_usage;
        (*lhs)[i].heap_usage += rhs[i].heap_usage;
        (*lhs)[i].tree_inserts += rhs[i].tree_inserts;
        (*lhs)[i].tree_erases += rhs[i].tree_erases;
        (*lhs)[i].binary_searches += rhs[i].binary_searches;
        (*lhs)[i].sequential_searches += rhs[i].sequential_searches;
    }
}

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r()};
    static std::mt19937 gen(seed);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

auto make_emeg(uint32_t n, uint32_t t, float bp, float dp)
    -> std::vector<icontact>
{
    std::vector<icontact> icontacts;

    uint32_t nil = std::numeric_limits<uint32_t>::max();
    std::vector<uint32_t> g(n * n, nil);

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = u + 1; v < n; ++v)
        {
            if(next_probability() < bp / (bp + dp))
            {
                g[u * n + v] = 0;
            }
        }
    }

    for(uint32_t t1 = 1; t1 < t; ++t1)
    {
        for(uint32_t u = 0; u < n; ++u)
        {
            for(uint32_t v = u + 1; v < n; ++v)
            {
                if(g[u * n + v] != nil)
                {
                    if(next_probability() < dp)
                    {
                        icontacts.emplace_back(u, v,
                                               interval {g[u * n + v], t1});
                        g[u * n + v] = nil;
                    }
                }
                else
                {
                    if(next_probability() < bp)
                    {
                        g[u * n + v] = t1;
                    }
                }
            }
        }
    }

    for(int u = 0; u < n; ++u)
    {
        for(int v = u + 1; v < n; ++v)
        {
            if(g[u * n + v] != nil)
            {
                icontacts.emplace_back(u, v, interval {g[u * n + v], t});
            }
        }
    }

    return icontacts;
}

inline auto make_query(uint32_t n, uint32_t tau) -> icontact
{
    uint32_t u = next_probability() * n;
    uint32_t v = next_probability() * n;
    interval interv = {static_cast<uint32_t>(next_probability() * tau),
                       static_cast<uint32_t>(next_probability() * tau)};

    if(interv.right < interv.left)
    {
        std::swap(interv.left, interv.right);
    }
    else if(interv.left == interv.right)
    {
        interv.right += 1;
    }

    return {u, v, interv};
}
