#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
library(ggpmisc)
library(scales)
library(grid)


# args <- commandArgs(trailingOnly = TRUE)
args <- c("benchmarks/ttc/results2.csv", "out")


if (length(args) != 2) {
  stop("Missing arguments, use \"plot.r in.csv outputdir\"", call. = FALSE)
}

theme_Publication <- function(base_size = 14, base_family = "sans") {
    library(grid)
    library(ggthemes)
    (theme_foundation(base_size = base_size, base_family = base_family)
    + theme(
            plot.title = element_text(
                face = "bold",
                size = rel(1.2),
                hjust = 0.5
            ),
            plot.subtitle = element_text(
                face = "bold",
                size = rel(0.7),
                hjust = 0.5
            ),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold", size = rel(1)),
            axis.title.y = element_text(angle = 90, vjust = 2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour = "#f0f0f0"),
            panel.grid.minor = element_blank(),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.box = "vertical",
            legend.margin = margin(),
            legend.direction = "horizontal",
            legend.key.size = unit(0.6, "cm"),
            legend.spacing = unit(0, "cm"),
            legend.title = element_text(face = "italic", size = rel(0.8)),
            plot.margin = unit(c(10, 5, 5, 5), "mm"),
            strip.background = element_rect(colour = "#f0f0f0", fill = "#f0f0f0"),
            strip.text = element_text(face = "bold")
        ))
}


data <- read.csv(args[1], sep = ";")
data <- data[data$data_structure != "" & data$operation != "", ]
data <- data[data$contacts > 128, ]

nops <- c("bulkload_creation", "add_contact", "can_reach", "get_journey")
dops <- c("bulk_insertions", "single_insertions", "queries", "queries")

extract_x <- function(d, j) {
    if (j == 1) {
        return(d[, "bulk_insertions"])
    }

    if (j == 2) {
        return(d[, "bulk_insertions"] + (d[, "single_insertions"] / 2))
    }

    return(d[, "bulk_insertions"] + d[, "single_insertions"])
}

totittle <- function(op) {
    if (op == "bulkload_creation")
        "BULK_ADD_CONTACT"
    else if (op == "add_contact")
        "ADD_CONTACT"
    else if (op == "can_reach")
        "CAN_REACH"
    else
        "RECONSTRUCT_JOURNEY"
}

scientific_10 <- function(x) {
    parse(
        text = gsub("e\\+*", " %*% 10^",
        scales::scientific_format()(x)))
}


dir.create(args[2], showWarnings = F)

plots <- list()
plots_size <- 0

for (j in 1:4) {
    d <- data[
        data$operation == nops[j] &
        data$data_structure == "tcc_bst<adj_matrix>" &
        data$delta == 1, ]
    d$samples <- d[, dops[j]]
    d[d$samples == 0, "samples"] <- 1

    d$x <- extract_x(d, j)
    d$y <- if (j == 1) d$time else d$time / d$samples

    plots_size <- plots_size + 1
    plots[[plots_size]] <- ggplot(d, aes(x = x, y = y)) +
        geom_point() +
        scale_x_continuous(label = scientific_10) +
        labs(
            title = totittle(nops[j]),
            x = "Number of contacts",
            y = "Wall clock time (ms)"
        ) +
        theme_Publication()
}

for (j in 1:4) {
    d <- data[
        data$operation == nops[j] &
        data$data_structure == "tcc_bst<adj_matrix>" &
        data$delta == 1 &
        data$n == 256, ]
    d$samples <- d[, dops[j]]
    d[d$samples == 0, "samples"] <- 1

    d$x <- d$tau
    d$y <- if (j == 1) d$time else d$time / d$samples

    plots_size <- plots_size + 1
    plots[[plots_size]] <- ggplot(d, aes(x = x, y = y)) +
        geom_point() +
        labs(
            title = totittle(nops[j]),
            subtitle = expression(paste("For ", N, " = ", 256)),
            x = expression(paste("Number of timestamps (", tau, ")")),
            y = "Wall clock time (ms)"
        ) +
        scale_x_continuous(trans = log2_trans(),
            breaks = trans_breaks("log2", function(x) 2^x)) +
        theme_Publication()
}

for (j in 1:4) {
    d <- data[
        data$operation == nops[j] &
        data$data_structure == "tcc_bst<adj_matrix>" &
        data$delta == 1 &
        data$tau == 256, ]
    d$samples <- d[, dops[j]]
    d[d$samples == 0, "samples"] <- 1

    d$x <- d$n
    d$y <- if (j == 1) d$time else d$time / d$samples

    plots_size <- plots_size + 1
    plots[[plots_size]] <- ggplot(d, aes(x = x, y = y)) +
        geom_point() +
        scale_x_continuous(trans = log2_trans(),
            breaks = trans_breaks("log2", function(x) 2^x)) +
        labs(
            title = totittle(nops[j]),
            subtitle = expression(paste("For ", tau, " = ", 256)),
            x = "Number of vertices (N)",
            y = "Wall clock time (ms)"
        ) +
        theme_Publication()
}

pages <- ggarrange(plotlist = plots, nrow = 1, ncol = 1)
ggexport(pages, filename = paste0(args[2], "/time.pdf"))


plots <- list()
plots_size <- 0

for (j in 1:2) {
    d <- data[
        data$operation == nops[j] &
        data$data_structure == "ttc<adj_matrix>", ]
    d$samples <- d[, dops[j]]
    d[d$samples == 0, "samples"] <- 1
    d$tuples <- d$tree_insertions - d$tree_erases

    d$x <- extract_x(d, j)
    d$y <- if (j == 1) d$tuples else d$tuples / d$samples

    plots_size <- plots_size + 1
    plots[[plots_size]] <- ggplot(d, aes(x = x, y = y)) +
        geom_line() +
        scale_x_continuous(label = scientific_10) +
        scale_y_continuous(label = scientific_10) +
        labs(
            title = totittle(nops[j]),
            x = "Number of contacts",
            y = "Number of tuples"
        ) +
        theme_Publication()
}

pages <- ggarrange(plotlist = plots, nrow = 1, ncol = 1)
ggexport(pages, filename = paste0(args[2], "/space.pdf"))
