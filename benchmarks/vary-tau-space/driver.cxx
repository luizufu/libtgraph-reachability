#include <array>

#include <libgraph-structures/adj-matrix.hxx>
#include <libtgraph-reachability/ttc.hxx>
#include <libtgraph-reachability/rttree.hxx>

#include <iomanip>
#include <iostream>
#include <random>
#include <thread>
#include <tuple>
#include <chrono>

using namespace tgraph_reachability;
using namespace graph_structures;

using icontact = std::tuple<uint32_t, uint32_t, interval>;

auto make_complete(uint32_t n, uint32_t tau) -> std::vector<contact>;
auto make_query(uint32_t n, uint32_t tau) -> icontact;
auto generator() -> std::mt19937&;

void dont_optimize(auto&& x)
{
    static auto ttid = std::this_thread::get_id();
    if(ttid == std::thread::id())
    {
        const auto* p = &x;
        putchar(*reinterpret_cast<const char*>(p));

        std::abort();
    }
};

auto main() -> int
{
    /* std::cout << "contacts;delta;add_contact;can_reach;reconstruct_journey"
     */
    /* << std::endl; */

    uint32_t n = 256;
    std::array<uint32_t, 8> taus = {8, 16, 32, 64, 128, 256, 512, 1024};
    uint32_t delta = 1;
    double bulkrate = 0.25;
    uint32_t n_operations = 10000;

    using graph_structures::adj_matrix;
    using tgraph_reachability::ttc;

    for(uint32_t tau: taus)
    {
        auto contacts = make_complete(n, tau);
        uint32_t size = contacts.size();
        uint32_t bulksize = size * bulkrate;

        std::shuffle(contacts.begin(), contacts.end(), generator());
        contacts.erase(contacts.begin() + (bulksize + n_operations),
                       contacts.end());

        ttc<adj_matrix, rttree> idx(n, tau, delta, contacts.begin(),
                            contacts.begin() + bulksize);

        double res1 = ttc<adj_matrix, rttree>::tree_inserts();
        double res2 = ttc<adj_matrix, rttree>::tree_erases();
        double res3 = 0;
        double res4 = 0;

        for(auto it = contacts.begin() + bulksize; it != contacts.end(); ++it)
        {
            uint64_t inserts_before = ttc<adj_matrix, rttree>::tree_inserts();
            uint64_t erases_before = ttc<adj_matrix, rttree>::tree_erases();
            idx.add_contact(*it);
            uint64_t inserts_after = ttc<adj_matrix, rttree>::tree_inserts();
            uint64_t erases_after = ttc<adj_matrix, rttree>::tree_erases();

            res3 += inserts_after - inserts_before;
            res4 += erases_after - erases_before;
        }

        std::cout << n << ";" << tau << ";" << delta << ";" << bulksize << ";"
                  << res1 << ";" << res2 << ";"
                  << (res3 / static_cast<double>(n_operations)) << ";"
                  << (res4 / static_cast<double>(n_operations)) << std::endl;
    }
}

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r()};
    static std::mt19937 gen(seed);
    return gen;
}

auto next_probability() -> double
{
    static std::uniform_real_distribution<double> dist(0.F, 1.F);
    return dist(generator());
}

auto make_complete(uint32_t n, uint32_t tau) -> std::vector<contact>
{
    std::vector<contact> contacts;

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = 0; v < n; ++v)
        {
            for(uint32_t t = 0; t < tau; ++t)
            {
                contacts.push_back({u, v, t});
            }
        }
    }

    return contacts;
}

auto make_query(uint32_t n, uint32_t tau) -> icontact
{
    uint32_t u = next_probability() * n;
    if(u == n)
    {
        --u;
    }

    uint32_t v = next_probability() * n;
    if(v == n)
    {
        --v;
    }
    interval interv = {static_cast<uint32_t>(next_probability() * tau),
                       static_cast<uint32_t>(next_probability() * tau)};

    if(interv.right < interv.left)
    {
        std::swap(interv.left, interv.right);
    }
    else if(interv.left == interv.right)
    {
        interv.right += 1;
    }

    return {u, v, interv};
}
