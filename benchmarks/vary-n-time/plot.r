#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
library(ggpmisc)
library(scales)
library(grid)
library(gtable)

theme_Publication <- function(base_size = 14, base_family = "sans") {
    library(grid)
    library(ggthemes)
    (theme_foundation(base_size = base_size, base_family = base_family)
    + theme(
            plot.title = element_text(
                face = "bold",
                size = rel(1.2),
                hjust = 0.5
            ),
            plot.subtitle = element_text(
                face = "bold",
                size = rel(0.7),
                hjust = 0.5
            ),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold", size = rel(0.8)),
            axis.title.y = element_text(angle = 90, vjust = 2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(size = rel(0.6)),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour = "#f0f0f0"),
            panel.grid.minor = element_blank(),
            panel.spacing.y = unit(-1, "lines"),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.box = "vertical",
            legend.margin = margin(),
            legend.direction = "horizontal",
            legend.key.size = unit(0.6, "cm"),
            legend.text = element_text(margin = margin(r = 10)),
            legend.spacing = unit(0, "cm"),
            legend.title = element_text(face = "italic", size = rel(0.8)),
            plot.margin = unit(c(0, 0, 0, 1.2), "mm"),
            strip.background = element_blank(),
            strip.text = element_text(face = "bold", hjust = 0, vjust = 0.3)
        ))
}

scientific_10 <- function(x) {
    parse(
        text = gsub(
            "e\\+*", " %*% 10^",
            scales::scientific_format()(x)
        )
    )
}

input_file <- "benchmarks/vary-n-time/results.csv"
output_dir <- "benchmarks/vary-n-time/"

data <- read.csv(input_file, sep = ";")
names(data) <- c(
    "n", "tau", "delta", "contacts",
    "construction", "add_contact", "can_reach", "reconstruct_journey"
)
data <- aggregate(
    . ~ n + tau + contacts + delta,
    data,
    function(x) c(mean = mean(x), sd = sd(x))
)
data$delta <- factor(data$delta)

y_cols <- c(
    "construction",
    "add_contact",
    "can_reach",
    "reconstruct_journey"
)

plots <- list()
plots_size <- 0

for (y_col in y_cols) {
    y_col_mean <- paste0(y_col, "[, \"mean\"]")
    y_col_sd <- paste0(y_col, "[, \"sd\"]")
    errbar <- aes_string(
        ymin = paste0(y_col_mean, "-", y_col_sd),
        ymax = paste0(y_col_mean, "+", y_col_sd)
    )

    plots_size <- plots_size + 1
    plots[[plots_size]] <-
        ggplot(data, aes_string(x = "n", y = y_col_mean)) +
        geom_line(size = 0.6) +
        geom_errorbar(mapping = errbar, size = 0.6) +
        geom_point(size = 1.5) +
        scale_x_continuous(labels = number) +
        scale_y_continuous(labels = scientific_10) +
        labs(
            title = toupper(y_col),
            x = expression(N),
            y = "Wall clock time (ms)"
        ) +
        theme_Publication()
}

pages <- ggarrange(plotlist = plots, nrow = 1, ncol = 1)
ggexport(pages, filename = paste0(output_dir, "time.pdf"))
