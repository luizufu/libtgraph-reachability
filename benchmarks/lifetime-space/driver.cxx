#include <array>

#include <libgraph-structures/adj-matrix.hxx>
#include <libtgraph-reachability/ttc.hxx>
#include <libtgraph-reachability/rttree.hxx>

#include <iomanip>
#include <iostream>
#include <random>
#include <thread>
#include <tuple>
#include <chrono>

using namespace tgraph_reachability;
using namespace graph_structures;

using icontact = std::tuple<uint32_t, uint32_t, interval>;

auto make_complete(uint32_t n, uint32_t t) -> std::vector<contact>;
auto generator() -> std::mt19937&;

void dont_optimize(auto&& x)
{
    static auto ttid = std::this_thread::get_id();
    if(ttid == std::thread::id())
    {
        const auto* p = &x;
        putchar(*reinterpret_cast<const char*>(p));

        std::abort();
    }
};

auto main() -> int
{
    /* std::cout << "contacts;delta;inserts;erases" << std::endl; */

    uint32_t n = 50;
    uint32_t tau = 100;
    std::array<uint32_t, 1> deltas = {1};
    uint32_t biggest_delta = 1;

    using graph_structures::adj_matrix;
    using tgraph_reachability::ttc;

    auto contacts = make_complete(n, tau);
    std::shuffle(contacts.begin(), contacts.end(), generator());

    for(uint32_t delta: deltas)
    {
        ttc<adj_matrix, rttree>::reset_benchmark();
        ttc<adj_matrix, rttree> dt(n, tau, delta);

        uint32_t c = 0;
        for(auto contact: contacts)
        {
            dt.add_contact(contact);
            c++;

            uint64_t inserts = ttc<adj_matrix, rttree>::tree_inserts();
            uint64_t erases = ttc<adj_matrix, rttree>::tree_erases();
            const auto& freqs = ttc<adj_matrix, rttree>::journey_duration_frequency();
            auto end = freqs.end();

            std::cout << c << ";" << delta << ";" << inserts << ";" << erases
                      << ";" << (inserts - erases) << ";";
            std::cout << (freqs.find(0) != end ? freqs.at(0) : 0);
            for(uint32_t i = 1; i < tau + biggest_delta + 1; ++i)
            {
                std::cout << ";" << (freqs.find(i) != end ? freqs.at(i) : 0);
            }

            std::cout << std::endl;
        }
    }
}

auto generator() -> std::mt19937&
{
    static std::random_device r;
    /* static std::seed_seq seed {r(), r(), r()}; */
    static std::seed_seq seed {10000};
    static std::mt19937 gen(seed);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

auto make_complete(uint32_t n, uint32_t tau) -> std::vector<contact>
{
    std::vector<contact> contacts;

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = 0; v < n; ++v)
        {
            if(u == v)
            {
                continue;
            }

            for(uint32_t t = 0; t < tau; ++t)
            {
                contacts.push_back({u, v, t});
            }
        }
    }

    return contacts;
}
