#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
library(ggpmisc)
library(scales)
library(grid)
library(ff)
library(ffbase)
library(matrixStats)
library(gridExtra)


theme_Publication <- function(base_size = 14, base_family = "sans") {
    library(grid)
    library(ggthemes)
    (theme_foundation(base_size = base_size, base_family = base_family)
    + theme(
            plot.title = element_text(
                face = "bold",
                size = rel(1.2),
                hjust = 0.5
            ),
            plot.subtitle = element_text(
                face = "bold",
                size = rel(0.7),
                hjust = 0.5
            ),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold", size = rel(1.6)),
            axis.title.y = element_text(angle = 90, vjust = 2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(size = rel(1.2)),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour = "#f0f0f0"),
            panel.grid.minor = element_blank(),
            # panel.spacing.y = unit(-1, "lines"),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.box = "vertical",
            legend.margin = margin(),
            legend.direction = "horizontal",
            legend.key.size = unit(0.6, "cm"),
            legend.text = element_text(margin = margin(r = 10)),
            legend.spacing = unit(0, "cm"),
            legend.title = element_text(face = "italic", size = rel(0.8)),
            plot.margin = unit(c(5, 5, 2, 2), "mm"),
            strip.background = element_blank(),
            strip.text = element_text(face = "bold", hjust = 0, vjust = 0.3)
        ))
}

scientific_10 <- function(x) {
    parse(
        text = gsub(
            "e\\+*", " %*% 10^",
            scales::scientific_format()(x)
        )
    )
}

or_z <- function(v) {
    ifelse(is.na(v), 0, v)
}

compute_statistics <- function(x) {
    delta <- as.numeric(x[2])
    # freqs <- x[(4 + delta):length(x)]
    freqs <- x[(6 + delta):length(x)]
    durations <- delta:(delta + length(freqs) - 1)
    sum_freqs <- sum(freqs)
    c(
        or_z(sum(freqs != 0)),
        or_z(matrixStats::weightedMean(durations, w = freqs)),
        or_z(sum(freqs[-1]) / sum_freqs),
        or_z(sum(freqs[-c(1, 2)]) / sum_freqs),
        or_z(sum(freqs[-c(1, 2, 3)]) / sum_freqs)
    )
}

generate_names <- function(file) {
    ncols <- 0
    con <- file(input_file, open = "r")
    if (length(line <- readLines(con, n = 1, warn = FALSE)) > 0) {
        ncols <- length(unlist(strsplit(line, ";")))
    }
    close(con)
    c(
        "contacts", "delta", "inserts", "erases", "remaining",
        paste0("freq", 0:(ncols - 6)),
        c(
            "count",
            "wmean",
            "non_direct_ratio1",
            "non_direct_ratio2",
            "non_direct_ratio3"
        )
    )
}

get_averages <- function(m, cnames) {
    colnames(m) <- cnames
    do.call(
        data.frame,
        aggregate(
            . ~ contacts + delta,
            data.frame(m),
            function(x) c(mean = mean(x), sd = sd(x))
        )
    )
}

input_file <- "benchmarks/lifetime-space/results-sorted.csv"
output_file <- "benchmarks/lifetime-space/results-aggregated.csv"
output_dir <- "benchmarks/lifetime-space/"
video_prefix <- "benchmarks/lifetime-space/video/frame"
dir.create("benchmarks/lifetime-space/video", showWarnings = F)

agg <- NULL

if (file.exists(output_file)) {
    agg <- read.csv2.ffdf(
        file = output_file,
        colClasses = rep("numeric", 218)
    )
} else {
    rest <- NULL
    cnames <- generate_names(input_file)
    con <- file(input_file, open = "r")
    while (length(lines <- readLines(con, n = 100000, warn = FALSE)) > 0) {
        m <- rbind(rest, t(sapply(strsplit(lines, ";"), function(x) {
            tryCatch({
                    x <- as.numeric(x)
                    return(c(x, compute_statistics(x)))
                },
                error = function(x) {
                    print("error")
                    return(NULL)
                },
                warning = function(x) {
                    print("warning")
                    return(NULL)
                }
            )
        })))

        i <- nrow(m)
        last <- m[i, c(1, 2)]
        while (all(m[i, c(1, 2)] == last)) {
            i <- i - 1
        }
        rest <- m[(i + 1):nrow(m), ]
        m <- m[1:i, ]

        agg <- ffdfappend(
            agg,
            as.ffdf(get_averages(m, cnames)),
            adjustvmode = FALSE
        )
        print(m[i, c(1, 2)])
    }
    if (!is.null(ncol(rest))) {
        agg <- ffdfappend(
            agg,
            as.ffdf(get_averages(rest, cnames)),
            adjustvmode = FALSE
        )
    }
    close(con)

    write.csv2.ffdf(agg, file = output_file)
}

# plotg1 <- function(data) {
#     xs <- c(30, 8000, 25000, 36000, 50000, 67000, 125000, 200000)
#     g <- ggplot(
#         data[seq(1, nrow(data), by = 1000), ],
#         aes(x = contacts, y = remaining.mean, color = factor(delta))
#     ) +
#         scale_x_continuous(labels = number) +
#         scale_y_continuous(labels = number) +
#         geom_errorbar(aes(
#             ymin = remaining.mean - remaining.sd,
#             ymax = remaining.mean + remaining.sd
#         ), width = 1, position = position_dodge(0.1)) +
#         geom_line(size = 1) +
#         geom_vline(xintercept = xs, color = "blue") +
#         geom_abline(slope = 1, intercept = 0) +
#         labs(
#             x = "Number of Contacts",
#             y = "Number of R-tuples"
#         ) +
#         guides(color = FALSE) +
#         theme_Publication()

#     freqs <- paste0("freq", 0:101, ".mean")
#     plots <- list()
#     for (i in seq_along(xs)) {
#         df <- melt(
#             data[data$contacts == xs[i], c("delta", freqs)],
#             id.vars = c("delta"),
#             variable.name = "x",
#             value.name = "y"
#         )
#         df$x <- as.numeric(
#             gsub(".mean", "",
#                 gsub("freq", "", as.character(df$x))
#             )
#         )

#         plots[[i]] <-
#             ggplot(df, aes(x = x, y = y, fill = factor(delta))) +
#                 scale_y_continuous(labels = number) +
#                 geom_bar(stat = "identity") +
#                 labs(x = "Duration of R-tuples", y = "Frequency") +
#                 guides(fill = FALSE) +
#                 theme_Publication()
#     }

#     list(g = g, histograms = plots)
# }

# make_video_plots <- function(data) {
#     names(data)[which(grepl("freq.+.mean", names(data)))] <- 0:101
#     data <- data[seq(1, nrow(data), by = 100), ]
#     tab <- melt(
#         data[, c("contacts", "delta", as.character(0:101))],
#         id.vars = c("contacts", "delta"),
#         variable.name = "x",
#         value.name = "y"
#     )

#     g_template <- ggplot(
#         data[, c("contacts", "delta", "remaining.mean")],
#         aes(x = contacts, y = remaining.mean, color = factor(delta))
#     ) +
#         scale_x_continuous(labels = number) +
#         scale_y_continuous(labels = number) +
#         geom_line(size = 1) +
#         geom_abline(slope = 1, intercept = 0) +
#         labs(
#             x = "Number of Contacts",
#             y = "Number of R-tuples"
#         ) +
#         guides(color = FALSE) +
#         theme_Publication()

#     contacts <- unique(data$contacts)
#     h_template <- ggplot() +
#         scale_y_continuous(
#             labels = number,
#             limits = c(0, tail(contacts, n = 1))
#         ) +
#         scale_x_discrete(breaks = seq(0, 101, 5)) +
#         labs(x = "Duration of R-tuples", y = "Frequency") +
#         guides(fill = FALSE) +
#         theme_Publication()

#     h_mapping <- aes(x = x, y = y, fill = factor(delta))

#     i <- 0
#     for (contact in contacts[order(contacts)]) {
#         g <- g_template + geom_vline(xintercept = contact, color = "blue")
#         h <- h_template + geom_col(
#             tab[tab$contacts == contact, ],
#             mapping = h_mapping
#         )

#         i <- i + 1
#         ggsave(
#             paste0(video_prefix, i, ".png"),
#             marrangeGrob(list(g, h), nrow = 2, ncol = 1),
#             width = 10
#         )
#     }
# }


# g1 <- ggplot(
#     agg[seq(1, nrow(agg), by = 1000), ],
#     aes(x = contacts, y = remaining.mean, color = factor(delta))
# ) +
#     scale_x_continuous(labels = number) +
#     scale_y_continuous(labels = number) +
#     geom_errorbar(aes(
#         ymin = remaining.mean - remaining.sd,
#         ymax = remaining.mean + remaining.sd
#     ), width = .2, position = position_dodge(1)) +
#     geom_line(size = 1) +
#     geom_abline(slope = 1, intercept = 0) +
#     labs(
#         x = "Number of contacts",
#         y = "Average Number of R-tuples"
#     ) +
#     guides(color = FALSE) +
#     theme_Publication()

# g2 <- ggplot(
#     agg[agg$contacts <= 30, ],
#     aes(x = contacts, y = remaining.mean, color = factor(delta))
# ) +
#     scale_x_continuous(labels = number) +
#     scale_y_continuous(labels = number) +
#     geom_errorbar(aes(
#         ymin = remaining.mean - remaining.sd,
#         ymax = remaining.mean + remaining.sd
#     ), width = .4, position = position_dodge(.5)) +
#     geom_line(size = 1) +
#     geom_abline(slope = 1, intercept = 0) +
#     labs(
#         x = "Number of contacts",
#         y = "Average Number of R-tuples"
#     ) +
#     guides(color = FALSE) +
#     theme_Publication()

# tmp <- agg[agg$contacts <= 150, ]
# g3 <- ggplot(
#     tmp[seq(1, nrow(tmp), by = 4), ],
#     aes(x = contacts, y = remaining.mean, color = factor(delta))
# ) +
#     scale_x_continuous(labels = number) +
#     scale_y_continuous(labels = number) +
#     geom_errorbar(aes(
#         ymin = remaining.mean - remaining.sd,
#         ymax = remaining.mean + remaining.sd
#     ), width = 1, position = position_dodge(0.1)) +
#     geom_line(size = 1) +
#     geom_abline(slope = 1, intercept = 0) +
#     labs(
#         x = "Number of contacts",
#         y = "Average Number of R-tuples"
#     ) +
#     guides(color = FALSE) +
#     theme_Publication()


# gs <- plotg1(as.data.frame(agg))

# g2 <- ggplot(
#     tmp[seq(1, nrow(tmp), by = 1000), ],
#     aes(x = contacts, y = non_direct_ratio.mean, color = factor(delta))
# ) +
#     scale_x_continuous(labels = number) +
#     scale_y_continuous(labels = number) +
#     geom_errorbar(aes(
#         ymin = non_direct_ratio.mean - non_direct_ratio.sd,
#         ymax = non_direct_ratio.mean + non_direct_ratio.sd
#     ), width = 1, position = position_dodge(0.1)) +
#     geom_line(size = 1) +
#     geom_abline(slope = 1, intercept = 0) +
#     labs(
#         x = "Number of contacts",
#         y = "Ratio of Non-Direct R-tuples"
#     ) +
#     guides(color = FALSE) +
#     theme_Publication()

# g3 <- ggplot(
#     tmp[seq(1, nrow(tmp), by = 1000), ],
#     aes(x = contacts, y = other_ratio.mean, color = factor(delta))
# ) +
#     scale_x_continuous(labels = number) +
#     scale_y_continuous(labels = number) +
#     geom_errorbar(aes(
#         ymin = other_ratio.mean - other_ratio.sd,
#         ymax = other_ratio.mean + other_ratio.sd
#     ), width = 1, position = position_dodge(0.1)) +
#     geom_line(size = 1) +
#     geom_abline(slope = 1, intercept = 0) +
#     labs(
#         x = "Number of contacts",
#         y = "Average Duration of R-tuples"
#     ) +
#     guides(color = FALSE) +
#     theme_Publication()

# make_video_plots(agg)

# ggsave(paste0(output_dir, "test.pdf"), marrangeGrob(list(g1, g2, g3), nrow = 3, ncol = 1), width = 8, height = 16)
# ggsave(paste0(output_dir, "histogram1.pdf"), gs$g, width = 8)
# ggsave(paste0(output_dir, "histogram2.pdf"), marrangeGrob(gs$histograms, nrow = 4, ncol = 2), width = 8)
# ggsave(paste0(output_dir, "average-tuples1.pdf"), g1, width = 8)
# ggsave(paste0(output_dir, "average-tuples2.pdf"), g2, width = 8)
# ggsave(paste0(output_dir, "average-tuples3.pdf"), g3, width = 8)




tmp <- as.data.frame(agg)
tmp$diff <- c(0, diff(tmp$remaining.mean))
ggplot(
    tmp[seq(1, nrow(tmp), by = 1000), ],
    aes(x = contacts, y = non_direct_ratio3.mean, color = factor(delta))
) +
    scale_x_continuous(labels = number) +
    scale_y_continuous(labels = number) +
    geom_line(size = 1) +
    geom_abline(slope = 1, intercept = 0) +
    labs(
        x = "Number of contacts",
        y = "Average Number of R-tuples"
    ) +
    guides(color = "none") +
    theme_Publication()

# 80003, 80004, 80006
