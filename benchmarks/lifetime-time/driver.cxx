#include <array>

#include <libgraph-structures/adj-matrix.hxx>
#include <libtgraph-reachability/ttc.hxx>
#include <libtgraph-reachability/rttree.hxx>

#include <iomanip>
#include <iostream>
#include <random>
#include <thread>
#include <tuple>
#include <chrono>

using namespace tgraph_reachability;
using namespace graph_structures;

using icontact = std::tuple<uint32_t, uint32_t, interval>;

auto make_complete(uint32_t n, uint32_t t) -> std::vector<contact>;
auto make_query(uint32_t n, uint32_t tau) -> icontact;
auto generator() -> std::mt19937&;

void dont_optimize(auto&& x)
{
    static auto ttid = std::this_thread::get_id();
    if(ttid == std::thread::id())
    {
        const auto* p = &x;
        putchar(*reinterpret_cast<const char*>(p));

        std::abort();
    }
};

auto main() -> int
{
    /* std::cout << "contacts;delta;add_contact;can_reach;reconstruct_journey"
     */
    /* << std::endl; */

    uint32_t n = 128;
    uint32_t tau = 256;
    std::array<uint32_t, 6> deltas = {1, 2, 4, 8, 16, 32};
    uint32_t n_queries = 1;
    uint32_t grain = 10000;

    auto now = []()
    {
        return std::chrono::high_resolution_clock::now();
    };

    auto diffnow = [&now](const auto& start)
    {
        std::chrono::duration<double, std::milli> diff = now() - start;
        return diff.count();
    };

    using graph_structures::adj_matrix;
    using tgraph_reachability::ttc;

    auto contacts = make_complete(n, tau);
    std::shuffle(contacts.begin(), contacts.end(), generator());

    for(uint32_t delta: deltas)
    {
        ttc<adj_matrix, rttree> ttc(n, tau, delta);

        uint32_t c = 0;
        double res1 = 0;
        double res2 = 0;
        double res3 = 0;

        for(auto contact: contacts)
        {
            auto start = now();
            ttc.add_contact(contact);
            res1 += diffnow(start);
            c++;

            start = now();
            for(uint32_t i = 0; i < n_queries; ++i)
            {
                auto [u, v, interv] = make_query(n, tau);
                dont_optimize(ttc.can_reach(u, v, interv));
            }
            res2 += diffnow(start) / n_queries;

            start = now();
            for(uint32_t i = 0; i < n_queries; ++i)
            {
                auto [u, v, interv] = make_query(n, tau);
                dont_optimize(ttc.reconstruct_journey(u, v, interv));
            }
            res3 += diffnow(start) / n_queries;

            if(c % grain == 0)
            {
                std::cout << (c - static_cast<uint32_t>(grain / 2)) << ";"
                          << delta << ";" << (res1 / static_cast<double>(grain))
                          << ";" << (res2 / static_cast<double>(grain)) << ";"
                          << (res3 / static_cast<double>(grain)) << std::endl;

                res1 = 0;
                res2 = 0;
                res3 = 0;
            }
        }
    }
}

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static std::seed_seq seed {r(), r(), r()};
    static std::mt19937 gen(seed);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

auto make_complete(uint32_t n, uint32_t tau) -> std::vector<contact>
{
    std::vector<contact> contacts;

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = 0; v < n; ++v)
        {
            for(uint32_t t = 0; t < tau; ++t)
            {
                contacts.push_back({u, v, t});
            }
        }
    }

    return contacts;
}

inline auto make_query(uint32_t n, uint32_t tau) -> icontact
{
    uint32_t u = next_probability() * n;
    if(u == n)
    {
        --u;
    }

    uint32_t v = next_probability() * n;
    if(v == n)
    {
        --v;
    }
    interval interv = {static_cast<uint32_t>(next_probability() * tau),
                       static_cast<uint32_t>(next_probability() * tau)};

    if(interv.right < interv.left)
    {
        std::swap(interv.left, interv.right);
    }
    else if(interv.left == interv.right)
    {
        interv.right += 1;
    }

    return {u, v, interv};
}
