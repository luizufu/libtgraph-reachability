#!/usr/bin/env Rscript

library(ggplot2)
library(ggpubr)
library(reshape2)
library(ggthemes)
library(ggpmisc)
library(scales)
library(grid)
library(gtable)

theme_Publication <- function(base_size = 14, base_family = "sans") {
    library(grid)
    library(ggthemes)
    (theme_foundation(base_size = base_size, base_family = base_family)
    + theme(
            plot.title = element_text(
                face = "bold",
                size = rel(1.2),
                hjust = 0.5
            ),
            plot.subtitle = element_text(
                face = "bold",
                size = rel(0.7),
                hjust = 0.5
            ),
            text = element_text(),
            panel.background = element_rect(colour = NA),
            plot.background = element_rect(colour = NA),
            panel.border = element_rect(colour = NA),
            axis.title = element_text(face = "bold", size = rel(0.8)),
            axis.title.y = element_text(angle = 90, vjust = 2),
            axis.title.x = element_text(vjust = -0.2),
            axis.text = element_text(size = rel(0.6)),
            axis.line.x = element_line(colour = "black"),
            axis.line.y = element_line(colour = "black"),
            axis.ticks = element_line(),
            panel.grid.major = element_line(colour = "#f0f0f0"),
            panel.grid.minor = element_blank(),
            panel.spacing.y = unit(-1, "lines"),
            legend.key = element_rect(colour = NA),
            legend.position = "bottom",
            legend.box = "vertical",
            legend.margin = margin(),
            legend.direction = "horizontal",
            legend.key.size = unit(0.6, "cm"),
            legend.text = element_text(margin = margin(r = 10)),
            legend.spacing = unit(0, "cm"),
            legend.title = element_text(face = "italic", size = rel(0.8)),
            plot.margin = unit(c(0, 0, 0, 1.2), "mm"),
            strip.background = element_blank(),
            strip.text = element_text(face = "bold", hjust = 0, vjust = 0.3)
        ))
}

scientific_10 <- function(x) {
    parse(
        text = gsub(
            "e\\+*", " %*% 10^",
            scales::scientific_format()(x)
        )
    )
}

input_file <- "benchmarks/lifetime-time/results.csv"
output_dir <- "benchmarks/lifetime-time/"

data <- aggregate(
    . ~ contacts + delta,
    read.csv(input_file, sep = ";"),
    mean
)
data$delta <- factor(data$delta)

for (y_col in c("add_contact", "can_reach", "reconstruct_journey")) {
    y_col_cs <- paste0(y_col, "_cs")
    data[, y_col_cs] <- ave(data[, y_col], data$delta, FUN = cumsum) * 10000
}

tab <- melt(data, id.vars = c("contacts", "delta"))
tab$variable <- factor(tab$variable,
    c(
        "add_contact", "add_contact_cs",
        "can_reach", "can_reach_cs",
        "reconstruct_journey", "reconstruct_journey_cs"
    ),
    labels = c(
        "                     Non-Cumulative\n\nADD_CONTACT",
        "                     Cumulative\n\n ",
        "\n\nCAN_REACH", "",
        "\n\nRECONSTRUCT_JOURNEY", " "
    )
)

g <- ggplot(tab, aes(x = contacts, y = value, color = delta)) +
    geom_point(size = 0.01) +
    scale_x_continuous(label = scientific_10) +
    scale_y_continuous(labels = scientific_10) +
    labs(
        x = "Number of contacts",
        y = "Wall clock time (ms)"
    ) +
    facet_wrap(vars(variable), scales = "free_y", ncol = 2) +
    guides(color = guide_legend(
        title = expression(paste(delta, "  = ")),
        override.aes = list(size = 2),
        nrow = 1
    )) +
    theme_Publication()

ggsave(g, filename = paste0(output_dir, "time.pdf"))
