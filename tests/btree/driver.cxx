#include <libtgraph-reachability/detail/btree.hxx>

#include <map>
#include <vector>
#include <algorithm>
#include <iostream>
#include <random>
#include <sstream>
#include <cassert>
#include <cmath>
#include <cstdio>

template<typename Key, typename Value>
static auto gen_test_data(uint32_t n)
{
    std::vector<std::pair<Key, Value>> entries(n);
    std::vector<Key> sample_keys(n / 3);
    /* std::default_random_engine urng(std::random_device{}()); */
    std::default_random_engine urng(2019);

    std::vector<Value> ns(2 * n);
    std::iota(ns.begin(), ns.end(), 0);
    std::shuffle(ns.begin(), ns.end(), urng);

    for(size_t i = 0; i < n; ++i)
    {
        entries[i] = {static_cast<Key>(ns[i]), ns[i]};
    }

    std::shuffle(ns.begin(), ns.end(), urng);

    for(size_t i = 0; i < n / 3; ++i)
    {
        sample_keys[i] = ns[i];
    }

    return std::make_pair(entries, sample_keys);
}

template<typename Map1, typename Map2, typename Container>
void test_insertion(Map1& map1, Map2& map2, const Container& entries)
{
    uint32_t i = 0;
    for(const auto& [value, key]: entries)
    {
        map1.insert(key, value);
        assert(map1.check_validity());
        map2.insert({key, value});
        auto res = map1.find(key);
        assert(res != map1.end());
        assert(res->second == map2.at(key));
        i++;
    }

    assert(map1.size() == map2.size());
}

template<typename Map1, typename Map2>
void test_iterator(const Map1& map1, const Map2& map2)
{
    {
        assert(map2.size() == std::distance(map1.begin(), map1.end()));

        auto it1 = map1.begin();
        auto it2 = map2.begin();
        for(; it2 != map2.end(); ++it1, ++it2)
        {
            assert(it1->second == it2->second);
        }
    }

    {
        assert(map2.size() == std::distance(map1.rbegin(), map1.rend()));

        auto it1 = map1.rbegin();
        auto it2 = map2.rbegin();
        for(; it2 != map2.rend(); ++it1, ++it2)
        {
            assert(it1->second == it2->second);
        }
    }
}

template<typename Map1, typename Map2, typename Container>
void test_search(const Map1& map1, const Map2& map2, const Container& keys)
{
    for(auto const& key: keys)
    {
        auto res1 = map1.find(key);
        auto res2 = map2.find(key);

        if(res2 != map2.end())
        {
            assert(res1 != map1.end());
            assert(res1->second == res2->second);
        }
        else
        {
            assert(res1 == map1.end());
        }
    }
}

template<typename Map1, typename Map2, typename Container>
void test_deletion(Map1& map1, Map2& map2, const Container& keys)
{
    uint32_t i = 0;
    for(const auto& key: keys)
    {
        auto res1 = map1.remove(key);
        assert(map1.check_validity());
        auto res2 = map2.find(key);

        if(res2 != map2.end())
        {
            assert(res1.has_value());
            assert(*res1 == res2->second);
            map2.erase(res2);
        }
        else
        {
            assert(!res1.has_value());
        }
        ++i;
    }

    assert(map1.size() == map2.size());
}

template<typename Key, typename Value>
void test_split(std::vector<std::pair<Key, Value>>& entries)
{
    auto sorted_entries = entries;
    std::sort(sorted_entries.begin(), sorted_entries.end());

    uint32_t dim = std::log10(entries.size()) - 1;
    uint32_t by = std::pow(10, dim > 0 ? dim - 1 : dim);
    for(uint32_t i = 0; i < sorted_entries.size(); i += by)
    {
        tgraph_reachability::detail::btree<Key, Value> map1;
        for(auto e: entries)
        {
            map1.insert(e.first, e.second);
        }

        auto map2 = map1.split(sorted_entries[i].first);

        for(uint32_t j = 0; j < i; ++j)
        {
            auto res1 = map1.find(sorted_entries[j].first);
            auto res2 = map2.find(sorted_entries[j].first);
            assert(res1 != map1.end());
            assert(res2 == map1.end());
            assert(res1->second == sorted_entries[j].second);
        }

        for(uint32_t j = i; j < sorted_entries.size(); ++j)
        {
            auto res1 = map1.find(sorted_entries[j].first);
            auto res2 = map2.find(sorted_entries[j].first);
            assert(res1 == map1.end());
            assert(res2 != map1.end());
            assert(res2->second == sorted_entries[j].second);
        }

        auto it1 = map1.begin();
        auto it2 = map2.begin();
        auto it3 = sorted_entries.begin();
        while(it3 != sorted_entries.begin() + i)
        {
            assert(it1->first == it3->first);
            assert(it1->second == it3->second);
            ++it1;
            ++it3;
        }

        uint32_t k = 0;
        while(it3 != sorted_entries.end())
        {
            assert(it2->first == it3->first);
            assert(it2->second == it3->second);
            ++it2;
            ++it3;
            ++k;
        }
    }
}

template<typename Key, typename Value>
void test_join(std::vector<std::pair<Key, Value>>& entries)
{
    if(entries.size() == 1)
    {
        return;
    }

    std::sort(entries.begin(), entries.end());
    uint32_t dim = std::log10(entries.size()) - 1;
    uint32_t by = std::pow(10, dim > 0 ? dim - 1 : dim);
    for(uint32_t n = 0; n <= entries.size(); n += by)
    {
        tgraph_reachability::detail::btree<Key, Value> map1;
        tgraph_reachability::detail::btree<Key, Value> map2;

        for(uint32_t i = 0; i < n; ++i)
        {
            map1.insert(entries[i].first, entries[i].second);
        }

        for(uint32_t i = n; i < entries.size(); ++i)
        {
            map2.insert(entries[i].first, entries[i].second);
        }

        map1.join(std::move(map2));
        assert(map1.size() == entries.size());
        assert(map1.check_validity());

        for(auto [key, value]: entries)
        {
            auto res = map1.find(key);
            assert(res != map1.end());
            assert(res->second == value);
        }

        auto it1 = map1.begin();
        auto it2 = entries.begin();
        while(it2 != entries.end())
        {
            assert(it1->first == it2->first);
            assert(it1->second == it2->second);
            ++it1;
            ++it2;
        }
    }
}

template<typename Key, typename Value>
void test(uint32_t n)
{
    /* std::cout << "Testing sizeof(Key) == " << sizeof(Key) */
    /*           << "; sizeof(Value) == " << sizeof(Value) << std::endl; */

    auto [entries, sample_keys] = gen_test_data<Key, Value>(n);

    {
        tgraph_reachability::detail::btree<Key, Value> map1;
        std::map<Key, Value> map2;
        assert(map1.empty());

        /*         test_insertion(map1, map2, entries); */
        /*         test_iterator(map1, map2); */
        /*         test_search(map1, map2, sample_keys); */
        /*         test_deletion(map1, map2, sample_keys); */
        /*         test_search(map1, map2, sample_keys); */
        test_split(entries);
        /* test_join(entries); */
    }
}

auto main() -> int
{
    for(uint32_t i = 2; i < 100000; ++i)
    {
        std::cout << i << std::endl;
        test<uint32_t, uint32_t>(i);
    }

    /* test<uint32_t, uint32_t>(2969); */
    test<uint32_t, uint32_t>(100000);
    /* test<uint32_t, uint64_t>(100000); */
    /* test<uint64_t, uint32_t>(100000); */
}
