#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>
#include <libtgraph-reachability/baseline.hxx>
#include <libtgraph-reachability/ttc.hxx>
#include <libtgraph-reachability/rttree.hxx>
#include <libtgraph-reachability/version.hxx>

#include <unordered_set>
#include <vector>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include <cassert>
#include <cmath>

using edge = std::pair<uint32_t, uint32_t>;

template<template<typename> class Graph, template<typename> class Tree>
void test_timestamp_insertion(uint32_t n, uint32_t tau);

auto main() -> int
{
    using namespace graph_structures;
    using namespace tgraph_reachability;

    test_timestamp_insertion<adj_matrix, rttree>(64, 256);
    /* 1699864578 */
    /* 14 2052784469 */
    /* 24, 1761480634 8/8 */

    /* for(size_t n = 32; n <= 256; n *= 2) */
    /* { */
    /*     for(size_t tau = 64; tau <= 512; tau *= 2) */
    /*     { */
    /*         test_timestamp_insertion<adj_list>(n, tau); */
    /*         test_timestamp_insertion<adj_matrix>(n, tau); */
    /*         test_timestamp_insertion<edge_grid>(n, tau); */
    /*     } */
    /* } */

    return 0;
}

static unsigned int SEED = {};

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static bool first = true;

    if(first)
    {
        SEED = r();
        /* SEED = 1430173019; */
        first = false;
    }
    static std::mt19937 gen(SEED);
    return gen;
}

auto next_probability() -> float
{
    static std::uniform_real_distribution<float> dist(0.F, 1.F);
    return dist(generator());
}

auto generate_interval(uint32_t tau) -> tgraph_reachability::interval
{
    std::uniform_int_distribution<uint32_t> tdist(0, tau - 1);

    tgraph_reachability::interval i = {tdist(generator()), tdist(generator())};

    if(i.right < i.left)
    {
        std::swap(i.left, i.right);
    }
    else if(i.left == i.right)
    {
        i.right += 1;
    }

    return i;
}

auto make_emeg(uint32_t n, uint32_t t, float bp, float dp)
    -> std::vector<std::pair<edge, tgraph_reachability::interval>>
{
    std::vector<std::pair<edge, tgraph_reachability::interval>> contacts;

    uint32_t nil = std::numeric_limits<uint32_t>::max();
    std::vector<uint32_t> g(n * n, nil);

    for(uint32_t u = 0; u < n; ++u)
    {
        for(uint32_t v = u + 1; v < n; ++v)
        {
            if(next_probability() < bp / (bp + dp))
            {
                g[u * n + v] = 0;
            }
        }
    }

    for(uint32_t t1 = 1; t1 < t; ++t1)
    {
        for(uint32_t u = 0; u < n; ++u)
        {
            for(uint32_t v = u + 1; v < n; ++v)
            {
                if(g[u * n + v] != nil)
                {
                    if(next_probability() < dp)
                    {
                        contacts.push_back({{u, v}, {g[u * n + v], t1}});
                        g[u * n + v] = nil;
                    }
                }
                else
                {
                    if(next_probability() < bp)
                    {
                        g[u * n + v] = t1;
                    }
                }
            }
        }
    }

    for(int u = 0; u < n; ++u)
    {
        for(int v = u + 1; v < n; ++v)
        {
            if(g[u * n + v] != nil)
            {
                contacts.push_back({{u, v}, {g[u * n + v], t}});
            }
        }
    }

    return contacts;
}

template<typename T>
constexpr auto type_name() noexcept
{
    std::string_view name = "Error: unsupported compiler";
    std::string_view prefix;
    std::string_view suffix;
#ifdef __clang__
    name = __PRETTY_FUNCTION__;
    prefix = "auto type_name() [T = ";
    suffix = "]";
#elif defined(__GNUC__)
    name = __PRETTY_FUNCTION__;
    prefix = "constexpr auto type_name() [with T = ";
    suffix = "]";
#elif defined(_MSC_VER)
    name = __FUNCSIG__;
    prefix = "auto __cdecl type_name<";
    suffix = ">(void) noexcept";
#endif
    name.remove_prefix(prefix.size());
    name.remove_suffix(suffix.size());
    return name;
}

template<template<typename> class Graph, template<typename> class Tree>
void test_timestamp_insertion(uint32_t n, uint32_t tau)
{
    /* std::cerr << "Testing n = " << n << "; tau = " << tau << "; " */
    /*           << type_name<tgraph_reachability::ttc<Graph>>() << std::endl;
     */

    /* std::cout << "initiating test " << n << ", " << tau << std::endl; */
    std::uniform_int_distribution<uint32_t> vdist(0, n - 1);
    tgraph_reachability::baseline t1(n, tau, 1);
    tgraph_reachability::ttc<Graph, Tree> t2(n, tau, 1);

    auto contacts = make_emeg(n, tau, 0.1, 0.3);
    std::shuffle(contacts.begin(), contacts.end(), generator());

    uint32_t total_contacts = 0;
    for(const auto& [e, interv]: contacts)
    {
        total_contacts += interv.right - interv.left;
    }

    uint32_t running_contacts = 0;
    uint32_t ii = 0;

    for(const auto& [e, interv]: contacts)
    {
        /* std::cout << e.first << ", " << e.second << ", " << interv.left <<
         * "," */
        /* << interv.right << std::endl; */
        ++ii;
        for(uint32_t t = interv.left; t < interv.right; ++t)
        {
            t1.add_contact({e.first, e.second, t});
            t2.add_contact({e.first, e.second, t});

            for(uint32_t i = 0; i < 5; ++i)
            {
                uint32_t u = vdist(generator());
                uint32_t v = vdist(generator());
                auto interv = generate_interval(tau);

                bool res1 = t1.can_reach(u, v, interv);
                bool res2 = t2.can_reach(u, v, interv);

                if(res1 != res2)
                {
                    std::cout << "t2 error: " << res1 << std::endl;
                    std::cout << ii << ", " << SEED << std::endl;
                    std::cout << "Query: " << u << ", " << v << ", "
                              << interv.left << ", " << interv.right
                              << std::endl;
                }
                assert(res1 == res2);
            }

            ++running_contacts;
            /* int p = 100 * running_contacts / (float)total_contacts; */
            /* std::cout << '\r' << std::setw(3) << std::setfill(' ') << p */
            /*           << std::flush; */
        }
    }

    /* std::cerr << '\r' << "   " << '\r'; */
}
