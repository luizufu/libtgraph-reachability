#include <libtgraph-reachability/detail/rtbst.hxx>

#include <map>
#include <vector>
#include <algorithm>
#include <iostream>
#include <random>
#include <sstream>
#include <cassert>
#include <cstdio>

void test1()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(10);

    assert(!tree.find_prev_arrival(5));
    assert(!tree.find_next_departure(5));

    tree.insert({5, 8}, 20);
    assert(tree.find_prev_arrival(8));
    assert((tree.find_prev_arrival(8)->first == interval {5, 8}));
    assert((tree.find_prev_arrival(8)->second == 20));
    assert(!tree.find_prev_arrival(5));
    assert(tree.find_next_departure(5));
    assert((tree.find_next_departure(5)->first == interval {5, 8}));
    assert((tree.find_next_departure(5)->second == 20));
    assert(!tree.find_next_departure(8));

    assert(!tree.find_next_departure(6));

    tree.insert({6, 9}, 30);
    assert(tree.find_prev_arrival(8));
    assert((tree.find_prev_arrival(8)->first == interval {5, 8}));
    assert((tree.find_prev_arrival(8)->second == 20));
    assert(tree.find_prev_arrival(9));
    assert((tree.find_prev_arrival(9)->first == interval {6, 9}));
    assert((tree.find_prev_arrival(9)->second == 30));
    assert(!tree.find_prev_arrival(5));
    assert(tree.find_next_departure(5));
    assert((tree.find_next_departure(5)->first == interval {5, 8}));
    assert((tree.find_next_departure(5)->second == 20));
    assert(!tree.find_next_departure(8));

    tree.insert({5, 6}, 200);
    assert(tree.find_prev_arrival(8));
    assert((tree.find_prev_arrival(8)->first == interval {5, 6}));
    assert((tree.find_prev_arrival(8)->second == 200));
    assert(!tree.find_prev_arrival(5));
    assert(tree.find_next_departure(5));
    assert((tree.find_next_departure(5)->first == interval {5, 6}));
    assert((tree.find_next_departure(5)->second == 200));
    assert(!tree.find_next_departure(8));

    tree.insert({8, 9}, 300);
    assert(tree.find_prev_arrival(8));
    assert((tree.find_prev_arrival(8)->first == interval {5, 6}));
    assert((tree.find_prev_arrival(8)->second == 200));
    assert(tree.find_prev_arrival(9));
    assert((tree.find_prev_arrival(9)->first == interval {8, 9}));
    assert((tree.find_prev_arrival(9)->second == 300));
    assert(!tree.find_prev_arrival(5));
    assert(tree.find_next_departure(5));
    assert((tree.find_next_departure(5)->first == interval {5, 6}));
    assert((tree.find_next_departure(5)->second == 200));
    assert(tree.find_next_departure(8));
    assert((tree.find_next_departure(8)->first == interval {8, 9}));
    assert((tree.find_next_departure(8)->second == 300));
}

void test2()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(15);

    tree.insert({2, 7}, 20);
    tree.insert({3, 8}, 30);
    tree.insert({4, 9}, 40);
    tree.insert({5, 10}, 50);

    assert(tree.find_prev_arrival(12));
    assert((tree.find_prev_arrival(12)->first == interval {5, 10}));
    assert((tree.find_prev_arrival(12)->second == 50));

    assert(tree.find_prev_arrival(10));
    assert((tree.find_prev_arrival(10)->first == interval {5, 10}));
    assert((tree.find_prev_arrival(10)->second == 50));

    assert(tree.find_prev_arrival(9));
    assert((tree.find_prev_arrival(9)->first == interval {4, 9}));
    assert((tree.find_prev_arrival(9)->second == 40));

    assert(tree.find_prev_arrival(8));
    assert((tree.find_prev_arrival(8)->first == interval {3, 8}));
    assert((tree.find_prev_arrival(8)->second == 30));

    assert(tree.find_prev_arrival(7));
    assert((tree.find_prev_arrival(7)->first == interval {2, 7}));
    assert((tree.find_prev_arrival(7)->second == 20));

    assert(!tree.find_prev_arrival(6));
    assert(!tree.find_prev_arrival(5));
    assert(!tree.find_prev_arrival(4));

    assert(tree.find_next_departure(0));
    assert((tree.find_next_departure(0)->first == interval {2, 7}));
    assert((tree.find_next_departure(0)->second == 20));

    assert(tree.find_next_departure(2));
    assert((tree.find_next_departure(2)->first == interval {2, 7}));
    assert((tree.find_next_departure(2)->second == 20));

    assert(tree.find_next_departure(3));
    assert((tree.find_next_departure(3)->first == interval {3, 8}));
    assert((tree.find_next_departure(3)->second == 30));

    assert(tree.find_next_departure(4));
    assert((tree.find_next_departure(4)->first == interval {4, 9}));
    assert((tree.find_next_departure(4)->second == 40));

    assert(tree.find_next_departure(5));
    assert((tree.find_next_departure(5)->first == interval {5, 10}));
    assert((tree.find_next_departure(5)->second == 50));

    assert(!tree.find_next_departure(6));
    assert(!tree.find_next_departure(7));
    assert(!tree.find_next_departure(8));

    tree.insert({6, 7}, 100);

    assert(tree.find_prev_arrival(12));
    assert((tree.find_prev_arrival(12)->first == interval {6, 7}));
    assert((tree.find_prev_arrival(12)->second == 100));

    assert(tree.find_prev_arrival(10));
    assert((tree.find_prev_arrival(10)->first == interval {6, 7}));
    assert((tree.find_prev_arrival(10)->second == 100));

    assert(tree.find_prev_arrival(9));
    assert((tree.find_prev_arrival(9)->first == interval {6, 7}));
    assert((tree.find_prev_arrival(9)->second == 100));

    assert(tree.find_prev_arrival(8));
    assert((tree.find_prev_arrival(8)->first == interval {6, 7}));
    assert((tree.find_prev_arrival(8)->second == 100));

    assert(tree.find_prev_arrival(7));
    assert((tree.find_prev_arrival(7)->first == interval {6, 7}));
    assert((tree.find_prev_arrival(7)->second == 100));

    assert(!tree.find_prev_arrival(6));
    assert(!tree.find_prev_arrival(5));
    assert(!tree.find_prev_arrival(4));

    assert(tree.find_next_departure(0));
    assert((tree.find_next_departure(0)->first == interval {6, 7}));
    assert((tree.find_next_departure(0)->second == 100));

    assert(tree.find_next_departure(2));
    assert((tree.find_next_departure(2)->first == interval {6, 7}));
    assert((tree.find_next_departure(2)->second == 100));

    assert(tree.find_next_departure(3));
    assert((tree.find_next_departure(3)->first == interval {6, 7}));
    assert((tree.find_next_departure(3)->second == 100));

    assert(tree.find_next_departure(4));
    assert((tree.find_next_departure(4)->first == interval {6, 7}));
    assert((tree.find_next_departure(4)->second == 100));

    assert(tree.find_next_departure(5));
    assert((tree.find_next_departure(5)->first == interval {6, 7}));
    assert((tree.find_next_departure(5)->second == 100));

    assert(tree.find_next_departure(6));
    assert((tree.find_next_departure(6)->first == interval {6, 7}));
    assert((tree.find_next_departure(6)->second == 100));

    assert(!tree.find_next_departure(7));
    assert(!tree.find_next_departure(8));
}

void test3()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(12);

    tree.insert({1, 2}, 20);
    tree.insert({2, 3}, 30);

    assert(!tree.find_next_departure(4));
}

void test4()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(4);

    tree.insert({1, 3}, 1);
    tree.insert({2, 3}, 3);

    assert(tree.find_next_departure(0));
}

void test5()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(3);

    tree.insert({0, 3}, 1);
    tree.insert({0, 1}, 3);

    assert(tree.find_next_departure(0));
}

void test6()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(256);

    tree.insert({132, 133}, 1);
    tree.insert({193, 194}, 1);

    assert(tree.find_next_departure(164));
}

void test7()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(8);

    tree.insert({2, 4}, 1);
    tree.insert({3, 5}, 1);
    tree.insert({4, 6}, 1);

    tree.insert({4, 5}, 1);
    assert((tree.find_next_departure(1)->first == interval {2, 4}));
}

void test8()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(8);

    tree.insert({0, 2}, 1);
    tree.insert({1, 3}, 1);
    tree.insert({2, 3}, 1);
    assert((tree.find_next_departure(1)->first == interval {2, 3}));
}

void test9()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(8);

    tree.insert({0, 2}, 1);
    tree.insert({1, 3}, 1);
    tree.insert({2, 4}, 1);
    tree.insert({3, 5}, 1);
    tree.insert({5, 7}, 1);
    tree.insert({6, 8}, 1);

    assert((tree.find_next_departure(0)->first == interval {0, 2}));
    assert((tree.find_next_departure(1)->first == interval {1, 3}));
    assert((tree.find_next_departure(2)->first == interval {2, 4}));
    assert((tree.find_next_departure(3)->first == interval {3, 5}));
    assert((tree.find_next_departure(4)->first == interval {5, 7}));
    assert((tree.find_next_departure(5)->first == interval {5, 7}));
    assert((tree.find_next_departure(6)->first == interval {6, 8}));
    assert(!tree.find_next_departure(7));

    assert(!tree.find_prev_arrival(0));
    assert(!tree.find_prev_arrival(1));
    assert((tree.find_prev_arrival(2)->first == interval {0, 2}));
    assert((tree.find_prev_arrival(3)->first == interval {1, 3}));
    assert((tree.find_prev_arrival(4)->first == interval {2, 4}));
    assert((tree.find_prev_arrival(5)->first == interval {3, 5}));
    assert((tree.find_prev_arrival(6)->first == interval {3, 5}));
    assert((tree.find_prev_arrival(7)->first == interval {5, 7}));
    assert((tree.find_prev_arrival(8)->first == interval {6, 8}));
}

void test10()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(8);

    tree.insert({2, 3}, 1);
    tree.insert({3, 4}, 1);
    tree.insert({4, 6}, 1);

    tree.insert({4, 5}, 1);

    assert((tree.find_next_departure(3)->first == interval {3, 4}));
}

void test11()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(16);

    tree.insert({2, 3}, 1);
    tree.insert({3, 4}, 1);
    tree.insert({4, 5}, 1);
    tree.insert({5, 6}, 1);
    tree.insert({6, 7}, 1);
    tree.insert({8, 10}, 1);
    tree.insert({9, 11}, 1);
    assert((tree.find_next_departure(5)->first == interval {5, 6}));
}

void test12()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(8);

    tree.insert({0, 1}, 1);
    tree.insert({4, 8}, 1);
    assert((tree.find_next_departure(0)->first == interval {0, 1}));
}

void test13()
{
    using namespace tgraph_reachability;
    detail::rtbst<uint32_t> tree(8);

    tree.insert({0, 2}, 1);
    tree.insert({2, 4}, 1);
    assert((tree.find_next_departure(0)->first == interval {0, 2}));
}

void test14()
{
    using namespace tgraph_reachability;

    detail::rtbst<uint32_t> tree(8);
    tree.insert({1, 3}, 1);
    tree.insert({0, 1}, 1);
    tree.insert({1, 2}, 1);
    assert((tree.find_next_departure(0)->first == interval {0, 1}));
}

void test15()
{
    using namespace tgraph_reachability;

    detail::rtbst<uint32_t> tree(8);
    tree.insert({4, 6}, 1);
    tree.insert({5, 7}, 1);
    tree.insert({6, 7}, 1);
    assert((tree.find_next_departure(4)->first == interval {4, 6}));
}

void test16()
{
    using namespace tgraph_reachability;

    detail::rtbst<uint32_t> tree(16);
    tree.insert({5, 15}, 1);
    tree.insert({9, 10}, 1);
    assert((tree.find_next_departure(1)->first == interval {9, 10}));
}

void test17()
{
    using namespace tgraph_reachability;

    detail::rtbst<uint32_t> tree(8);
    tree.insert({0, 1}, 3);
    tree.insert({1, 5}, 3);
    tree.insert({3, 6}, 3);
    tree.insert({4, 7}, 3);

    tree.insert({5, 6}, 3);
    tree.insert({4, 5}, 3);

    assert((tree.find_next_departure(1)->first == interval {4, 5}));
}

void test18()
{
    using namespace tgraph_reachability;

    detail::rtbst<uint32_t> tree(8);
    tree.insert({1, 5}, 3);
    tree.insert({2, 6}, 3);
    tree.insert({5, 7}, 3);
    tree.insert({5, 6}, 3);
    tree.insert({4, 5}, 3);

    auto res = tree.find_next_departure(1);
    /* std::cout << res->first.left << ", " << res->first.right << std::endl; */
    assert((tree.find_next_departure(1)->first == interval {4, 5}));
}

auto main() -> int
{
    /* test1(); */
    /* test2(); */
    test3();
    test4();
    test5();
    /* test6(); */
    test7();
    test8();
    test9();
    test10();
    test11();
    test12();
    test13();
    test14();
    test15();
    test16();
    test17();
    test18();
}
