#include <libtgraph-reachability/detail/rtbst.hxx>
#include <libtgraph-reachability/detail/rttree.hxx>
#include <libtgraph-reachability/version.hxx>

#include <unordered_set>
#include <vector>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <random>
#include <cassert>
#include <cmath>

void test(uint32_t tau, uint32_t n_intervals, uint32_t n_queries);

auto main() -> int
{
    test(1024, 1024, 1024);
}

static unsigned int SEED = {};

auto generator() -> std::mt19937&
{
    static std::random_device r;
    static bool first = true;

    if(first)
    {
        SEED = r();
        /* SEED = 2996613411; */
        first = false;
    }
    static std::mt19937 gen(SEED);
    return gen;
}

auto generate_interval(uint32_t tau) -> tgraph_reachability::interval
{
    std::uniform_int_distribution<uint32_t> tdist(0, tau - 1);

    tgraph_reachability::interval i = {tdist(generator()), tdist(generator())};

    if(i.right < i.left)
    {
        std::swap(i.left, i.right);
    }
    else if(i.left == i.right)
    {
        i.right += 1;
    }

    return i;
}

void test(uint32_t tau, uint32_t n_intervals, uint32_t n_queries)
{
    tgraph_reachability::detail::rttree<uint32_t> t1;
    tgraph_reachability::detail::rtbst<uint32_t> t2(tau);

    uint32_t ii = 0;

    for(uint32_t i = 0; i < n_intervals; ++i)
    {
        ++ii;
        auto interv = generate_interval(tau);
        /* std::cout << interv.left << ", " << interv.right << std::endl; */

        t1.insert(interv, interv.left);
        t2.insert(interv, interv.left);

        for(uint32_t i = 0; i < n_queries; ++i)
        {
            auto interv = generate_interval(tau);

            auto res1 = t1.find_next(interv.right);
            auto res2 = t2.find_next_departure(interv.right);

            if(res1 != res2)
            {
                std::cout << "t2 find_next error" << std::endl;
                std::cout << ii << ", " << SEED << std::endl;
                std::cout << "Query: " << interv.left << ", " << interv.right
                          << std::endl;
            }
            assert(res1 == res2);

            res1 = t1.find_prev(interv.left);
            res2 = t2.find_prev_arrival(interv.left);

            if(res1 != res2)
            {
                std::cout << "t2 find_prev error" << std::endl;
                std::cout << ii << ", " << SEED << std::endl;
                std::cout << "Query: " << interv.left << ", " << interv.right
                          << std::endl;
            }
            assert(res1 == res2);
        }
    }
}
