#include <libgraph-structures/adj-matrix.hxx>
#include <libtgraph-reachability/baseline.hxx>
#include <libtgraph-reachability/detail/btree.hxx>
#include <libtgraph-reachability/journey.hxx>
#include <libtgraph-reachability/ttc.hxx>
#include <libtgraph-reachability/version.hxx>

#include <iostream>
#include <iterator>
#include <random>
#include <cassert>
#include <cmath>
#include <vector>

// template<typename Key, typename Value>
// static auto gen_test_data(uint32_t n)
// {
//     std::vector<std::pair<Key, Value>> entries(n);
//     std::default_random_engine urng(2019);

//     std::vector<Value> ns(2 * n);
//     std::iota(ns.begin(), ns.end(), 0);
//     std::shuffle(ns.begin(), ns.end(), urng);

//     for(size_t i = 0; i < n; ++i)
//     {
//         entries[i] = {static_cast<Key>(ns[i]), ns[i]};
//     }

//     return entries;
// }

// template<typename Vector>
// static auto get_split_range(Vector entries, double ratio)
// {
//     uint32_t n = entries.size() * ratio;
//     std::sort(entries.begin(), entries.end());

//     return std::pair(entries[n].first, entries[entries.size() - n].first);
// }

// template<typename Key, typename Value>
// void test1();

// void test2();
// void test3();
// void test4();
// void test5();

auto main() -> int
{
    /* test1<uint32_t, uint32_t>(); */
    /* test2(); */
    /* test3(); */
    // test4();
    /* test5(); */
    // int test = 0;
    // auto test2 = 0;

        std::vector<int> a;

}

// template<typename Key, typename Value>
// void test1()
// {
//     using btree = tgraph_reachability::detail::btree<Key, Value>;
//     auto entries = gen_test_data<Key, Value>(1000);

//     btree map;
//     for(auto [key, value]: entries)
//     {
//         map.insert(key, value);
//     }

//     {
//         btree copy = map;
//     }

//     for(auto [key, value]: entries)
//     {
//         auto res = map.find(key);
//         assert(res != map.end());
//         assert(res->second = value);
//     }

//     {
//         auto [first, last] = get_split_range(entries, 1.f / 3.f);
//         {
//             btree mid = map.split(first);
//             map.join(mid.split(last));
//         }
//         for(auto [key, value]: entries)
//         {
//             auto res = map.find(key);
//             if(key < first || last <= key)
//             {
//                 assert(res != map.end());
//                 assert(res->second = value);
//             }
//             else
//             {
//                 assert(res == map.end());
//             }
//         }
//     }

//     {
//         auto [first, last] = get_split_range(entries, 1.f / 4.f);
//         {
//             btree rest = map.split(first);
//         }
//         for(auto [key, value]: entries)
//         {
//             auto res = map.find(key);
//             if(key < first)
//             {
//                 assert(res != map.end());
//                 assert(res->second = value);
//             }
//             else
//             {
//                 assert(res == map.end());
//             }
//         }
//     }
// }

// void test2()
// {
//     using ttc = tgraph_reachability::ttc<graph_structures::adj_matrix>;
//     ttc index(3, 22, 1);

//     index.add_contact({0, 1, 21});

//     index.add_contact({2, 0, 14});
//     index.add_contact({2, 0, 15});
//     index.add_contact({2, 0, 16});

//     index.add_contact({0, 1, 0});
//     index.add_contact({0, 1, 1});
//     index.add_contact({0, 1, 2});
//     index.add_contact({0, 1, 3});
//     index.add_contact({0, 1, 4});
//     index.add_contact({0, 1, 5});
//     index.add_contact({0, 1, 6});
//     index.add_contact({0, 1, 7});
//     index.add_contact({0, 1, 8});
//     index.add_contact({0, 1, 9});
//     index.add_contact({0, 1, 10});
//     index.add_contact({0, 1, 11});
//     index.add_contact({0, 1, 12});
//     index.add_contact({0, 1, 13});
//     index.add_contact({0, 1, 14});
//     index.add_contact({0, 1, 15});
//     index.add_contact({0, 1, 16});
//     index.add_contact({0, 1, 17});
// }

// void test3()
// {
//     using interval = tgraph_reachability::interval;
//     using btree = tgraph_reachability::detail::btree<interval, uint32_t>;

//     auto remove = [](auto* tree, interval first, std::optional<interval> last)
//     {
//         auto mid = tree->split(first);
//         if(last)
//         {
//             tree->join(mid.split(*last));
//         }
//     };

//     btree b01;
//     btree b21;

//     b01.print_root();
//     b21.print_root();

//     b01.insert({0, 0}, 0);
//     b01.insert({1, 0}, 0);
//     b01.insert({2, 0}, 0);
//     b01.insert({3, 0}, 0);
//     b01.insert({4, 0}, 0);
//     b01.insert({5, 0}, 0);
//     b01.insert({6, 0}, 0);
//     b01.insert({7, 0}, 0);
//     b01.insert({8, 0}, 0);
//     b01.insert({9, 0}, 0);
//     b01.insert({10, 0}, 0);
//     b01.insert({11, 0}, 0);
//     b01.insert({12, 0}, 0);
//     b01.insert({13, 0}, 0);
//     b01.insert({14, 0}, 0);
//     b01.insert({15, 0}, 0);
//     b01.insert({16, 0}, 0);
//     b01.insert({21, 0}, 0);

//     b21.insert({15, 0}, 0);
//     b21.insert({16, 0}, 0);

//     {
//         std::cout << "t1" << std::endl;
//         b01.print_root();
//         b21.print_root();
//         auto mid = b21.split({16, 0});
//         b21.join(mid.split({16, 0}));
//         std::cout << "t2" << std::endl;
//         b01.print_root();
//         b21.print_root();
//     }

//     b01.print_root();
//     b21.print_root();
//     b01.insert({17, 0}, 0);

//     {
//         std::cout << "t3" << std::endl;
//         b01.print_root();
//         b21.print_root();
//         {
//             auto mid = b21.split({16, 0});
//             b21.join(mid.split({16, 0}));
//         }
//         std::cout << "t4" << std::endl;
//         b01.print_root();
//         b21.print_root();
//     }
//     std::cout << "t5" << std::endl;
//     b01.print_root();
//     b21.print_root();
// }

// void test4()
// {
//     using interval = tgraph_reachability::interval;
//     using btree = tgraph_reachability::detail::btree<interval, uint32_t>;
//     using ttc = tgraph_reachability::ttc<graph_structures::adj_matrix>;
//     using ttc_simple =
//         tgraph_reachability::ttc_simple<graph_structures::adj_matrix>;

//     auto insert = [](auto* index1, auto* index2, uint32_t u, uint32_t v,
//                      uint32_t t1, uint32_t t2)
//     {
//         for(uint32_t t = t1; t < t2; ++t)
//         {
//             index1->add_contact({u, v, t});
//             index2->add_contact({u, v, t});
//         }
//     };

//     /* auto insert2 = */
//     /*     [](auto* index1, auto* index2, uint32_t u, uint32_t v, uint32_t t) */
//     /* { */
//     /*     index1->add_contact({u, v, t}); */
//     /*     index2->add_contact({u, v, t}); */
//     /* }; */

//     ttc index1(4, 64, 1);
//     ttc_simple index2(4, 64, 1);

//     insert(&index1, &index2, 0, 2, 6, 31);
//     insert(&index1, &index2, 0, 2, 39, 43);
//     insert(&index1, &index2, 0, 2, 44, 47);
//     insert(&index1, &index2, 0, 2, 50, 53);
//     insert(&index1, &index2, 1, 2, 61, 64);
//     insert(&index1, &index2, 0, 1, 59, 63);
//     insert(&index1, &index2, 2, 3, 51, 54);

//     assert(index1.can_reach(0, 2, {19, 23})
//            == index2.can_reach(0, 2, {19, 23}));
// }

// void test5()
// {
//     using interval = tgraph_reachability::interval;
//     using rttree = tgraph_reachability::detail::rttree<uint32_t>;
//     using rttree_simple = tgraph_reachability::detail::rttree_simple<uint32_t>;

//     auto insert = [](auto* t1, auto* t2, interval interv, uint32_t succ)
//     {
//         t2->insert(interv, succ);
//         t1->insert(interv, succ);
//         assert(t2->is_equal(*t1));
//         assert(t2->all_reachable(*t1));
//     };

//     rttree t021;
//     rttree t121;
//     rttree t011;
//     rttree t231;
//     rttree t031;
//     rttree_simple t022;
//     rttree_simple t122;
//     rttree_simple t012;
//     rttree_simple t232;
//     rttree_simple t032;

//     insert(&t021, &t022, {6, 7}, 2);
//     insert(&t021, &t022, {7, 8}, 2);
//     insert(&t021, &t022, {8, 9}, 2);
//     insert(&t021, &t022, {9, 10}, 2);
//     insert(&t021, &t022, {10, 11}, 2);
//     insert(&t021, &t022, {11, 12}, 2);
//     insert(&t021, &t022, {12, 13}, 2);
//     insert(&t021, &t022, {13, 14}, 2);
//     insert(&t021, &t022, {14, 15}, 2);
//     insert(&t021, &t022, {15, 16}, 2);
//     insert(&t021, &t022, {16, 17}, 2);
//     insert(&t021, &t022, {17, 18}, 2);
//     insert(&t021, &t022, {18, 19}, 2);
//     insert(&t021, &t022, {19, 20}, 2);
//     insert(&t021, &t022, {20, 21}, 2);
//     insert(&t021, &t022, {21, 22}, 2);
//     insert(&t021, &t022, {22, 23}, 2);
//     insert(&t021, &t022, {23, 24}, 2);
//     insert(&t021, &t022, {24, 25}, 2);
//     insert(&t021, &t022, {25, 26}, 2);
//     insert(&t021, &t022, {26, 27}, 2);
//     insert(&t021, &t022, {27, 28}, 2);
//     insert(&t021, &t022, {28, 29}, 2);
//     insert(&t021, &t022, {29, 30}, 2);
//     insert(&t021, &t022, {30, 31}, 2);
//     insert(&t021, &t022, {39, 40}, 2);
//     insert(&t021, &t022, {40, 41}, 2);
//     insert(&t021, &t022, {41, 42}, 2);
//     insert(&t021, &t022, {42, 43}, 2);
//     insert(&t021, &t022, {44, 45}, 2);
//     insert(&t021, &t022, {45, 46}, 2);
//     insert(&t021, &t022, {46, 47}, 2);
//     insert(&t021, &t022, {50, 51}, 2);
//     insert(&t021, &t022, {51, 52}, 2);
//     insert(&t021, &t022, {52, 53}, 2);
//     insert(&t121, &t122, {61, 62}, 2);
//     insert(&t121, &t122, {62, 63}, 2);
//     insert(&t121, &t122, {63, 64}, 2);
//     insert(&t011, &t012, {59, 60}, 1);
//     insert(&t021, &t022, {59, 62}, 1);
//     insert(&t011, &t012, {60, 61}, 1);
//     insert(&t021, &t022, {60, 62}, 1);
//     /* insert(&t011, &t012, {61, 62}, 1); */
//     /* insert(&t021, &t022, {61, 63}, 1); */
//     /* insert(&t011, &t012, {62, 63}, 1); */
//     /* insert(&t021, &t022, {62, 64}, 1); */
//     /* insert(&t231, &t232, {51, 52}, 3); */
//     /* insert(&t031, &t032, {50, 52}, 2); */
//     /* insert(&t231, &t232, {52, 53}, 3); */
//     /* insert(&t031, &t032, {51, 53}, 2); */
//     /* insert(&t231, &t232, {53, 54}, 3); */
//     /* insert(&t031, &t032, {52, 54}, 2); */
// }
