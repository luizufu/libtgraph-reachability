#include <libgraph-structures/adj-matrix.hxx>
#include <libtgraph-reachability/rttree-compact.hxx>
#include <libtgraph-reachability/rttree-compact2.hxx>
#include <libtgraph-reachability/ttc.hxx>

#include <iostream>

void print(auto interv)
{
    if(interv)
        std::cout << interv->first << '(' << interv->second << std::endl;
    else
        std::cout << "no" << std::endl;
}

auto main() -> int
{
tgraph_reachability::rttree_succinct2<uint32_t> tree;

    tree.insert({1, 2}, 0);
    // tree.insert({1, 200}, 0);
    // tree.insert({1, 222}, 0);
    // index.add_contact({0, 100, 200});
}
