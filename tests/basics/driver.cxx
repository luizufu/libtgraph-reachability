#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>
#include <libtgraph-reachability/baseline.hxx>
#include <libtgraph-reachability/ttc.hxx>
#include <libtgraph-reachability/rttree.hxx>
#include <libtgraph-reachability/rttree-simple.hxx>
#include <libtgraph-reachability/rttree-compact.hxx>
#include <libtgraph-reachability/rttree-compact2.hxx>
#include <libtgraph-reachability/version.hxx>

#include <iostream>
#include <cassert>

template<typename Ttc>
void test();

auto main (int  /*argc*/, char * /*argv*/[]) -> int
{
    using namespace tgraph_reachability;
    using namespace graph_structures;
    test<baseline>();
    test<ttc<adj_matrix, rttree>>();
    test<ttc<adj_matrix, rttree_simple>>();
    test<ttc<adj_matrix, rttree_succinct>>();
    test<ttc<adj_matrix, rttree_sparse>>();
    test<ttc<adj_matrix, rttree_succinct2>>();
    test<ttc<adj_matrix, rttree_sparse2>>();
    
    return 0;
}


template<typename T>
constexpr auto type_name() noexcept
{
    std::string_view name = "Error: unsupported compiler";
    std::string_view prefix;
    std::string_view suffix;
#ifdef __clang__
    name = __PRETTY_FUNCTION__;
    prefix = "auto type_name() [T = ";
    suffix = "]";
#elif defined(__GNUC__)
    name = __PRETTY_FUNCTION__;
    prefix = "constexpr auto type_name() [with T = ";
    suffix = "]";
#elif defined(_MSC_VER)
    name = __FUNCSIG__;
    prefix = "auto __cdecl type_name<";
    suffix = ">(void) noexcept";
#endif
    name.remove_prefix(prefix.size());
    name.remove_suffix(suffix.size());
    return name;
}

template<typename Ttc>
void test_true_journey(const Ttc& index, uint32_t u, uint32_t v, uint32_t t1,
                       uint32_t t2)
{
    auto j = index.reconstruct_journey(u, v, {t1, t2});
    /* std::cout << "rec(" << u << ", " << v << ", [" << t1 << ", " << t2 << "]
     * = " << *j << std::endl; */
    assert(j.has_value());
    assert(j->from() == u);
    assert(j->to() == v);
    assert(t1 <= j->departure());
    assert(j->arrival(index.delta()) < t2);

    const auto& cs = j->contacts();
    for(size_t i = 1; i < cs.size(); ++i)
    {
        uint32_t t1 = cs[i - 1].t;
        uint32_t t2 = cs[i].t;
        assert(t1 + index.delta() <= t2);
    }
}

template<typename Ttc>
void test_false_journey(const Ttc& index, uint32_t u, uint32_t v, uint32_t t1,
                        uint32_t t2)
{
    auto j = index.reconstruct_journey(u, v, {t1, t2});
    assert(!j.has_value());
}

template<typename Ttc>
void test_basic()
{
    std::cout << "Testing basic; " << type_name<Ttc>() << std::endl;

    const uint32_t a = 0;
    const uint32_t b = 1;
    const uint32_t c = 2;
    const uint32_t d = 3;
    const uint32_t e = 4;
    const uint32_t f = 5;
    const uint32_t g = 6;

    Ttc index(7, 12, 1);

    index.add_contact({a, b, 1});
    index.add_contact({b, c, 3});
    index.add_contact({c, d, 5});
    index.add_contact({a, e, 4});
    index.add_contact({e, d, 9});
    index.add_contact({a, f, 6});
    index.add_contact({f, g, 6});
    index.add_contact({g, d, 6});
    index.add_contact({a, b, 2});
    index.add_contact({b, c, 4});
    index.add_contact({c, d, 6});
    index.add_contact({a, e, 5});
    index.add_contact({e, d, 10});
    index.add_contact({a, f, 7});
    index.add_contact({f, g, 7});
    index.add_contact({g, d, 7});
    index.add_contact({a, f, 8});
    index.add_contact({f, g, 8});
    index.add_contact({g, d, 8});
    index.add_contact({g, d, 9});

    assert(index.can_reach(a, b, {0, 12}));
    assert(index.can_reach(a, c, {0, 12}));
    assert(index.can_reach(a, d, {0, 12}));
    assert(index.can_reach(a, f, {0, 12}));
    assert(index.can_reach(a, g, {0, 12}));
    assert(index.can_reach(b, c, {0, 12}));
    assert(index.can_reach(b, d, {0, 12}));
    assert(index.can_reach(e, d, {0, 12}));
    assert(index.can_reach(f, g, {0, 12}));
    assert(index.can_reach(f, d, {0, 12}));
    assert(index.can_reach(g, d, {0, 12}));
    assert(!index.can_reach(a, b, {4, 5}));
    assert(!index.can_reach(a, c, {5, 6}));
    assert(!index.can_reach(a, d, {20, 40}));
    assert(!index.can_reach(a, f, {1, 5}));
    assert(!index.can_reach(a, g, {1, 5}));
    assert(!index.can_reach(b, c, {0, 2}));
    assert(!index.can_reach(b, d, {0, 4}));
    assert(!index.can_reach(e, d, {5, 8}));
    assert(!index.can_reach(f, g, {1, 5}));
    assert(!index.can_reach(f, d, {1, 5}));
    assert(!index.can_reach(g, d, {1, 5}));

    test_true_journey(index, a, d, 0, 12);
    test_true_journey(index, a, d, 4, 12);
    test_true_journey(index, a, d, 6, 12);
    test_true_journey(index, a, d, 7, 12);
    test_false_journey(index, a, d, 8, 12);
}

template<typename Ttc>
void test_two_jumps()
{
    std::cout << "Testing two jumps; " << type_name<Ttc>() << std::endl;

    const uint32_t a = 0;
    const uint32_t b = 1;
    const uint32_t c = 2;

    Ttc index(3, 4, 1);

    index.add_contact({a, b, 0});
    index.add_contact({a, b, 1});
    index.add_contact({a, b, 2});
    index.add_contact({a, b, 3});
    index.add_contact({c, b, 0});
    index.add_contact({c, b, 1});
    index.add_contact({c, a, 2});

    assert(index.can_reach(c, b, {2, 5}));

    test_true_journey(index, c, b, 2, 5);
}

template<typename Ttc>
void test_find_better_journey()
{
    std::cout << "Testing better_journey; " << type_name<Ttc>() << std::endl;

    const uint32_t a = 0;
    const uint32_t b = 1;
    const uint32_t c = 2;
    const uint32_t d = 3;

    Ttc index(4, 20, 1);

    index.add_contact({a, b, 0});
    index.add_contact({a, b, 1});
    index.add_contact({a, b, 2});
    index.add_contact({a, b, 3});
    index.add_contact({b, c, 5});
    index.add_contact({b, c, 6});
    index.add_contact({b, c, 7});
    index.add_contact({b, c, 8});
    index.add_contact({c, d, 10});
    index.add_contact({c, d, 11});
    index.add_contact({c, d, 12});
    index.add_contact({c, d, 13});
    index.add_contact({a, d, 5});

    assert(index.can_reach(a, d, {5, 7}));
    test_true_journey(index, a, d, 5, 7);
}

template<typename Ttc>
void test_baseline_error()
{
    std::cout << "Testing baseline_error; " << type_name<Ttc>() << std::endl;

    Ttc index(16, 32, 1);

    auto insert =
        [](auto* index, uint32_t u, uint32_t v, uint32_t start, uint32_t end)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            index->add_contact({u, v, t});
        }
    };

    insert(&index, 0, 3, 0, 32);
    insert(&index, 9, 1, 0, 15);
    insert(&index, 4, 1, 0, 32);
    insert(&index, 9, 8, 0, 32);
    insert(&index, 3, 2, 0, 14);
    insert(&index, 0, 2, 0, 32);
    insert(&index, 8, 3, 0, 7);
    insert(&index, 4, 0, 0, 32);
    insert(&index, 5, 1, 0, 32);
    insert(&index, 12, 6, 0, 32);
    insert(&index, 11, 1, 0, 32);
    insert(&index, 12, 1, 0, 28);
    insert(&index, 1, 0, 0, 32);

    assert(index.can_reach(9, 8, {13, 28}));
    test_true_journey(index, 9, 8, 13, 28);
}

template<typename Ttc>
void test_delete_multiple_labels()
{
    std::cout << "Testing delete_mutiple; " << type_name<Ttc>() << std::endl;

    Ttc index(4, 3, 1);

    index.add_contact({0, 1, 1});
    index.add_contact({0, 1, 2});
    index.add_contact({0, 1, 0});
    index.add_contact({1, 2, 2});
    index.add_contact({1, 2, 3});
    index.add_contact({1, 2, 1});
    index.add_contact({2, 3, 3});
    index.add_contact({2, 3, 4});
    index.add_contact({2, 3, 2});

    assert(index.can_reach(0, 3, {0, 4}));
    test_true_journey(index, 0, 3, 0, 4);

    index.add_contact({0, 3, 0});

    assert(index.can_reach(0, 3, {0, 2}));
    test_true_journey(index, 0, 3, 0, 2);
}

template<typename Ttc>
void test_tc_error()
{
    std::cout << "Testing tc_error1; " << type_name<Ttc>() << std::endl;

    auto insert =
        [](auto* index, uint32_t u, uint32_t v, uint32_t start, uint32_t end)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            index->add_contact({u, v, t});
        }
    };

    Ttc index(3, 4, 1);

    insert(&index, 0, 2, 0, 4);
    insert(&index, 1, 0, 0, 4);
    insert(&index, 1, 2, 0, 4);

    assert(index.can_reach(1, 2, {2, 4}));
    test_true_journey(index, 1, 2, 2, 4);
}

template<typename Ttc>
void test_tc_error2()
{
    std::cout << "Testing tc_error2; " << type_name<Ttc>() << std::endl;

    auto insert =
        [](auto* index, uint32_t u, uint32_t v, uint32_t start, uint32_t end)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            index->add_contact({u, v, t});
        }
    };

    Ttc index(32, 32, 1);

    insert(&index, 1, 2, 0, 32);
    insert(&index, 2, 3, 0, 32);
    insert(&index, 3, 5, 0, 32);
    insert(&index, 1, 3, 0, 32);
    insert(&index, 15, 16, 0, 32);
    insert(&index, 16, 2, 0, 30);
    insert(&index, 15, 1, 0, 32);
    insert(&index, 16, 3, 0, 32);
    insert(&index, 0, 1, 0, 28);

    /*     insert(&index, 14, 0, 0, 32); */
    /*     insert(&index, 14, 1, 0, 26); */
    /*     insert(&index, 1, 2, 0, 32); */
    /*     insert(&index, 18, 2, 0, 32); */
    /*     insert(&index, 9, 10, 0, 32); */
    /*     insert(&index, 7, 3, 0, 32); */
    /*     insert(&index, 2, 0, 0, 5); */
    /*     insert(&index, 2, 3, 0, 32); */
    /*     insert(&index, 3, 5, 0, 32); */
    /*     insert(&index, 0, 4, 0, 32); */
    /*     insert(&index, 9, 7, 0, 32); */
    /*     insert(&index, 27, 2, 0, 32); */
    /*     insert(&index, 27, 1, 0, 32); */
    /*     insert(&index, 8, 2, 0, 32); */
    /*     insert(&index, 5, 0, 0, 32); */
    /*     insert(&index, 4, 5, 0, 32); */
    /*     insert(&index, 24, 1, 0, 21); */
    /*     insert(&index, 21, 5, 0, 32); */
    /*     insert(&index, 25, 4, 17, 32); */
    /*     insert(&index, 1, 3, 0, 32); */
    /*     insert(&index, 6, 4, 0, 32); */
    /*     insert(&index, 23, 0, 0, 32); */
    /*     insert(&index, 8, 1, 0, 23); */
    /*     insert(&index, 16, 4, 0, 32); */
    /*     insert(&index, 25, 11, 0, 32); */
    /*     insert(&index, 11, 2, 0, 13); */
    /*     insert(&index, 25, 2, 0, 16); */
    /*     insert(&index, 6, 3, 0, 32); */
    /*     insert(&index, 8, 4, 0, 32); */
    /*     insert(&index, 26, 3, 0, 32); */
    /*     insert(&index, 12, 10, 0, 32); */
    /*     insert(&index, 12, 8, 0, 32); */
    /*     insert(&index, 5, 2, 0, 32); */
    /*     insert(&index, 12, 11, 0, 32); */
    /*     insert(&index, 10, 4, 0, 32); */
    /*     insert(&index, 7, 9, 0, 32); */
    /*     insert(&index, 13, 5, 0, 32); */
    /*     insert(&index, 15, 16, 0, 32); */
    /*     insert(&index, 15, 4, 0, 32); */
    /*     insert(&index, 19, 0, 0, 32); */
    /*     insert(&index, 16, 2, 0, 30); */
    /*     insert(&index, 15, 1, 0, 32); */
    /*     insert(&index, 16, 3, 0, 32); */
    /*     insert(&index, 10, 0, 0, 32); */
    /*     insert(&index, 9, 4, 0, 32); */
    /*     insert(&index, 0, 1, 0, 28); */
    /*  */
    assert(index.can_reach(0, 2, {19, 22}));
    test_true_journey(index, 0, 2, 19, 22);
}

template<typename Ttc>
void test_tc_error3()
{
    std::cout << "Testing tc_error3; " << type_name<Ttc>() << std::endl;

    auto insert =
        [](auto* index, uint32_t u, uint32_t v, uint32_t start, uint32_t end)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            index->add_contact({u, v, t});
        }
    };

    Ttc index(16, 64, 1);

    insert(&index, 3, 1, 31, 64);
    insert(&index, 0, 3, 0, 64);
    insert(&index, 9, 2, 0, 55);
    insert(&index, 1, 2, 0, 38);
    insert(&index, 5, 2, 0, 41);
    insert(&index, 3, 2, 0, 13);
    insert(&index, 7, 0, 14, 33);
    insert(&index, 12, 10, 0, 46);
    insert(&index, 5, 11, 47, 64);
    insert(&index, 0, 2, 39, 59);
    insert(&index, 11, 3, 0, 42);
    insert(&index, 6, 1, 0, 58);
    insert(&index, 9, 1, 26, 54);
    insert(&index, 10, 2, 0, 64);
    insert(&index, 8, 0, 0, 64);
    insert(&index, 6, 3, 0, 4);
    insert(&index, 6, 7, 0, 49);
    insert(&index, 11, 0, 0, 25);
    insert(&index, 11, 2, 0, 22);
    insert(&index, 3, 1, 10, 64); // here
    insert(&index, 3, 1, 0, 9);
    insert(&index, 9, 8, 23, 64);
    insert(&index, 6, 0, 0, 7);
    insert(&index, 2, 1, 0, 64);
    insert(&index, 0, 1, 0, 29);
    insert(&index, 8, 1, 0, 64);
    insert(&index, 11, 10, 52, 64);
    insert(&index, 10, 2, 57, 64);
    insert(&index, 10, 1, 0, 5);
    insert(&index, 10, 0, 0, 1);
    insert(&index, 9, 3, 0, 2);
    insert(&index, 11, 1, 0, 60);
    insert(&index, 1, 3, 0, 64);
    insert(&index, 7, 1, 0, 64); // here
    insert(&index, 4, 0, 0, 45);
    insert(&index, 8, 9, 0, 63);
    insert(&index, 12, 4, 0, 40);
    insert(&index, 9, 1, 0, 6);
    insert(&index, 12, 9, 0, 12);

    assert(!index.can_reach(12, 0, {13, 15}));
    test_false_journey(index, 12, 0, 13, 15);

    assert(index.can_reach(12, 0, {13, 16}));
    test_true_journey(index, 12, 0, 13, 16);
}

template<typename Ttc>
void test_tc_error4()
{
    std::cout << "Testing tc_error4; " << type_name<Ttc>() << std::endl;

    auto insert =
        [](auto* index, uint32_t u, uint32_t v, uint32_t start, uint32_t end)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            index->add_contact({u, v, t});
        }
    };

    Ttc index(5, 512, 1);

    insert(&index, 3, 0, 263, 512);
    insert(&index, 3, 2, 0, 203);
    insert(&index, 2, 0, 0, 255);
    insert(&index, 0, 1, 250, 326);
    insert(&index, 1, 4, 89, 381);

    assert(index.can_reach(3, 4, {234, 463}));
    test_true_journey(index, 3, 4, 234, 463);
}

template<typename Ttc>
void test_tc_error5()
{
    std::cout << "Testing tc_error5; " << type_name<Ttc>() << std::endl;

    auto insert =
        [](auto* index, uint32_t u, uint32_t v, uint32_t start, uint32_t end)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            index->add_contact({u, v, t});
        }
    };

    Ttc index(8, 32, 1);

    insert(&index, 6, 7, 19, 30);
    insert(&index, 5, 7, 21, 26);
    insert(&index, 5, 6, 9, 11);
    insert(&index, 5, 7, 28, 32);
    insert(&index, 5, 7, 0, 11);

    assert(index.can_reach(5, 7, {10, 19}));
}

template<typename Ttc>
void test_tc2_error1()
{
    std::cout << "Testing tc2_error1; " << type_name<Ttc>() << std::endl;

    auto insert =
        [](auto* index, uint32_t u, uint32_t v, uint32_t start, uint32_t end)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            index->add_contact({u, v, t});
        }
    };

    Ttc index(6, 16, 1);

    insert(&index, 0, 1, 0, 1);
    insert(&index, 2, 3, 2, 6);
    insert(&index, 4, 5, 11, 14);
    insert(&index, 3, 4, 5, 13);
    insert(&index, 1, 2, 0, 2);

    assert(index.can_reach(0, 5, {0, 13}));
}

template<typename Ttc>
void test_tc3_error1()
{
    std::cout << "Testing tc3_error1; " << type_name<Ttc>() << std::endl;

    auto insert =
        [](auto* index, uint32_t u, uint32_t v, uint32_t start, uint32_t end)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            index->add_contact({u, v, t});
        }
    };

    Ttc index(4, 4, 1);

    insert(&index, 0, 2, 1, 4);
    insert(&index, 1, 2, 3, 4);
    insert(&index, 0, 1, 0, 1);

    assert(index.can_reach(0, 2, {0, 3}));
}

template<typename Ttc>
void test_tc5_error1()
{
    std::cout << "Testing tc5_error1; " << type_name<Ttc>() << std::endl;

    Ttc index(4, 4, 1);
    index.add_contact({2, 3, 3});
    index.add_contact({1, 2, 2});
    index.add_contact({0, 1, 0});
    index.add_contact({0, 1, 1});
    index.add_contact({0, 1, 2});

    assert(!index.can_reach(0, 3, {2, 3}));
}

template<typename Ttc>
void test_tc6_error1()
{
    std::cout << "Testing tc6_error1; " << type_name<Ttc>() << std::endl;

    Ttc index(5, 5, 1);
    index.add_contact({2, 3, 2});
    index.add_contact({2, 3, 3});
    index.add_contact({3, 4, 4});
    index.add_contact({1, 2, 1});
    index.add_contact({1, 2, 2});
    index.add_contact({0, 1, 0});
    index.add_contact({0, 1, 1});
    index.add_contact({0, 2, 1});
    index.add_contact({0, 2, 2});

    assert(index.can_reach(0, 4, {2, 6}));
}

template<typename Ttc>
void test_tc6_error2()
{
    std::cout << "Testing tc6_error2; " << type_name<Ttc>() << std::endl;

    Ttc index(4, 4, 1);
    index.add_contact({1, 2, 1});
    index.add_contact({1, 2, 2});
    index.add_contact({0, 1, 0});
    index.add_contact({0, 1, 1});
    index.add_contact({2, 3, 1});
    index.add_contact({2, 3, 2});
    index.add_contact({2, 3, 3});
    index.add_contact({0, 2, 1});
    index.add_contact({0, 2, 0});

    assert(index.can_reach(0, 3, {1, 5}));
}

template<typename Ttc>
void test_tc6_error3()
{
    std::cout << "Testing tc6_error3; " << type_name<Ttc>() << std::endl;

    Ttc index(5, 7, 1);
    index.add_contact({0, 2, 0});
    index.add_contact({1, 3, 4});
    index.add_contact({3, 4, 2});
    index.add_contact({3, 4, 5});
    index.add_contact({1, 4, 6});
    index.add_contact({0, 1, 0});
    index.add_contact({0, 1, 4});
    index.add_contact({0, 3, 4});
    index.add_contact({2, 3, 1});

    assert(index.can_reach(0, 4, {3, 7}));
}

template<typename Ttc>
void test_tc6_error4()
{
    std::cout << "Testing tc6_error4; " << type_name<Ttc>() << std::endl;

    Ttc index(5, 5, 1);

    index.add_contact({3, 4, 4});
    index.add_contact({1, 2, 1});
    index.add_contact({2, 3, 3});
    index.add_contact({2, 3, 2});
    index.add_contact({0, 1, 0});

    assert(index.can_reach(0, 4, {0, 6}));
}

template<typename Ttc>
void test_tc7_error1()
{
    std::cout << "Testing tc7_error1; " << type_name<Ttc>() << std::endl;

    Ttc index(5, 4, 1);

    index.add_contact({2, 3, 2});
    index.add_contact({1, 2, 1});
    index.add_contact({3, 4, 3});
    index.add_contact({1, 3, 1});
    index.add_contact({0, 1, 0});

    assert(index.can_reach(0, 4, {0, 5}));
}

template<typename Ttc>
void test_tc7_error2()
{
    std::cout << "Testing tc7_error2; " << type_name<Ttc>() << std::endl;

    Ttc index(6, 6, 1);

    index.add_contact({4, 5, 5});
    index.add_contact({3, 4, 3});
    index.add_contact({3, 4, 4});
    index.add_contact({2, 3, 2});
    index.add_contact({2, 3, 3});
    index.add_contact({1, 2, 1});
    index.add_contact({0, 1, 0});

    assert(index.can_reach(0, 5, {0, 7}));
}

template<typename Ttc>
void test_bst_error1()
{
    std::cout << "Testing bst_error1; " << type_name<Ttc>() << std::endl;

    auto insert =
        [](auto* index, uint32_t u, uint32_t v, uint32_t start, uint32_t end)
    {
        for(uint32_t t = start; t < end; ++t)
        {
            index->add_contact({u, v, t});
        }
    };

    Ttc index(8, 16, 1);
    index.add_contact({0, 2, 1});
    index.add_contact({0, 2, 8});
    index.add_contact({0, 3, 4});
    index.add_contact({1, 3, 10});
    index.add_contact({2, 4, 0});
    index.add_contact({2, 4, 2});
    index.add_contact({2, 4, 13});
    index.add_contact({3, 4, 10});
    index.add_contact({3, 4, 11});

    index.add_contact({0, 1, 7});
    index.add_contact({0, 1, 9});
    index.add_contact({2, 3, 9});

    assert(index.can_reach(0, 4, {2, 13}));
}

template <typename Ttc>
void test()
{

    test_basic<Ttc>();
    test_two_jumps<Ttc>();
    test_find_better_journey<Ttc>();
    test_baseline_error<Ttc>();
    test_delete_multiple_labels<Ttc>();
    test_tc_error<Ttc>();
    test_tc_error2<Ttc>();
    test_tc_error3<Ttc>();
    test_tc_error4<Ttc>();
    test_tc_error5<Ttc>();
    test_tc2_error1<Ttc>();
    test_tc3_error1<Ttc>();
    test_tc5_error1<Ttc>();
    test_tc6_error1<Ttc>();
    test_tc6_error2<Ttc>();
    test_tc6_error3<Ttc>();
    test_tc6_error4<Ttc>();
    test_tc7_error1<Ttc>();
    test_tc7_error2<Ttc>();
    test_tc7_error2<Ttc>();
    test_bst_error1<Ttc>();
    test_tc7_error2<Ttc>();
    test_bst_error1<Ttc>();
}
