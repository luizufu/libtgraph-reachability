#pragma once

#include <libtgraph-reachability/journey.hxx>
#include <optional>
#include <iostream>
#include <unordered_map>

namespace tgraph_reachability
{
template<template<typename> class Graph, template<typename> class Tree>
class ttc
{
public:
    ttc(uint32_t n, uint32_t tau, uint32_t delta)
        : _g(n)
        , _tau(tau)
        , _delta(delta)
    {
    }

    template<typename It>
    ttc(uint32_t n, uint32_t tau, uint32_t delta, It begin_contacts,
        It end_contacts)
        : _g(n)
        , _tau(tau)
        , _delta(delta)
    {
        for(auto it = begin_contacts; it != end_contacts; ++it)
        {
            const auto& [u, v, t] = *it;

            if(u == v || t >= _tau)
            {
                continue;
            }

            if(tree(u, v)->insert({t, t + _delta}, v))
            {
                for(auto& [wminus, tr]: _g.in_neighbors(u))
                {
                    if(wminus == v)
                    {
                        continue;
                    }

                    if(auto res = tr.find_prev(t))
                    {
                        tree(wminus, v)->insert({res->first.left, t + _delta},
                                                  res->second);
                    }
                }
            }
        }
    }

    void add_contact(contact c);

    [[nodiscard]] auto can_reach(uint32_t u, uint32_t v, interval i) const
        -> bool;

    [[nodiscard]] auto reconstruct_journey(uint32_t u, uint32_t v,
                                           interval i) const
        -> std::optional<journey>;

    [[nodiscard]] auto tau() const -> uint32_t
    {
        return _tau;
    }

    [[nodiscard]] auto delta() const -> uint32_t
    {
        return _delta;
    }

#ifdef BENCHMARK_ON
    static auto stack_usage() -> uint64_t;
    static auto heap_usage() -> uint64_t;
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tree_inserts() -> uint64_t;
    static auto tree_erases() -> uint64_t;
    static auto journey_duration_frequency()
        -> const std::unordered_map<uint32_t, uint32_t>&;
    static void reset_benchmark();
#endif

private:
    auto tree(uint32_t u, uint32_t v) -> Tree<uint32_t>*;

    Graph<Tree<uint32_t>> _g;
    uint32_t _tau;
    uint32_t _delta;
};

} // namespace tgraph_reachability
