#pragma once

#include <libtgraph-reachability/journey.hxx>

#include <vector>
#include <iostream>
#include <optional>

namespace tgraph_reachability
{
class ttc2
{
public:
    ttc2(uint32_t n, uint32_t tau, uint32_t delta);

    template<typename It>
    ttc2(uint32_t n, uint32_t tau, uint32_t delta, It begin_contacts,
         It end_contacts)
        : ttc2(n, tau, delta)
    {
        /*     for(auto it = begin_contacts; it != end_contacts; ++it) */
        /*     { */
        /*         const auto& [u, v, t] = *it; */

        /*         if(u == v || t >= _tau) */
        /*         { */
        /*             return; */
        /*         } */

        /*         if(rttree(u, v)->insert(t, t + _delta, v)) */
        /*         { */
        /*             for(auto& [wminus, tree]: _g.in_neighbors(u)) */
        /*             { */
        /*                 if(wminus == v) */
        /*                 { */
        /*                     continue; */
        /*                 } */

        /*                 if(auto res = tree.find_prev(t)) */
        /*                 { */
        /*                     rttree(wminus, v)->insert(res->first.first, t +
         * _delta, */
        /*                                               res->second); */
        /*                 } */
        /*             } */
        /*         } */
        /*     } */
    }

    void add_contact(contact c);

    [[nodiscard]] auto can_reach(uint32_t u, uint32_t v, interval i) const
        -> bool;

    [[nodiscard]] auto reconstruct_journey(uint32_t u, uint32_t v,
                                           interval i) const
        -> std::optional<journey>;

    [[nodiscard]] auto tau() const -> uint32_t
    {
        return _tau;
    }

    [[nodiscard]] auto delta() const -> uint32_t
    {
        return _delta;
    }

#ifdef BENCHMARK_ON
    static auto stack_usage() -> uint64_t;
    static auto heap_usage() -> uint64_t;
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tree_inserts() -> uint64_t;
    static auto tree_erases() -> uint64_t;
    static void reset_benchmark();
#endif

private:
    class data
    {
        struct entry
        {
            uint32_t tminus;
            uint32_t tplus;
            uint32_t succ;
        };

    public:
        data(uint32_t n, uint32_t tau, uint32_t delta);

        auto insert(uint32_t u, uint32_t v, uint32_t t1, uint32_t t2,
                    uint32_t succ) -> bool;

        [[nodiscard]] auto find_prev(uint32_t u, uint32_t v, uint32_t t) const
            -> std::optional<entry>;

        [[nodiscard]] auto find_next(uint32_t u, uint32_t v, uint32_t t) const
            -> std::optional<entry>;

        void update_pointers(uint32_t u, uint32_t v, uint32_t t1, uint32_t t2);

    private:
        auto in_v(uint32_t u, uint32_t v, uint32_t t)
            -> std::pair<uint32_t, uint32_t>&;
        [[nodiscard]] auto in_v(uint32_t u, uint32_t v, uint32_t t) const
            -> const std::pair<uint32_t, uint32_t>&;

        auto out_v(uint32_t u, uint32_t v, uint32_t t)
            -> std::pair<uint32_t, uint32_t>&;
        [[nodiscard]] auto out_v(uint32_t u, uint32_t v, uint32_t t) const
            -> const std::pair<uint32_t, uint32_t>&;

        auto in_t(uint32_t u, uint32_t v, uint32_t t) -> uint32_t&;
        [[nodiscard]] auto in_t(uint32_t u, uint32_t v, uint32_t t) const
            -> const uint32_t&;

        auto out_t(uint32_t u, uint32_t v, uint32_t t) -> uint32_t&;
        [[nodiscard]] auto out_t(uint32_t u, uint32_t v, uint32_t t) const
            -> const uint32_t&;

        std::vector<std::pair<uint32_t, uint32_t>> _in_v;
        std::vector<std::pair<uint32_t, uint32_t>> _out_v;
        std::vector<uint32_t> _in_t;
        std::vector<uint32_t> _out_t;
        uint32_t _n;
        uint32_t _tau;
        uint32_t _delta;
    };

    data _data;
    uint32_t _n;
    uint32_t _tau;
    uint32_t _delta;
};

} // namespace tgraph_reachability
