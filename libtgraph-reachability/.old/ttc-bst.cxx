#include <libtgraph-reachability/ttc-bst.hxx>

#include <queue>
#include <algorithm>

namespace tgraph_reachability
{
template<template<typename> class Graph>
auto ttc_bst<Graph>::ttc_bst::rtbst(uint32_t u, uint32_t v)
    -> detail::rtbst<uint32_t>*
{
    if(detail::rtbst<uint32_t>* tree = _g.label(u, v))
    {
        return tree;
    }

    _g.insert_edge(u, v, detail::rtbst<uint32_t>(_tau));
    return _g.label(u, v);
}

template<template<typename> class Graph>
ttc_bst<Graph>::ttc_bst(uint32_t n, uint32_t tau, uint32_t delta)
    : _g(n)
    , _tau(tau)
    , _delta(delta)
{
}

template<template<typename> class Graph>
void ttc_bst<Graph>::add_contact(contact c)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= _tau)
    {
        return;
    }

    if(rtbst(u, v)->insert({t, t + _delta}, v))
    {
        std::vector<std::pair<uint32_t, uint32_t>> d;

        for(auto& [wplus, tree]: _g.out_neighbors(v))
        {
            if(wplus == u)
            {
                continue;
            }

            if(auto res = tree.find_next_departure(t + _delta))
            {
                uint32_t tplus = res->first.right;
                if(rtbst(u, wplus)->insert({t, tplus}, v))
                {
                    d.emplace_back(wplus, tplus);
                }
            }
        }

        for(auto& [wminus, tree]: _g.in_neighbors(u))
        {
            if(wminus == v)
            {
                continue;
            }

            if(auto res = tree.find_prev_arrival(t))
            {
                uint32_t tminus = res->first.left;
                uint32_t succ = res->second;
                if(rtbst(wminus, v)->insert({tminus, t + _delta}, succ))
                {
                    for(const auto& [wplus, tplus]: d)
                    {
                        if(wplus == wminus)
                        {
                            continue;
                        }

                        rtbst(wminus, wplus)->insert({tminus, tplus}, succ);
                    }
                }
            }
        }
    }
}

template<template<typename> class Graph>
auto ttc_bst<Graph>::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    if(const auto* tree = _g.label(u, v))
    {
        if(i.left == 0 && i.right == _tau)
        {
            return true;
        }

        if(auto res = tree->find_next_departure(i.left))
        {
            uint32_t tplus = res->first.right;
            return std::min(tplus, _tau + _delta - 1) < i.right;
        }
    }

    return false;
}

template<template<typename> class Graph>
auto ttc_bst<Graph>::reconstruct_journey(uint32_t u, uint32_t v,
                                         interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return {};
    }

    const auto* tree = _g.label(u, v);

    if(tree == nullptr)
    {
        return {};
    }

    auto res = tree->find_next_departure(i.left);

    if(!res.has_value() || res->first.right >= i.right)
    {
        return {};
    }

    uint32_t succ = res->second;
    uint32_t tminus = res->first.left;

    journey j({u, succ, tminus});

    while(succ != v)
    {
        auto res2 = _g.label(succ, v)->find_next_departure(tminus + _delta);
        uint32_t next_succ = res2->second;
        uint32_t next_tminus = res2->first.left;

        j.push_back({succ, next_succ, next_tminus});

        succ = next_succ;
        tminus = next_tminus;
    }

    return j;
}

#ifdef BENCHMARK_ON
template<template<typename> class Graph>
auto ttc_bst<Graph>::stack_usage() -> uint64_t
{
    return detail::rtbst<uint32_t>::total_stack_usage
           + Graph<detail::rtbst<uint32_t>>::total_stack_usage;
}

template<template<typename> class Graph>
auto ttc_bst<Graph>::heap_usage() -> uint64_t
{
    return detail::rtbst<uint32_t>::total_heap_usage
           + Graph<detail::rtbst<uint32_t>>::total_heap_usage;
}

template<template<typename> class Graph>
auto ttc_bst<Graph>::binary_searches() -> uint64_t
{
    return detail::rtbst<uint32_t>::total_binary_searches
           + Graph<detail::rtbst<uint32_t>>::total_binary_searches;
}

template<template<typename> class Graph>
auto ttc_bst<Graph>::sequential_searches() -> uint64_t
{
    return detail::rtbst<uint32_t>::total_sequential_searches
           + Graph<detail::rtbst<uint32_t>>::total_sequential_searches;
}

template<template<typename> class Graph>
auto ttc_bst<Graph>::tree_inserts() -> uint64_t
{
    return detail::rtbst<uint32_t>::total_inserts;
}

template<template<typename> class Graph>
auto ttc_bst<Graph>::tree_erases() -> uint64_t
{
    return detail::rtbst<uint32_t>::total_erases;
}

template<template<typename> class Graph>
void ttc_bst<Graph>::reset_benchmark()
{
    detail::rtbst<uint32_t>::total_sequential_searches = 0;
    detail::rtbst<uint32_t>::total_binary_searches = 0;
    detail::rtbst<uint32_t>::total_inserts = 0;
    detail::rtbst<uint32_t>::total_erases = 0;

    Graph<detail::rtbst<uint32_t>>::total_sequential_searches = 0;
    Graph<detail::rtbst<uint32_t>>::total_binary_searches = 0;
}

#endif

} // namespace tgraph_reachability

#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>

template class tgraph_reachability::ttc_bst<graph_structures::adj_list>;
template class tgraph_reachability::ttc_bst<graph_structures::adj_matrix>;
template class tgraph_reachability::ttc_bst<graph_structures::edge_grid>;
