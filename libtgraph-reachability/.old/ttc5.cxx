#include <libtgraph-reachability/ttc5.hxx>

#include <queue>
#include <stack>
#include <algorithm>

namespace tgraph_reachability
{
template<template<typename> class Graph>
ttc5<Graph>::ttc5(uint32_t n, uint32_t tau, uint32_t delta)
    : _forest(n)
    , _pointers(n)
    , _n(n)
    , _tau(tau)
    , _delta(delta)
{
    for(uint32_t u = 0; u < _n; ++u)
    {
        _pointers.insert(u, u, _forest.root(u));
    }
}

template<template<typename> class Graph>
void ttc5<Graph>::add_contact(contact c)
{
    if(c.u == c.v || c.t >= _tau)
    {
        return;
    }

    auto uu = find_or_create(c.u, c.t);
    interval interv1 = {c.t, c.t + _delta};
    if(auto uv = insert(c.u, c.v, interv1, uu); uv != nullptr)
    {
        if(auto vv = _pointers.next(c.v, c.v, c.t + _delta);
           vv != _pointers.nend(c.v, c.v))
        {
            meld(c.u, uv, c.v, *vv);
        }

        for(auto& [wminus, _]: _pointers.in(c.u))
        {
            if(wminus == c.u || wminus == c.v)
            {
                continue;
            }

            if(auto wu = _pointers.prev(wminus, c.u, c.t);
               wu != _pointers.pend(wminus, c.u))
            {
                interval interv2 = {(*wu)->i.left, c.t + _delta};
                if(auto wv = insert(wminus, c.v, interv2, *wu); wv != *wu)
                {
                    meld(wminus, wv, c.u, uv);
                }
            }
        }
    }
}

template<template<typename> class Graph>
auto ttc5<Graph>::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    if(auto n = _pointers.prev(u, v, i.right - 1); n != _pointers.pend(u, v))
    {
        return (*n)->i.left >= i.left;
    }

    return false;
}

template<template<typename> class Graph>
auto ttc5<Graph>::reconstruct_journey(uint32_t u, uint32_t v, interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return {};
    }

    if(auto n = _pointers.prev(u, v, i.right - 1); n != _pointers.pend(u, v))
    {
        if((*n)->i.left >= i.left)
        {
            std::stack<contact> contacts;
            const node* cur = *n;

            while(cur->v != u)
            {
                contacts.push({cur->parent->v, cur->v, cur->i.right - _delta});
                cur = cur->parent;
            }

            journey j;
            while(!contacts.empty())
            {
                j.push_back(contacts.top());
                contacts.pop();
            }

            return j;
        }
    }

    return {};
}

#ifdef BENCHMARK_ON
template<template<typename> class Graph>
auto ttc5<Graph>::stack_usage() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*, by_departure>>::total_stack_usage */
    /*        + Graph<std::set<node*, by_arrival_rev>>::total_stack_usage;
     */
}

template<template<typename> class Graph>
auto ttc5<Graph>::heap_usage() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*, by_departure>>::total_heap_usage */
    /*        + Graph<std::set<node*, by_arrival_rev>>::total_heap_usage; */
}

template<template<typename> class Graph>
auto ttc5<Graph>::binary_searches() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*,
     * by_departure>>::total_binary_searches */
    /*        + Graph<std::set<node*,
     * by_arrival_rev>>::total_binary_searches;
     */
}

template<template<typename> class Graph>
auto ttc5<Graph>::sequential_searches() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*,
     * by_departure>>::total_sequential_searches */
    /*        + Graph<std::set<node*,
     * by_arrival_rev>>::total_sequential_searches; */
}

template<template<typename> class Graph>
auto ttc5<Graph>::tree_inserts() -> uint64_t
{
    return 0;
}

template<template<typename> class Graph>
auto ttc5<Graph>::tree_erases() -> uint64_t
{
    return 0;
}

template<template<typename> class Graph>
void ttc5<Graph>::reset_benchmark()
{
    /* Graph<std::set<node*, by_departure>>::total_sequential_searches = 0;
     */
    /* Graph<std::set<node*, by_departure>>::total_binary_searches = 0; */
    /* Graph<std::set<node*, by_arrival_rev>>::total_sequential_searches =
     * 0; */
    /* Graph<std::set<node*, by_arrival_rev>>::total_binary_searches = 0; */
}

#endif

template<template<typename> class Graph>
auto ttc5<Graph>::insert(uint32_t u, uint32_t v, interval interv, node* parent)
    -> node*
{
    if(auto collecteds = _pointers.collect_inside(u, v, interv))
    {
        auto n = _forest.create_node(u, v, interv, parent);
        for(node* collected: *collecteds)
        {
            _pointers.remove(u, v, collected);
            _forest.add_children(n, _forest.remove_node(collected));
        }

        _pointers.insert(u, v, n);
        return n;
    }

    return parent;
}

template<template<typename> class Graph>
void ttc5<Graph>::meld(uint32_t wminus, node* wu, uint32_t wplus, node* vw)
{
    struct parameters
    {
        auto operator<(const parameters& rhs) const -> bool
        {
            return v->i.left > rhs.v->i.left;
        }

        node* u = nullptr;
        node* v = nullptr;
    };

    std::priority_queue<parameters> min_heap;
    min_heap.push({.u = wu, .v = vw});

    while(!min_heap.empty())
    {
        auto p = min_heap.top();
        min_heap.pop();

        auto next_u =
            p.v->v == wplus
                ? p.u
                : insert(wminus, p.v->v, {p.u->i.left, p.v->i.right}, p.u);

        for(auto& next_v: p.v->children)
        {
            if(next_v)
            {
                min_heap.push({
                    .u = next_u,
                    .v = next_v.get(),
                });
            }
        }
    }
}

template<template<typename> class Graph>
ttc5<Graph>::forest::forest(uint32_t n)
{
    for(uint32_t i = 0; i < n; ++i)
    {
        _roots.push_back(std::make_unique<node>(
            node {.i = {0, 0}, .v = i, .parent = nullptr}));
        _roots.back()->children.push_back(nullptr);
    }
}

template<template<typename> class Graph>
auto ttc5<Graph>::forest::create_node(uint32_t u, uint32_t v, interval interv,
                                      node* parent) -> node*
{
    auto newnode =
        std::make_unique<node>(node {.i = interv, .v = v, .parent = parent});

    if(u != v)
    {
        parent->children.push_back(std::move(newnode));

        return parent->children.back().get();
    }

    newnode->children.push_back(std::move(parent->children[0]));
    parent->children[0] = std::move(newnode);

    return parent->children[0].get();
}

template<template<typename> class Graph>
auto ttc5<Graph>::forest::remove_node(node* n)
    -> std::vector<std::unique_ptr<node>>
{
    auto children = std::move(n->children);

    if(n->parent != nullptr)
    {
        auto& pchildren = n->parent->children;
        for(uint32_t i = 0; i < pchildren.size(); ++i)
        {
            if(pchildren[i].get() == n)
            {
                pchildren.erase(pchildren.begin() + i);
                break;
            }
        }
    }

    return children;
}

template<template<typename> class Graph>
void ttc5<Graph>::forest::add_children(
    node* n, std::vector<std::unique_ptr<node>>&& children)
{
    for(auto& child: children)
    {
        child->parent = n;
        n->children.push_back(std::move(child));
    }
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::collect_inside(uint32_t u, uint32_t v,
                                            interval interv)
    -> std::optional<std::vector<node*>>
{
    std::vector<node*> inside;

    if(empty(u, v))
    {
        return inside;
    }

    // get boundary elements
    auto it1 = prev(u, v, interv.right);
    if(it1 != pend(u, v) && interv.left <= (*it1)->i.left)
    {
        return std::nullopt;
    }

    node q2 = {.i = {.left = interv.left}};
    auto it2 = next(u, v, interv.left);
    if(it2 != nend(u, v) && interv.right >= (*it2)->i.right)
    {
        return std::nullopt;
    }

    auto it = it1 == pend(u, v) ? prbegin(u, v)
              : (*it1)->i.right < interv.right
                  ? std::reverse_iterator(it1)
                  : std::reverse_iterator(std::next(it1));

    auto end = it2 == nend(u, v)              ? it2
               : (*it2)->i.left > interv.left ? it2
                                              : std::next(it2);

    while(!(end == nend(u, v) && it == prend(u, v))
          && !((end != nend(u, v) && it != prend(u, v) && *it == *end)))
    {
        inside.push_back(*it);
        ++it;
    }

    return inside;
}

template<template<typename> class Graph>
auto ttc5<Graph>::forest::root(uint32_t i) -> node*
{
    return _roots[i].get();
}

template<template<typename> class Graph>
ttc5<Graph>::pred_succ::pred_succ(uint32_t n)
    : _desc(n)
    , _asc(n)
{
}

template<template<typename> class Graph>
void ttc5<Graph>::pred_succ::insert(uint32_t u, uint32_t v, node* n)
{
    if(_desc.label(v, u) == nullptr)
    {
        _desc.insert_edge(v, u, std::set<node*, by_arrival_rev> {});
        _asc.insert_edge(u, v, std::set<node*, by_departure> {});
    }

    _desc.label(v, u)->insert(n);
    _asc.label(u, v)->insert(n);
}

template<template<typename> class Graph>
void ttc5<Graph>::pred_succ::remove(uint32_t u, uint32_t v, node* n)
{
    if(_desc.label(v, u) != nullptr)
    {
        _desc.label(v, u)->erase(n);
        _asc.label(u, v)->erase(n);
    }
}

template<template<typename> class Graph>
auto ttc5<Graph>::find_or_create(uint32_t v, uint32_t t) -> node*
{
    auto vv = _pointers.prev(v, v, t);

    if((*vv)->i.left == t)
    {
        return *vv;
    }

    node* n = _forest.create_node(v, v, {t, t}, *vv);
    _pointers.insert(v, v, n);

    return n;
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::in(uint32_t u) ->
    typename Graph<std::set<node*, by_arrival_rev>>::vertex_stream
{
    return _desc.out_neighbors(u);
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::desc(uint32_t u, uint32_t v)
    -> std::set<node*, by_arrival_rev>*
{
    if(auto* nodes = _desc.label(v, u))
    {
        return nodes;
    }

    _desc.insert_edge(v, u, std::set<node*, by_arrival_rev> {});
    _asc.insert_edge(u, v, std::set<node*, by_departure> {});
    return _desc.label(v, u);
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::desc(uint32_t u, uint32_t v) const
    -> const std::set<node*, by_arrival_rev>*
{
    if(auto* nodes = _desc.label(v, u))
    {
        return nodes;
    }

    _desc.insert_edge(v, u, std::set<node*, by_arrival_rev> {});
    _asc.insert_edge(u, v, std::set<node*, by_departure> {});
    return _desc.label(v, u);
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::prev(uint32_t u, uint32_t v, uint32_t t) const ->
    typename std::set<node*, by_arrival_rev>::const_iterator
{
    auto nodes = desc(u, v);
    if(nodes == nullptr)
    {
        return nodes->end();
    }

    node q = {.i = {.right = t}};
    return nodes->lower_bound(&q);
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::pend(uint32_t u, uint32_t v) const ->
    typename std::set<node*, by_arrival_rev>::const_iterator
{
    return desc(u, v)->end();
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::prbegin(uint32_t u, uint32_t v) const ->
    typename std::set<node*, by_arrival_rev>::const_reverse_iterator
{
    return desc(u, v)->rbegin();
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::prend(uint32_t u, uint32_t v) const ->
    typename std::set<node*, by_arrival_rev>::const_reverse_iterator
{
    return desc(u, v)->rend();
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::asc(uint32_t u, uint32_t v) const
    -> const std::set<node*, by_departure>*
{
    if(auto* nodes = _asc.label(u, v))
    {
        return nodes;
    }

    _asc.insert_edge(u, v, std::set<node*, by_departure> {});
    _desc.insert_edge(v, u, std::set<node*, by_arrival_rev> {});
    return _asc.label(u, v);
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::next(uint32_t u, uint32_t v, uint32_t t) const ->
    typename std::set<node*, by_departure>::const_iterator
{
    auto nodes = asc(u, v);
    if(nodes == nullptr)
    {
        return nodes->end();
    }

    node q = {.i = {.left = t}};
    return nodes->lower_bound(&q);
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::nend(uint32_t u, uint32_t v) const ->
    typename std::set<node*, by_departure>::const_iterator
{
    return asc(u, v)->end();
}

template<template<typename> class Graph>
auto ttc5<Graph>::pred_succ::empty(uint32_t u, uint32_t v) const -> bool
{
    return desc(u, v)->empty();
}

} // namespace tgraph_reachability

#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>

template class tgraph_reachability::ttc5<graph_structures::adj_list>;
template class tgraph_reachability::ttc5<graph_structures::adj_matrix>;
template class tgraph_reachability::ttc5<graph_structures::edge_grid>;
