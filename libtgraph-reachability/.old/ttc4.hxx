#pragma once

#include <libtgraph-reachability/detail/tmatrix.hxx>
#include <libtgraph-reachability/journey.hxx>

#include <vector>
#include <iostream>
#include <optional>

namespace tgraph_reachability
{
class ttc4
{
public:
    ttc4(uint32_t n, uint32_t tau, uint32_t delta);

    template<typename It>
    ttc4(uint32_t n, uint32_t tau, uint32_t delta, It begin_contacts,
         It end_contacts)
        : ttc4(n, tau, delta)
    {
        /*     for(auto it = begin_contacts; it != end_contacts; ++it) */
        /*     { */
        /*         const auto& [u, v, t] = *it; */

        /*         if(u == v || t >= _tau) */
        /*         { */
        /*             return; */
        /*         } */

        /*         if(rttree(u, v)->insert(t, t + _delta, v)) */
        /*         { */
        /*             for(auto& [wminus, tree]: _g.in_neighbors(u)) */
        /*             { */
        /*                 if(wminus == v) */
        /*                 { */
        /*                     continue; */
        /*                 } */

        /*                 if(auto res = tree.find_prev(t)) */
        /*                 { */
        /*                     rttree(wminus, v)->insert(res->first.first, t +
         * _delta, */
        /*                                               res->second); */
        /*                 } */
        /*             } */
        /*         } */
        /*     } */
    }

    void add_contact(contact c);

    [[nodiscard]] auto can_reach(uint32_t u, uint32_t v, interval i) const
        -> bool;

    [[nodiscard]] auto reconstruct_journey(uint32_t u, uint32_t v,
                                           interval i) const
        -> std::optional<journey>;

    [[nodiscard]] auto tau() const -> uint32_t
    {
        return _tau;
    }

    [[nodiscard]] auto delta() const -> uint32_t
    {
        return _delta;
    }

#ifdef BENCHMARK_ON
    static auto stack_usage() -> uint64_t;
    static auto heap_usage() -> uint64_t;
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tree_inserts() -> uint64_t;
    static auto tree_erases() -> uint64_t;
    static void reset_benchmark();
#endif

private:
    auto insert(uint32_t u, uint32_t v, uint32_t t1, uint32_t t2) -> bool;
    auto idx(uint32_t u, uint32_t t) const -> uint32_t;

    using descendants = std::vector<uint32_t>;
    std::vector<descendants> _in;
    std::vector<descendants> _out;
    uint32_t _n;
    uint32_t _tau;
    uint32_t _delta;
};

} // namespace tgraph_reachability
