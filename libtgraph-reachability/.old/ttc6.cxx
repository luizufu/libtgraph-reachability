#include <libtgraph-reachability/ttc6.hxx>

#include <queue>
#include <stack>
#include <unordered_set>
#include <algorithm>

namespace tgraph_reachability
{
template<template<typename> class Graph>
ttc6<Graph>::ttc6(uint32_t n, uint32_t tau, uint32_t delta)
    : _trees(n, graph_structures::adj_list<node>(n * tau))
    , _roots(n)
    , _pointers(n)
    , _n(n)
    , _tau(tau)
    , _delta(delta)
{
    for(uint32_t v = 0; v < _n; ++v)
    {
        _roots[v] = {.i = {0, 0}, .v = v, .parent = nullptr};
        _pointers.insert(v, v, &_roots[v]);
    }
}

template<template<typename> class Graph>
void ttc6<Graph>::add_contact(contact c)
{
    if(c.u == c.v || c.t >= _tau)
    {
        return;
    }

    auto uu = find_or_create(c.u, c.t);
    interval interv1 = {c.t, c.t + _delta};
    if(auto uv = insert(c.u, c.v, interv1, uu); uv != nullptr)
    {
        if(auto vv = _pointers.next(c.v, c.v, c.t + _delta);
           vv != _pointers.nend(c.v, c.v))
        {
            meld(c.u, uv, c.v, *vv);
        }

        for(auto& [wminus, _]: _pointers.in(c.u))
        {
            if(wminus == c.u || wminus == c.v)
            {
                continue;
            }

            if(auto wu = _pointers.prev(wminus, c.u, c.t);
               wu != _pointers.pend(wminus, c.u))
            {
                interval interv2 = {(*wu)->i.left, c.t + _delta};
                if(auto wv = insert(wminus, c.v, interv2, *wu); wv != *wu)
                {
                    meld(wminus, wv, c.u, uv);
                }
            }
        }
    }
}

template<template<typename> class Graph>
auto ttc6<Graph>::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    if(auto n = _pointers.prev(u, v, i.right - 1); n != _pointers.pend(u, v))
    {
        return (*n)->i.left >= i.left;
    }

    return false;
}

template<template<typename> class Graph>
auto ttc6<Graph>::reconstruct_journey(uint32_t u, uint32_t v, interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return {};
    }

    if(auto n = _pointers.prev(u, v, i.right - 1); n != _pointers.pend(u, v))
    {
        if((*n)->i.left >= i.left)
        {
            std::stack<contact> contacts;
            const node* cur = *n;

            while(cur->v != u)
            {
                contacts.push({cur->parent->v, cur->v, cur->i.right - _delta});
                cur = cur->parent;
            }

            journey j;
            while(!contacts.empty())
            {
                j.push_back(contacts.top());
                contacts.pop();
            }

            return j;
        }
    }

    return {};
}

#ifdef BENCHMARK_ON
template<template<typename> class Graph>
auto ttc6<Graph>::stack_usage() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*, by_departure>>::total_stack_usage */
    /*        + Graph<std::set<node*, by_arrival_rev>>::total_stack_usage;
     */
}

template<template<typename> class Graph>
auto ttc6<Graph>::heap_usage() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*, by_departure>>::total_heap_usage */
    /*        + Graph<std::set<node*, by_arrival_rev>>::total_heap_usage; */
}

template<template<typename> class Graph>
auto ttc6<Graph>::binary_searches() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*,
     * by_departure>>::total_binary_searches */
    /*        + Graph<std::set<node*,
     * by_arrival_rev>>::total_binary_searches;
     */
}

template<template<typename> class Graph>
auto ttc6<Graph>::sequential_searches() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*,
     * by_departure>>::total_sequential_searches */
    /*        + Graph<std::set<node*,
     * by_arrival_rev>>::total_sequential_searches; */
}

template<template<typename> class Graph>
auto ttc6<Graph>::tree_inserts() -> uint64_t
{
    return 0;
}

template<template<typename> class Graph>
auto ttc6<Graph>::tree_erases() -> uint64_t
{
    return 0;
}

template<template<typename> class Graph>
void ttc6<Graph>::reset_benchmark()
{
    /* Graph<std::set<node*, by_departure>>::total_sequential_searches = 0;
     */
    /* Graph<std::set<node*, by_departure>>::total_binary_searches = 0; */
    /* Graph<std::set<node*, by_arrival_rev>>::total_sequential_searches =
     * 0; */
    /* Graph<std::set<node*, by_arrival_rev>>::total_binary_searches = 0; */
}

#endif

template<template<typename> class Graph>
auto ttc6<Graph>::find_or_create(uint32_t v, uint32_t t) -> node*
{
    auto vv = _pointers.prev(v, v, t);

    if((*vv)->i.left == t)
    {
        return *vv;
    }

    std::optional<node> cpy;

    uint32_t pi = vidx(v, (*vv)->i.right);
    if(node* next = _trees[v].label(pi, v); next != nullptr)
    {
        cpy = *next;
        _trees[v].delete_edge(pi, v);
        _pointers.remove(v, v, next);
    }

    _trees[v].insert_edge(pi, v, {.i = {t, t}, .v = v, .parent = *vv});
    node* n = _trees[v].label(pi, v);
    _pointers.insert(v, v, n);

    if(cpy)
    {
        uint32_t ci = vidx(v, t);
        cpy->parent = n;
        _trees[v].insert_edge(ci, v, *cpy);
        _pointers.insert(v, v, _trees[v].label(ci, v));
    }

    return n;
}

template<template<typename> class Graph>
auto ttc6<Graph>::insert(uint32_t u, uint32_t v, interval interv, node* parent)
    -> node*
{
    if(auto collecteds = _pointers.collect_inside(u, v, interv))
    {
        for(node& collected: *collecteds)
        {
            _pointers.remove(u, v, &collected);
            _trees[u].delete_edge(
                vidx(collected.parent->v, collected.parent->i.right), v);
        }

        uint32_t ui1 = vidx(parent->v, parent->i.right);
        _trees[u].insert_or_update_edge(
            ui1, v, {.i = interv, .v = v, .parent = parent});
        node* n = _trees[u].label(ui1, v);
        _pointers.insert(u, v, n);

        struct parameters
        {
            uint32_t v;
            uint32_t tplus;
            node* parent;
        };

        std::stack<parameters> stack;
        for(node& collected: *collecteds)
        {
            stack.push({.v = v, .tplus = collected.i.right, .parent = n});
        }

        while(!stack.empty())
        {
            auto [source_v, source_tplus, target_parent] = stack.top();
            stack.pop();

            if(source_tplus < _tau)
            {
                uint32_t vi1 = vidx(target_parent->v, target_parent->i.right);
                uint32_t vi2 = vidx(source_v, source_tplus);

                std::vector<node> children;
                for(auto& [_, child]: _trees[u].out_neighbors(vi2))
                {
                    children.push_back(child);
                }

                for(auto& child: children)
                {
                    _pointers.remove(u, child.v, &child);
                    _trees[u].delete_edge(vi2, child.v);
                    node* next_parent = target_parent;

                    child.parent = target_parent;
                    _trees[u].insert_edge(vi1, child.v, child);
                    next_parent = _trees[u].label(vi1, child.v);
                    _pointers.insert(u, child.v, next_parent);

                    stack.push({.v = child.v,
                                .tplus = child.i.right,
                                .parent = next_parent});
                }
            }
        }

        return n;
    }

    return parent;
}

/* template<template<typename> class Graph> */
/* auto ttc6<Graph>::insert(uint32_t u, uint32_t v, interval interv,
 * node* parent) */
/*     -> node* */
/* { */
/*     struct parameters */
/*     { */
/*         uint32_t v; */
/*         uint32_t tplus; */
/*         node* parent; */
/*     }; */
/*  */
/*     if(auto collecteds = _pointers.collect_inside(u, v, interv)) */
/*     { */
/*         for(node& collected: *collecteds) */
/*         { */
/*             _pointers.remove(u, v, &collected); */
/*             _trees[u].delete_edge( */
/*                 vidx(collected.parent->v, collected.parent->i.right),
 * v);
 */
/*         } */
/*  */
/*         uint32_t ui1 = vidx(parent->v, parent->i.right); */
/*         _trees[u].insert_edge( */
/*             ui1, v, {.i = interv, .u = parent->v, .v = v, .parent =
 * parent});
 */
/*         node* n = _trees[u].label(ui1, v); */
/*         _pointers.insert(u, v, n); */
/*  */
/*         std::stack<parameters> stack; */
/*         std::unordered_set<uint32_t> visited; */
/*  */
/*         for(uint32_t i = collecteds->size(); i > 0; --i) */
/*         { */
/*             auto& source = (*collecteds)[i - 1]; */
/*             stack.push({.v = v, .tplus = source.i.right, .parent =
 * n});
 */
/*         } */
/*  */
/*         std::vector<std::pair<uint32_t, node*>> deferred_pointers; */
/*         while(!stack.empty()) */
/*         { */
/*             auto [source_v, source_tplus, target_parent] =
 * stack.top();
 */
/*             stack.pop(); */
/*  */
/*             if(source_tplus < _tau) */
/*             { */
/*                 uint32_t vi1 = vidx(target_parent->v,
 * target_parent->i.right); */
/*                 uint32_t vi2 = vidx(source_v, source_tplus); */
/*                 std::vector<node> children; */
/*                 for(auto& [_, child]: _trees[u].out_neighbors(vi2))
 */
/*                 { */
/*                     children.push_back(child); */
/*                 } */
/*  */
/*                 for(uint32_t i = children.size(); i > 0; --i) */
/*                 { */
/*                     auto& child = children[i - 1]; */
/*  */
/*                     _pointers.remove(u, child.v, &child); */
/*                     _trees[u].delete_edge(vi2, child.v); */
/*                     node* next_parent = target_parent; */
/*  */
/*                     if(!visited.contains(child.v)) */
/*                     { */
/*                         if(auto collecteds =
 * _pointers.collect_inside( */
/*                                u, child.v, {interv.left,
 * child.i.right}))
 */
/*                         { */
/*                             for(node& collected: *collecteds) */
/*                             { */
/*                                 _pointers.remove(u, collected.v,
 * &collected);
 */
/*                                 _trees[u].delete_edge( */
/*                                     vidx(collected.parent->v, */
/*                                          collected.parent->i.right),
 */
/*                                     child.v); */
/*                             } */
/*  */
/*                             child.parent = target_parent; */
/*                             child.i.left = interv.left; */
/*                             _trees[u].insert_edge(vi1, child.v,
 * child);
 */
/*                             next_parent = _trees[u].label(vi1,
 * child.v);
 */
/*                             deferred_pointers.emplace_back(child.v,
 */
/*                                                            next_parent);
 */
/*                         } */
/*  */
/*                         visited.insert(child.v); */
/*                     } */
/*  */
/*                     stack.push({.v = child.v, */
/*                                 .tplus = child.i.right, */
/*                                 .parent = next_parent}); */
/*                 } */
/*             } */
/*         } */
/*  */
/*         for(auto& [p_v, p_parent]: deferred_pointers) */
/*         { */
/*             _pointers.insert(u, p_v, p_parent); */
/*         } */
/*  */
/*         return n; */
/*     } */
/*  */
/*     return parent; */
/* } */

template<template<typename> class Graph>
void ttc6<Graph>::meld(uint32_t wminus, node* wu, uint32_t wplus, node* vw)
{
    struct parameters
    {
        auto operator<(const parameters& rhs) const -> bool
        {
            return v->i.left > rhs.v->i.left;
        }

        node* u = nullptr;
        const node* v = nullptr;
    };

    std::priority_queue<parameters> min_heap;
    min_heap.push({.u = wu, .v = vw});

    while(!min_heap.empty())
    {
        auto p = min_heap.top();
        min_heap.pop();

        auto next_u =
            p.v->v == wplus
                ? p.u
                : insert(wminus, p.v->v, {p.u->i.left, p.v->i.right}, p.u);

        if(p.v->i.right < _tau)
        {
            uint32_t vi = vidx(p.v->v, p.v->i.right);
            for(auto& [_, child]: _trees[wplus].out_neighbors(vi))
            {
                min_heap.push({
                    .u = next_u,
                    .v = &child,
                });
            }
        }
    }
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::collect_inside(uint32_t u, uint32_t v,
                                            interval interv)
    -> std::optional<std::vector<node>>
{
    std::vector<node> inside;

    if(empty(u, v))
    {
        return inside;
    }

    // get boundary elements
    auto it1 = prev(u, v, interv.right);
    if(it1 != pend(u, v) && interv.left <= (*it1)->i.left)
    {
        return std::nullopt;
    }

    node q2 = {.i = {.left = interv.left}};
    auto it2 = next(u, v, interv.left);
    if(it2 != nend(u, v) && interv.right >= (*it2)->i.right)
    {
        return std::nullopt;
    }

    auto it = it1 == pend(u, v) ? prbegin(u, v)
              : (*it1)->i.right < interv.right
                  ? std::reverse_iterator(it1)
                  : std::reverse_iterator(std::next(it1));

    auto end = it2 == nend(u, v)              ? it2
               : (*it2)->i.left > interv.left ? it2
                                              : std::next(it2);

    while(!(end == nend(u, v) && it == prend(u, v))
          && !((end != nend(u, v) && it != prend(u, v) && *it == *end)))
    {
        inside.push_back(*(*it));
        ++it;
    }

    return inside;
}

template<template<typename> class Graph>
ttc6<Graph>::pred_succ::pred_succ(uint32_t n)
    : _desc(n)
    , _asc(n)
{
}

template<template<typename> class Graph>
void ttc6<Graph>::pred_succ::insert(uint32_t u, uint32_t v, node* n)
{
    if(_desc.label(v, u) == nullptr)
    {
        _desc.insert_edge(v, u, std::set<node*, by_arrival_rev> {});
        _asc.insert_edge(u, v, std::set<node*, by_departure> {});
    }

    _desc.label(v, u)->insert(n);
    _asc.label(u, v)->insert(n);
}

template<template<typename> class Graph>
void ttc6<Graph>::pred_succ::remove(uint32_t u, uint32_t v, node* n)
{
    if(_desc.label(v, u) != nullptr)
    {
        _desc.label(v, u)->erase(n);
        _asc.label(u, v)->erase(n);
    }
}

template<template<typename> class Graph>
auto ttc6<Graph>::vidx(uint32_t v, uint32_t t) -> uint32_t
{
    return v * _tau + t;
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::in(uint32_t u) ->
    typename Graph<std::set<node*, by_arrival_rev>>::vertex_stream
{
    return _desc.out_neighbors(u);
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::desc(uint32_t u, uint32_t v)
    -> std::set<node*, by_arrival_rev>*
{
    if(auto* nodes = _desc.label(v, u))
    {
        return nodes;
    }

    _desc.insert_edge(v, u, std::set<node*, by_arrival_rev> {});
    _asc.insert_edge(u, v, std::set<node*, by_departure> {});
    return _desc.label(v, u);
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::desc(uint32_t u, uint32_t v) const
    -> const std::set<node*, by_arrival_rev>*
{
    if(auto* nodes = _desc.label(v, u))
    {
        return nodes;
    }

    _desc.insert_edge(v, u, std::set<node*, by_arrival_rev> {});
    _asc.insert_edge(u, v, std::set<node*, by_departure> {});
    return _desc.label(v, u);
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::prev(uint32_t u, uint32_t v, uint32_t t) const ->
    typename std::set<node*, by_arrival_rev>::const_iterator
{
    auto nodes = desc(u, v);
    if(nodes == nullptr)
    {
        return nodes->end();
    }

    node q = {.i = {.right = t}};
    return nodes->lower_bound(&q);
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::pend(uint32_t u, uint32_t v) const ->
    typename std::set<node*, by_arrival_rev>::const_iterator
{
    return desc(u, v)->end();
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::prbegin(uint32_t u, uint32_t v) const ->
    typename std::set<node*, by_arrival_rev>::const_reverse_iterator
{
    return desc(u, v)->rbegin();
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::prend(uint32_t u, uint32_t v) const ->
    typename std::set<node*, by_arrival_rev>::const_reverse_iterator
{
    return desc(u, v)->rend();
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::asc(uint32_t u, uint32_t v) const
    -> const std::set<node*, by_departure>*
{
    if(auto* nodes = _asc.label(u, v))
    {
        return nodes;
    }

    _asc.insert_edge(u, v, std::set<node*, by_departure> {});
    _desc.insert_edge(v, u, std::set<node*, by_arrival_rev> {});
    return _asc.label(u, v);
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::next(uint32_t u, uint32_t v, uint32_t t) const ->
    typename std::set<node*, by_departure>::const_iterator
{
    auto nodes = asc(u, v);
    if(nodes == nullptr)
    {
        return nodes->end();
    }

    node q = {.i = {.left = t}};
    return nodes->lower_bound(&q);
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::nend(uint32_t u, uint32_t v) const ->
    typename std::set<node*, by_departure>::const_iterator
{
    return asc(u, v)->end();
}

template<template<typename> class Graph>
auto ttc6<Graph>::pred_succ::empty(uint32_t u, uint32_t v) const -> bool
{
    return desc(u, v)->empty();
}

} // namespace tgraph_reachability

#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>

template class tgraph_reachability::ttc6<graph_structures::adj_list>;
template class tgraph_reachability::ttc6<graph_structures::adj_matrix>;
template class tgraph_reachability::ttc6<graph_structures::edge_grid>;
