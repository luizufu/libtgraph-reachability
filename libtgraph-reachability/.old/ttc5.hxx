#pragma once

#include <libtgraph-reachability/journey.hxx>

#include <set>
#include <iostream>
#include <memory>
#include <optional>

namespace tgraph_reachability
{
template<template<typename> class Graph>
class ttc5
{
public:
    ttc5(uint32_t n, uint32_t tau, uint32_t delta);

    template<typename It>
    ttc5(uint32_t n, uint32_t tau, uint32_t delta, It begin_contacts,
         It end_contacts)
        : ttc5(n, tau, delta)
    {
        for(auto it = begin_contacts; it != end_contacts; ++it)
        {
            add_contact(*it);
        }
    }

    void add_contact(contact c);

    [[nodiscard]] auto can_reach(uint32_t u, uint32_t v, interval i) const
        -> bool;

    [[nodiscard]] auto reconstruct_journey(uint32_t u, uint32_t v,
                                           interval i) const
        -> std::optional<journey>;

    [[nodiscard]] auto tau() const -> uint32_t
    {
        return _tau;
    }

    [[nodiscard]] auto delta() const -> uint32_t
    {
        return _delta;
    }

#ifdef BENCHMARK_ON
    static auto stack_usage() -> uint64_t;
    static auto heap_usage() -> uint64_t;
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tree_inserts() -> uint64_t;
    static auto tree_erases() -> uint64_t;
    static void reset_benchmark();
#endif

private:
    struct node
    {
        interval i = {};
        uint32_t v = 0;
        node* parent = nullptr;
        std::vector<std::unique_ptr<node>> children;
    };

    class pred_succ
    {
        struct by_departure
        {
            auto operator()(const node* lhs, const node* rhs) const -> bool
            {
                return lhs->i.left < rhs->i.left;
            }
        };

        struct by_arrival_rev
        {
            auto operator()(const node* lhs, const node* rhs) const -> bool
            {
                return lhs->i.right > rhs->i.right;
            }
        };

    public:
        explicit pred_succ(uint32_t n);

        void insert(uint32_t u, uint32_t v, node* n);
        void remove(uint32_t u, uint32_t v, node* n);
        auto collect_inside(uint32_t u, uint32_t v, interval interv)
            -> std::optional<std::vector<node*>>;

        auto in(uint32_t u) ->
            typename Graph<std::set<node*, by_arrival_rev>>::vertex_stream;

        auto prev(uint32_t u, uint32_t v, uint32_t t) const ->
            typename std::set<node*, by_arrival_rev>::const_iterator;
        auto pend(uint32_t u, uint32_t v) const ->
            typename std::set<node*, by_arrival_rev>::const_iterator;
        auto prbegin(uint32_t u, uint32_t v) const ->
            typename std::set<node*, by_arrival_rev>::const_reverse_iterator;
        auto prend(uint32_t u, uint32_t v) const ->
            typename std::set<node*, by_arrival_rev>::const_reverse_iterator;

        auto next(uint32_t u, uint32_t v, uint32_t t) const ->
            typename std::set<node*, by_departure>::const_iterator;
        auto nend(uint32_t u, uint32_t v) const ->
            typename std::set<node*, by_departure>::const_iterator;

        auto empty(uint32_t u, uint32_t v) const -> bool;

    private:
        auto asc(uint32_t u, uint32_t v) const
            -> const std::set<node*, by_departure>*;
        auto desc(uint32_t u, uint32_t v) -> std::set<node*, by_arrival_rev>*;
        auto desc(uint32_t u, uint32_t v) const
            -> const std::set<node*, by_arrival_rev>*;

        mutable Graph<std::set<node*, by_arrival_rev>> _desc;
        mutable Graph<std::set<node*, by_departure>> _asc;
    };

    struct forest
    {
        explicit forest(uint32_t n);

        auto create_node(uint32_t u, uint32_t v, interval interv, node* parent)
            -> node*;
        auto remove_node(node* n) -> std::vector<std::unique_ptr<node>>;
        void add_children(node* n,
                          std::vector<std::unique_ptr<node>>&& children);
        auto root(uint32_t i) -> node*;

    private:
        std::vector<std::unique_ptr<node>> _roots;
    };

    void meld(uint32_t wminus, node* wu, uint32_t wplus, node* vw);
    auto insert(uint32_t u, uint32_t v, interval interv, node* parent) -> node*;
    auto find_or_create(uint32_t v, uint32_t t) -> node*;

    forest _forest;
    pred_succ _pointers;
    uint32_t _n;
    uint32_t _tau;
    uint32_t _delta;
};

} // namespace tgraph_reachability
