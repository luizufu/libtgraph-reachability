#include <libtgraph-reachability/ttc4.hxx>

#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_reachability
{
const static uint32_t null = std::numeric_limits<uint32_t>::max();

ttc4::ttc4(uint32_t n, uint32_t tau, uint32_t delta)
    : _in(n * tau, descendants(n, null))
    , _out(n * tau, descendants(n, null))
    , _n(n)
    , _tau(tau)
    , _delta(delta)
{
}
void ttc4::add_contact(contact c)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= _tau)
    {
        return;
    }

    if(insert(u, v, t, t + _delta))
    {
        std::vector<std::pair<uint32_t, uint32_t>> d;

        if(t + _delta < _tau)
        {
            for(uint32_t wplus = 0; wplus < _n; ++wplus)
            {
                if(wplus == u)
                {
                    continue;
                }

                if(uint32_t tplus = _out[idx(v, t + _delta)][wplus];
                   tplus != null && insert(u, wplus, t, tplus))
                {
                    d.emplace_back(wplus, tplus);
                }
            }
        }

        if(t >= _delta)
        {
            for(uint32_t wminus = 0; wminus < _n; ++wminus)
            {
                if(wminus == v)
                {
                    continue;
                }

                if(uint32_t tminus = _in[idx(u, t - _delta)][wminus];
                   tminus != null && insert(wminus, v, tminus, t + _delta))
                {
                    for(const auto& [wplus, tplus]: d)
                    {
                        if(wplus == wminus)
                        {
                            continue;
                        }

                        insert(wminus, wplus, tminus, tplus);
                    }
                }
            }
        }
    }
}

auto ttc4::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    return std::min(_out[idx(u, i.left)][v], _tau + _delta - 1) < i.right;
}

auto ttc4::reconstruct_journey(uint32_t u, uint32_t v, interval i) const
    -> std::optional<journey>
{
    /* if(u == v) */
    /* { */
    /*     return {}; */
    /* } */

    /* i.right = std::min(i.right, _tau + _delta); */

    /* if(i.left >= _tau || i.right - i.left < _delta) */
    /* { */
    /*     return {}; */
    /* } */

    /* auto n = _data.find_next(u, v, i.left); */
    /* if(!n || std::min(n->tplus, _tau + _delta - 1) >= i.right) */
    /* { */
    /*     return {}; */
    /* } */

    /* journey j({u, n->succ, n->tminus}); */

    /* while(n->succ != v) */
    /* { */
    /*     auto n_next = _data.find_next(n->succ, v, n->tplus); */
    /*     j.push_back({n->succ, n_next->succ, n_next->tminus}); */

    /*     n = n_next; */
    /* } */

    /* return j; */

    return {};
}

#ifdef BENCHMARK_ON
auto ttc4::stack_usage() -> uint64_t
{
    return 0;
}

auto ttc4::heap_usage() -> uint64_t
{
    return 0;
}

auto ttc4::binary_searches() -> uint64_t
{
    return 0;
}

auto ttc4::sequential_searches() -> uint64_t
{
    return 0;
}

auto ttc4::tree_inserts() -> uint64_t
{
    return 0;
}

auto ttc4::tree_erases() -> uint64_t
{
    return 0;
}

void ttc4::reset_benchmark()
{
}

#endif

auto ttc4::insert(uint32_t u, uint32_t v, uint32_t t1, uint32_t t2) -> bool
{
    if(_out[idx(u, t1)][v] < t2)
    {
        return false;
    }

    uint32_t tt = t1 + 1;
    while(tt > 0)
    {
        uint32_t tplus = _out[idx(u, tt - 1)][v];
        if(tplus != null && tplus < t2)
        {
            break;
        }

        _out[idx(u, tt - 1)][v] = t2;
        --tt;
    }

    tt = t2;
    while(tt < _tau)
    {
        uint32_t tminus = _in[idx(v, tt - _delta)][u];
        if(tminus != null && tminus > t1)
        {
            break;
        }
        _in[idx(v, tt - _delta)][u] = t1;
        ++tt;
    }

    return true;
}

auto ttc4::idx(uint32_t u, uint32_t t) const -> uint32_t
{
    return u * _tau + t;
}

} // namespace tgraph_reachability
