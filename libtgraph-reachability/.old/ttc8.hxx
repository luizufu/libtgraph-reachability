#pragma once

#include <libgraph-structures/adj-list.hxx>
#include <libtgraph-reachability/journey.hxx>

#include <set>
#include <unordered_set>
#include <iostream>
#include <memory>
#include <optional>

namespace tgraph_reachability
{
template<template<typename> class Graph>
class ttc8
{
public:
    ttc8(uint32_t n, uint32_t tau, uint32_t delta);

    template<typename It>
    ttc8(uint32_t n, uint32_t tau, uint32_t delta, It begin_contacts,
         It end_contacts)
        : ttc8(n, tau, delta)
    {
        for(auto it = begin_contacts; it != end_contacts; ++it)
        {
            add_contact(*it);
        }
    }

    void add_contact(contact c);

    [[nodiscard]] auto can_reach(uint32_t u, uint32_t v, interval i) const
        -> bool;

    [[nodiscard]] auto reconstruct_journey(uint32_t u, uint32_t v,
                                           interval i) const
        -> std::optional<journey>;

    [[nodiscard]] auto tau() const -> uint32_t
    {
        return _tau;
    }

    [[nodiscard]] auto delta() const -> uint32_t
    {
        return _delta;
    }

#ifdef BENCHMARK_ON
    static auto stack_usage() -> uint64_t;
    static auto heap_usage() -> uint64_t;
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tree_inserts() -> uint64_t;
    static auto tree_erases() -> uint64_t;
    static void reset_benchmark();
#endif

private:
    struct ctx
    {
        uint32_t parent = std::numeric_limits<uint32_t>::max();
        uint32_t v = 0;
        uint32_t tplus = 0;
    };

    void meld(uint32_t wminus, uint32_t tminus, uint32_t wplus, uint32_t v,
              uint32_t tplus, uint32_t parent);
    /* void meld(uint32_t wminus, uint32_t tminus, uint32_t wplus, uint32_t v,
     */
    /*           uint32_t tplus, uint32_t parent); */
    auto insert(uint32_t u, uint32_t v, interval interv, uint32_t parent)
        -> uint32_t;
    auto find(uint32_t v, uint32_t t) -> uint32_t;

    [[nodiscard]] auto idx(uint32_t u, uint32_t v, uint32_t t) const
        -> uint32_t;
    [[nodiscard]] auto vertex(uint32_t index) const -> uint32_t;
    [[nodiscard]] auto timestamp(uint32_t index) const -> uint32_t;

    graph_structures::adj_list<uint32_t> _adj_in;
    graph_structures::adj_list<uint32_t> _adj_out;
    std::vector<uint32_t> _parents;
    uint32_t _n;
    uint32_t _tau;
    uint32_t _delta;
};

} // namespace tgraph_reachability
