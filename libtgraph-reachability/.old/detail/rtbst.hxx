#pragma once

#include <libtgraph-reachability/journey.hxx>

#include <vector>
#include <optional>
#include <variant>

namespace tgraph_reachability::detail
{
template<typename T>
class rtbst
{
    using complete_bst = std::vector<
        std::optional<std::pair<interval, std::variant<interval, T>>>>;

public:
    explicit rtbst(uint32_t tau);
    rtbst(const rtbst& other);
    rtbst(rtbst&& other) noexcept;
    auto operator=(const rtbst& other) -> rtbst&;
    auto operator=(rtbst&& other) noexcept -> rtbst&;
    ~rtbst();

    auto insert(interval interv, T elem) -> bool;

    [[nodiscard]] auto find_next_departure(uint32_t t) const
        -> std::optional<std::pair<interval, T>>;

    [[nodiscard]] auto find_prev_arrival(uint32_t t) const
        -> std::optional<std::pair<interval, T>>;

#ifdef BENCHMARK_ON
    inline static uint64_t total_stack_usage = 0;
    inline static uint64_t total_heap_usage = 0;
    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_erases = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
#endif

private:
    auto insert_(interval interv, T elem) -> bool;
    auto any_contained_by(interval interv) -> bool;
    auto get_who_contains(interval interv)
        -> std::optional<std::pair<uint32_t, uint32_t>>;

    template<typename Func>
    [[nodiscard]] auto find_next_index(uint32_t t, Func access) const
        -> uint32_t;

    template<typename Func>
    [[nodiscard]] auto find_prev_index(uint32_t t, Func access) const
        -> uint32_t;

    void remove_range(std::pair<uint32_t, uint32_t> range);

    complete_bst _tree;
    uint32_t _tau;
};

} // namespace tgraph_reachability::detail

#include <libtgraph-reachability/detail/rtbst.ixx>
