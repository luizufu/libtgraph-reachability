#include <cmath>

namespace tgraph_reachability::detail
{
constexpr interval nullinterv = {0, std::numeric_limits<uint32_t>::max()};

template<typename T>
rtbst<T>::rtbst(uint32_t tau)
    : _tree(2 * (std::pow(2, std::ceil(std::log2(tau)))) - 1, std::nullopt)
    , _tau(tau)
{
#ifdef BENCHMARK_ON
    total_stack_usage += _tree.size() * sizeof(_tree[0]) + sizeof(_tau);
#endif
}

template<typename T>
rtbst<T>::rtbst(const rtbst& other)
    : _tree(other._tree)
    , _tau(other._tau)
{
#ifdef BENCHMARK_ON
    total_stack_usage += _tree.size() * sizeof(_tree[0]) + sizeof(_tau);
#endif
}

template<typename T>
rtbst<T>::rtbst(rtbst&& other) noexcept
    : _tree(std::move(other._tree))
    , _tau(std::move(other._tau))
{
#ifdef BENCHMARK_ON
    total_stack_usage += _tree.size() * sizeof(_tree[0]) + sizeof(_tau);
#endif
}

template<typename T>
auto rtbst<T>::operator=(const rtbst& other) -> rtbst&
{
    if(this != &other)
    {
        _tree = other._tree;
        _tau = other._tau;
#ifdef BENCHMARK_ON
        total_stack_usage += _tree.size() * sizeof(_tree[0]) + sizeof(_tau);
#endif
    }

    return *this;
}

template<typename T>
auto rtbst<T>::operator=(rtbst&& other) noexcept -> rtbst&
{
    if(this != &other)
    {
        _tree = std::move(other._tree);
        _tau = std::move(other._tau);
#ifdef BENCHMARK_ON
        total_stack_usage += _tree.size() * sizeof(_tree[0]) + sizeof(_tau);
#endif
    }

    return *this;
}

template<typename T>
rtbst<T>::~rtbst()
{
#ifdef BENCHMARK_ON
    total_heap_usage -= _tree.size() * sizeof(_tree[0]) + sizeof(_tau);
#endif
}

template<typename T>
auto rtbst<T>::insert(interval interv, T elem) -> bool
{
    if(!any_contained_by(interv))
    {
        if(auto range = get_who_contains(interv))
        {
            remove_range(*range);
        }

        return insert_(interv, elem);
    }

    return false;
}

template<typename T>
auto rtbst<T>::find_prev_arrival(uint32_t t) const
    -> std::optional<std::pair<interval, T>>
{
    uint32_t i = find_prev_index(t,
                                 [](const auto& interv)
                                 {
                                     return interv.right;
                                 });
    return i == 0 ? std::nullopt
                  : std::make_optional<std::pair<interval, T>>(
                      _tree[i - 1]->first, std::get<1>(_tree[i - 1]->second));
}

template<typename T>
auto rtbst<T>::find_next_departure(uint32_t t) const
    -> std::optional<std::pair<interval, T>>
{
    uint32_t i = find_next_index(t,
                                 [](const auto& interv)
                                 {
                                     return interv.left;
                                 });
    return i == 0 ? std::nullopt
                  : std::make_optional<std::pair<interval, T>>(
                      _tree[i - 1]->first, std::get<1>(_tree[i - 1]->second));
}

template<typename T>
auto rtbst<T>::insert_(interval interv, T elem) -> bool
{
    uint32_t i = 1;
    uint32_t t = std::pow(2, std::ceil(std::log2(_tau)) - 1);
    uint32_t len = t;
    bool cleaning = false;

    while(len > 0)
    {
        if(!_tree[i - 1] || cleaning)
        {
            _tree[i - 1] = {nullinterv, nullinterv};
            _tree[i * 2 - 1] = std::nullopt;
            _tree[i * 2] = std::nullopt;
            cleaning = true;
        }

        len /= 2;
        if(interv.left < t)
        {
            if(interv.left >= _tree[i - 1]->first.left)
            {
                _tree[i - 1]->first = interv;
            }

            i = 2 * i;
            t -= len;
        }
        else
        {
            if(interv.right <= std::get<0>(_tree[i - 1]->second).right)
            {
                _tree[i - 1]->second = interv;
            }

            i = 2 * i + 1;
            t += len;
        }
    }

    _tree[i - 1] = {interv, elem};
#ifdef BENCHMARK_ON
    ++total_binary_searches;
    ++total_inserts;
#endif
    return true;
}

template<typename T>
auto rtbst<T>::any_contained_by(interval interv) -> bool
{
    if(_tree[0])
    {
        auto i1 = find_prev_index(interv.right,
                                  [](const auto& interv)
                                  {
                                      return interv.right;
                                  });
        if(i1 > 0 && interv.left <= _tree[i1 - 1]->first.left)
        {
            return true;
        }

        auto i2 = find_next_index(interv.left,
                                  [](const auto& interv)
                                  {
                                      return interv.left;
                                  });
        if(i2 > 0 && interv.right >= _tree[i2 - 1]->first.right)
        {
            return true;
        }
    }

    return false;
}

template<typename T>
auto rtbst<T>::get_who_contains(interval interv)
    -> std::optional<std::pair<uint32_t, uint32_t>>
{
    if(_tree[0])
    {
        uint32_t i1 = find_next_index(interv.right,
                                      [](const auto& interv)
                                      {
                                          return interv.right;
                                      });
        i1 = i1 > 0 && _tree[i1 - 1]->first.left <= interv.left ? i1 : 0;
        uint32_t i2 = find_prev_index(interv.left,
                                      [](const auto& interv)
                                      {
                                          return interv.left;
                                      });
        i2 = i2 > 0 && _tree[i2 - 1]->first.right >= interv.right ? i2 : 0;

        if(i1 > 0 || i2 > 0)
        {
            return std::make_pair(i1 > 0 ? i1 : i2, i2 > 0 ? i2 : i1);
        }
    }

    return std::nullopt;
}

template<typename T>
template<typename Func>
auto rtbst<T>::find_prev_index(uint32_t t, Func access) const -> uint32_t
{
    if(!_tree[0])
    {
        return 0;
    }

#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    uint32_t i = 1;
    uint32_t len = std::pow(2, std::ceil(std::log2(_tau)) - 1);

    while(len > 0)
    {
        len /= 2;
        if(_tree[i * 2] && t >= access(std::get<0>(_tree[i - 1]->second)))
        {
            i = i * 2 + 1;
        }
        else if(_tree[i * 2 - 1])
        {
            i = i * 2;
        }
        else
        {
            return 0;
        }
    }

    return access(_tree[i - 1]->first) <= t ? i : 0;
}

template<typename T>
template<typename Func>
auto rtbst<T>::find_next_index(uint32_t t, Func access) const -> uint32_t
{
    if(!_tree[0])
    {
        return 0;
    }

#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    uint32_t i = 1;
    uint32_t len = std::pow(2, std::ceil(std::log2(_tau)) - 1);

    while(len > 0)
    {
        len /= 2;
        if(_tree[i * 2 - 1] && t <= access(_tree[i - 1]->first))
        {
            i = i * 2;
        }
        else if(_tree[i * 2])
        {
            i = i * 2 + 1;
        }
        else
        {
            return 0;
        }
    }

    return access(_tree[i - 1]->first) >= t ? i : 0;
}

template<typename T>
void rtbst<T>::remove_range(std::pair<uint32_t, uint32_t> range)
{
    auto [i1, i2] = range;

    _tree[i1 - 1] = std::nullopt;
    if(i1 % 2 == 1)
    {
        _tree[i1 / 2 - 1]->second = nullinterv;
    }

    _tree[i2 - 1] = std::nullopt;
    if(i2 % 2 == 0)
    {
        _tree[i2 / 2 - 1]->first = nullinterv;
    }

    uint32_t p = i1;
    interval pmax = nullinterv;
    interval pmin = nullinterv;

    if(i1 != i2)
    {
        while(i1 != i2)
        {
            if(i1 / 2 != i2 / 2)
            {
                if(i1 % 2 == 0)
                {
                    _tree[i1] = std::nullopt;
                    _tree[i1 / 2 - 1]->first = pmax;
                    _tree[i1 / 2 - 1]->second = nullinterv;
                }

                if(i2 % 2 == 1)
                {
                    _tree[i2 - 2] = std::nullopt;
                    _tree[i2 / 2 - 1]->first = nullinterv;
                    _tree[i2 / 2 - 1]->second = pmin;
                }

                if(_tree[i1 / 2 - 1]->first.left >= pmax.left)
                {
                    pmax = _tree[i1 / 2 - 1]->first;
                }
                if(std::get<0>(_tree[i2 / 2 - 1]->second).right <= pmin.right)
                {
                    pmin = std::get<0>(_tree[i2 / 2 - 1]->second);
                }
            }

            if(i1 - 1 < _tree.size() / 2 && !_tree[i1 * 2 - 1]
               && !_tree[i1 * 2])
            {
                _tree[i1 - 1] = std::nullopt;
                _tree[i1 / 2 - 1]->second = nullinterv;
            }

            if(i2 - 1 < _tree.size() / 2 && !_tree[i2 * 2 - 1]
               && !_tree[i2 * 2])
            {
                _tree[i2 - 1] = std::nullopt;
                _tree[i2 / 2 - 1]->first = nullinterv;
            }

            i1 /= 2;
            i2 /= 2;
        }

        p = i1;
        _tree[p - 1]->first = pmax;
        _tree[p - 1]->second = pmin;
    }

    bool updating_max = pmax == nullinterv;
    bool updating_min = pmin == nullinterv;

    while(p / 2 != 0 && (updating_max || updating_min))
    {
        if(p - 1 < _tree.size() / 2 && !_tree[p * 2 - 1] && !_tree[p * 2])
        {
            _tree[p - 1] = std::nullopt;
        }

        if(updating_max && p % 2 == 1)
        {
            std::get<0>(_tree[p / 2 - 1]->second) = pmin;
            if(_tree[p / 2 - 1]->first.right < pmin.right)
            {
                updating_max = false;
            }
        }

        if(updating_min && p % 2 == 0)
        {
            _tree[p / 2 - 1]->first = pmax;
            if(std::get<0>(_tree[p / 2 - 1]->second).left > pmax.left)
            {
                updating_min = false;
            }
        }

        if(_tree[p / 2 - 1]->first.left >= pmax.left)
        {
            pmax = _tree[p / 2 - 1]->first;
        }
        if(std::get<0>(_tree[p / 2 - 1]->second).left <= pmin.left)
        {
            pmin = std::get<0>(_tree[p / 2 - 1]->second);
        }

        p /= 2;
    }

#ifdef BENCHMARK_ON
    total_binary_searches += 2;
    ++total_erases;
#endif
}

} // namespace tgraph_reachability::detail
