#pragma once

#include <set>
#include <vector>

namespace tgraph_reachability::detail
{
template<typename Ord>
class tmatrix
{
public:
    tmatrix(uint32_t tau, uint32_t n);

    [[nodiscard]] auto read(uint32_t t, uint32_t v) const
        -> std::pair<uint32_t, uint32_t>;
    void write(uint32_t t, uint32_t v, std::pair<uint32_t, uint32_t> value);
    void remove(uint32_t t, uint32_t v);
    void remove_col_range(uint32_t t1, uint32_t t2, uint32_t v);

    [[nodiscard]] auto next_row(uint32_t t, uint32_t v) const
        -> std::optional<uint32_t>;

    [[nodiscard]] auto col_empty(uint32_t v) const -> bool;

private:
    std::vector<std::pair<uint32_t, uint32_t>> _matrix;
    std::vector<std::set<uint32_t, Ord>> _adj_list;
    uint32_t _n;
    uint32_t _tau;
};

} // namespace tgraph_reachability::detail
