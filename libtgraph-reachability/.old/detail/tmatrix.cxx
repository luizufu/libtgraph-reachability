#include <libtgraph-reachability/detail/tmatrix.hxx>

#include <functional>
#include <limits>
#include <type_traits>

namespace tgraph_reachability::detail
{
const static uint32_t null = std::numeric_limits<uint32_t>::max();

static auto idx(uint32_t i, uint32_t j, uint32_t n)
{
    return i * n + j;
}

template<typename Ord>
tmatrix<Ord>::tmatrix(uint32_t tau, uint32_t n)
    : _matrix(tau * n, {null, null})
    , _adj_list(n)
    , _n(n)
    , _tau(tau)
{
}

template<typename Ord>
auto tmatrix<Ord>::read(uint32_t t, uint32_t v) const
    -> std::pair<uint32_t, uint32_t>
{
    return _matrix[idx(t, v, _n)];
}

template<typename Ord>
void tmatrix<Ord>::write(uint32_t t, uint32_t v,
                         std::pair<uint32_t, uint32_t> value)
{
    _matrix[idx(t, v, _n)] = value;
    _adj_list[v].insert(t);
}

template<typename Ord>
void tmatrix<Ord>::remove(uint32_t t, uint32_t v)
{
    _matrix[idx(t, v, _n)] = {null, null};
    _adj_list[v].erase(t);
}

template<typename Ord>
void tmatrix<Ord>::remove_col_range(uint32_t t1, uint32_t t2, uint32_t v)
{
    if constexpr(std::is_same_v<Ord, std::less<>>)
    {
        for(auto it = _adj_list[v].lower_bound(t1);
            it != _adj_list[v].end() && *it < t2;)
        {
            _matrix[idx(*it, v, _n)] = {null, null};
            _adj_list[v].erase(it);
        }
    }
    else
    {
        for(auto it = _adj_list[v].lower_bound(t2);
            it != _adj_list[v].end() && t1 < *it;)
        {
            _matrix[idx(*it, v, _n)] = {null, null};
            _adj_list[v].erase(it);
        }
    }
}

template<typename Ord>
auto tmatrix<Ord>::next_row(uint32_t t, uint32_t v) const
    -> std::optional<uint32_t>
{
    auto it = _adj_list[v].lower_bound(t);
    return it != _adj_list[v].end() ? std::make_optional(*it) : std::nullopt;
}

template<typename Ord>
[[nodiscard]] auto tmatrix<Ord>::col_empty(uint32_t v) const -> bool
{
    return _adj_list[v].empty();
}

} // namespace tgraph_reachability::detail

template class tgraph_reachability::detail::tmatrix<std::less<>>;
template class tgraph_reachability::detail::tmatrix<std::greater<>>;
