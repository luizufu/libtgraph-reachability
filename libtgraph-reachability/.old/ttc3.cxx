#include <libtgraph-reachability/ttc3.hxx>

#include <queue>
#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_reachability
{
ttc3::ttc3(uint32_t n, uint32_t tau, uint32_t delta)
    : _data(n, tau, delta)
    , _n(n)
    , _tau(tau)
    , _delta(delta)
{
}

void ttc3::add_contact(contact c)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= _tau)
    {
        return;
    }

    if(_data.insert(u, v, t, t + _delta, v))
    {
        std::vector<std::pair<uint32_t, uint32_t>> d;

        for(uint32_t wplus = 0; wplus < _n; ++wplus)
        {
            if(wplus == u)
            {
                continue;
            }

            if(auto n = _data.find_next(v, wplus, t + _delta);
               n && _data.insert(u, wplus, t, n->tplus, v))
            {
                d.emplace_back(wplus, n->tplus);
            }
        }

        for(uint32_t wminus = 0; wminus < _n; ++wminus)
        {
            if(wminus == v)
            {
                continue;
            }

            if(auto p = _data.find_prev(wminus, u, t);
               p && _data.insert(wminus, v, p->tminus, t + _delta, p->succ))
            {
                for(const auto& [wplus, tplus]: d)
                {
                    if(wplus == wminus)
                    {
                        continue;
                    }

                    _data.insert(wminus, wplus, p->tminus, tplus, p->succ);
                }
            }
        }
    }
}

auto ttc3::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    if(auto n = _data.find_next(u, v, i.left))
    {
        return std::min(n->tplus, _tau + _delta - 1) < i.right;
    }

    return false;
}

auto ttc3::reconstruct_journey(uint32_t u, uint32_t v, interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return {};
    }

    auto n = _data.find_next(u, v, i.left);
    if(!n || std::min(n->tplus, _tau + _delta - 1) >= i.right)
    {
        return {};
    }

    journey j({u, n->succ, n->tminus});

    while(n->succ != v)
    {
        auto n_next = _data.find_next(n->succ, v, n->tplus);
        j.push_back({n->succ, n_next->succ, n_next->tminus});

        n = n_next;
    }

    return j;
}

#ifdef BENCHMARK_ON
auto ttc3::stack_usage() -> uint64_t
{
    return 0;
}

auto ttc3::heap_usage() -> uint64_t
{
    return 0;
}

auto ttc3::binary_searches() -> uint64_t
{
    return 0;
}

auto ttc3::sequential_searches() -> uint64_t
{
    return 0;
}

auto ttc3::tree_inserts() -> uint64_t
{
    return 0;
}

auto ttc3::tree_erases() -> uint64_t
{
    return 0;
}

void ttc3::reset_benchmark()
{
}

#endif

ttc3::data::data(uint32_t n, uint32_t tau, uint32_t delta)
    : _in(n, detail::tmatrix<std::greater<>>(tau, n))
    , _out(n, detail::tmatrix<std::less<>>(tau, n))
    , _n(n)
    , _tau(tau)
    , _delta(delta)
{
}

auto ttc3::data::insert(uint32_t u, uint32_t v, uint32_t t1, uint32_t t2,
                        uint32_t succ) -> bool
{
    if(!_out[u].col_empty(v))
    {
        auto p = find_prev(u, v, t2);
        if(p && t1 <= p->tminus)
        {
            return false;
        }

        auto n = find_next(u, v, t1);
        if(n && t2 >= n->tplus)
        {
            return false;
        }

        /* _in[v].remove_col_range( */
        /*     !p ? 0 : p->tplus - _delta + ((p->tplus < t2) ? 1 : 0), */
        /*     !n ? _tau : n->tplus - _delta + ((n->tminus > t1) ? 0 : 1), u);
         */

        /* _out[u].remove_col_range( */
        /*     !p ? 0 : p->tminus + ((p->tplus < t2) ? 1 : 0), */
        /*     !n ? _tau : n->tminus + ((n->tminus > t1) ? 0 : 1), v); */

        uint32_t start = !p ? 0 : p->tminus + ((p->tplus < t2) ? 1 : 0);
        uint32_t end = !n ? _tau : n->tminus + ((n->tminus > t1) ? 0 : 1);

        while(auto r = find_next(u, v, start))
        {
            if(r->tminus >= end)
            {
                break;
            }

            _in[v].remove(r->tplus - _delta, u);
            _out[u].remove(r->tminus, v);
        }
    }

    _in[v].write(t2 - _delta, u, {t1, succ});
    _out[u].write(t1, v, {t2, succ});
    return true;
}

auto ttc3::data::find_prev(uint32_t u, uint32_t v, uint32_t t) const
    -> std::optional<entry>
{
    if(t < _delta)
    {
        return std::nullopt;
    }

    if(auto tt = _in[v].next_row(t - _delta, u))
    {
        auto res = _in[v].read(*tt, u);
        return entry {
            .tminus = res.first, .tplus = *tt + _delta, .succ = res.second};
    }

    return std::nullopt;
}

auto ttc3::data::find_next(uint32_t u, uint32_t v, uint32_t t) const
    -> std::optional<entry>
{
    if(auto tt = _out[u].next_row(t, v))
    {
        auto res = _out[u].read(*tt, v);
        return entry {.tminus = *tt, .tplus = res.first, .succ = res.second};
    }

    return std::nullopt;
}

} // namespace tgraph_reachability
