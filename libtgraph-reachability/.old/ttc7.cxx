#include <libtgraph-reachability/ttc7.hxx>

#include <queue>
#include <stack>
#include <unordered_set>
#include <algorithm>
#include <optional>
#include <cassert>

namespace tgraph_reachability
{
const uint32_t null_index = std::numeric_limits<uint32_t>::max();

template<template<typename> class Graph>
ttc7<Graph>::ttc7(uint32_t n, uint32_t tau, uint32_t delta)
    : _adj(n * n * tau)
    , _parents(n * n * tau, null_index)
    , _pointers(n)
    , _n(n)
    , _tau(tau)
    , _delta(delta)
{
    for(uint32_t v = 0; v < _n; ++v)
    {
        rttree(v, v)->insert_({0, 0}, idx(v, v, 0));
    }
}

template<template<typename> class Graph>
void ttc7<Graph>::add_contact(contact c)
{
    if(c.u == c.v || c.t >= _tau)
    {
        return;
    }

    if(uint32_t* t = _adj.label(idx(c.u, c.u, c.t), c.v);
       t == nullptr || *t != c.t + _delta)
    {
        auto uu = find_or_create(c.u, c.t);
        auto vv_optional = rttree(c.v, c.v)->find_next(c.t + _delta);
        auto vv = vv_optional ? vv_optional->second : idx(c.u, c.u, _tau);

        for(auto& [w, _]: _pointers.in_neighbors(c.u))
        {
            if(w != c.v)
            {
                if(auto wu = rttree(w, c.u)->find_prev(c.t))
                {
                    meld(w, c.v, {wu->first.left, c.t + _delta}, wu->second,
                         vv);
                }
            }
        }
    }
}

template<template<typename> class Graph>
auto ttc7<Graph>::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    if(auto* tree = _pointers.label(u, v); tree != nullptr)
    {
        if(auto n = tree->find_prev(i.right - 1))
        {
            return n->first.left >= i.left;
        }
    }

    return false;
}

template<template<typename> class Graph>
auto ttc7<Graph>::reconstruct_journey(uint32_t u, uint32_t v, interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return {};
    }

    if(auto* tree = _pointers.label(u, v); tree != nullptr)
    {
        if(auto n = tree->find_prev(i.right - 1))
        {
            if(n->first.left >= i.left)
            {
                std::stack<contact> contacts;
                uint32_t cur = n->second - _delta;

                while(vertex(cur) != u)
                {
                    uint32_t parent =
                        _parents[cur - (vertex(cur) == v ? 0 : _delta)];
                    contacts.push(
                        {vertex(parent), vertex(cur), timestamp(parent)});
                    cur = parent;
                }

                journey j;
                while(!contacts.empty())
                {
                    j.push_back(contacts.top());
                    contacts.pop();
                }

                return j;
            }
        }
    }

    return {};
}

#ifdef BENCHMARK_ON
template<template<typename> class Graph>
auto ttc7<Graph>::stack_usage() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*, by_departure>>::total_stack_usage */
    /*        + Graph<std::set<node*, by_arrival_rev>>::total_stack_usage;
     */
}

template<template<typename> class Graph>
auto ttc7<Graph>::heap_usage() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*, by_departure>>::total_heap_usage */
    /*        + Graph<std::set<node*, by_arrival_rev>>::total_heap_usage; */
}

template<template<typename> class Graph>
auto ttc7<Graph>::binary_searches() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*,
     * by_departure>>::total_binary_searches */
    /*        + Graph<std::set<node*,
     * by_arrival_rev>>::total_binary_searches;
     */
}

template<template<typename> class Graph>
auto ttc7<Graph>::sequential_searches() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*,
     * by_departure>>::total_sequential_searches */
    /*        + Graph<std::set<node*,
     * by_arrival_rev>>::total_sequential_searches; */
}

template<template<typename> class Graph>
auto ttc7<Graph>::tree_inserts() -> uint64_t
{
    return 0;
}

template<template<typename> class Graph>
auto ttc7<Graph>::tree_erases() -> uint64_t
{
    return 0;
}

template<template<typename> class Graph>
void ttc7<Graph>::reset_benchmark()
{
    /* Graph<std::set<node*, by_departure>>::total_sequential_searches = 0;
     */
    /* Graph<std::set<node*, by_departure>>::total_binary_searches = 0; */
    /* Graph<std::set<node*, by_arrival_rev>>::total_sequential_searches =
     * 0; */
    /* Graph<std::set<node*, by_arrival_rev>>::total_binary_searches = 0; */
}

#endif

template<template<typename> class Graph>
auto ttc7<Graph>::find_or_create(uint32_t v, uint32_t t) -> uint32_t
{
    auto* tree = rttree(v, v);
    auto vv = tree->find_prev(t);

    if(timestamp(vv->second) == t)
    {
        return vv->second;
    }

    uint32_t ci_before = null_index;
    uint32_t ci_after = idx(v, v, t);

    if(uint32_t* tplus = _adj.label(vv->second, v); tplus != nullptr)
    {
        ci_before = idx(v, v, *tplus);
    }

    _adj.insert_or_update_edge(vv->second, v, t);
    _parents[ci_after] = vv->second;
    tree->insert_({t, t}, ci_after);

    if(ci_before != null_index)
    {
        _adj.insert_or_update_edge(ci_after, v, timestamp(ci_before));
        _parents[ci_before] = ci_after;
    }

    return ci_after;
}

template<template<typename> class Graph>
auto ttc7<Graph>::insert(uint32_t u, uint32_t v, interval interv,
                         uint32_t parent) -> uint32_t
{
    auto* tree = rttree(u, v);
    if(auto range = tree->none_contained_by_(interv))
    {
        std::vector<std::pair<interval, uint32_t>> collecteds;
        for(auto it = range->first; it != range->second; ++it)
        {
            collecteds.push_back(*it);
        }

        uint32_t ci = idx(u, v, interv.right);
        for(const auto& [key, collected]: collecteds)
        {
            tree->remove_(key);
            _adj.delete_edge(_parents[collected - _delta], v);
            _parents[collected - _delta] = null_index;
            if(key.right < _tau)
            {
                std::vector<std::pair<uint32_t, uint32_t>> children;
                for(auto& child: _adj.out_neighbors(collected))
                {
                    children.emplace_back(child);
                }

                for(auto& [w, t]: children)
                {
                    _adj.delete_edge(collected, w);
                    _adj.insert_or_update_edge(ci, w, t);
                    _parents[idx(u, w, t) - _delta] = ci;
                }
            }
        }

        _adj.insert_or_update_edge(parent, v, interv.right);
        _parents[ci - _delta] = parent;
        tree->insert_(interv, ci);

        return ci;
    }

    return parent;
}

/* template<template<typename> class Graph> */
/* void ttc7<Graph>::meld(uint32_t wminus, uint32_t tminus, uint32_t wplus, */
/*                        uint32_t v, uint32_t tplus, uint32_t parent) */
/* { */
/*     std::stack<ctx> q; */
/*     std::vector<uint8_t> visited(_n, 0U); */
/*     q.push({.parent = parent, .v = v, .tplus = tplus}); */
/*  */
/*     while(!q.empty()) */
/*     { */
/*         auto p = q.top(); */
/*         q.pop(); */
/*  */
/*         if(p.v != wplus && !visited[p.v]) */
/*         { */
/*             p.parent = insert(wminus, p.v, {tminus, p.tplus}, p.parent); */
/*             visited[p.v] = true; */
/*         } */
/*  */
/*         if(p.tplus < _tau) */
/*         { */
/*             for(auto& [w, t]: _adj.out_neighbors(idx(wplus, p.v, p.tplus)))
 */
/*             { */
/*                 q.push({.parent = p.parent, .v = w, .tplus = t}); */
/*             } */
/*         } */
/*     } */
/* } */

template<template<typename> class Graph>
void ttc7<Graph>::meld(uint32_t u, uint32_t v, interval interv, uint32_t up,
                       uint32_t vp)
{
    uint32_t vroot = root(vp);

    if(auto next_up = insert(u, v, interv, up);
       (next_up != up || v == vroot) && interv.right < _tau)
    {
        for(auto& [w, t]: _adj.out_neighbors(vp))
        {
            meld(u, w, {interv.left, t}, next_up, idx(vroot, w, t));
        }
    }
}

template<template<typename> class Graph>
auto ttc7<Graph>::ttc7::rttree(uint32_t u, uint32_t v)
    -> detail::rttree<uint32_t>*
{
    if(detail::rttree<uint32_t>* tree = _pointers.label(u, v))
    {
        return tree;
    }

    _pointers.insert_edge(u, v, detail::rttree<uint32_t>());
    return _pointers.label(u, v);
}

template<template<typename> class Graph>
auto ttc7<Graph>::idx(uint32_t u, uint32_t v, uint32_t t) const -> uint32_t
{
    return (u * _n * _tau) + (v * _tau + t);
}

template<template<typename> class Graph>
auto ttc7<Graph>::vertex(uint32_t index) const -> uint32_t
{
    return (index / _tau) % _n;
}

template<template<typename> class Graph>
auto ttc7<Graph>::timestamp(uint32_t index) const -> uint32_t
{
    return index % _tau;
}

template<template<typename> class Graph>
auto ttc7<Graph>::root(uint32_t index) const -> uint32_t
{
    return (index - _delta) / (_tau * _n);
}

} // namespace tgraph_reachability

#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>

template class tgraph_reachability::ttc7<graph_structures::adj_list>;
template class tgraph_reachability::ttc7<graph_structures::adj_matrix>;
template class tgraph_reachability::ttc7<graph_structures::edge_grid>;
