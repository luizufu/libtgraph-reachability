#include <libtgraph-reachability/ttc2.hxx>

#include <queue>
#include <algorithm>
#include <bits/stdint-uintn.h>
#include <limits>

namespace tgraph_reachability
{
ttc2::ttc2(uint32_t n, uint32_t tau, uint32_t delta)
    : _data(n, tau, delta)
    , _n(n)
    , _tau(tau)
    , _delta(delta)
{
}

void ttc2::add_contact(contact c)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= _tau)
    {
        return;
    }

    if(_data.insert(u, v, t, t + _delta, v))
    {
        std::vector<std::pair<uint32_t, uint32_t>> d;

        for(uint32_t wplus = 0; wplus < _n; ++wplus)
        {
            if(wplus == u)
            {
                continue;
            }

            if(auto n = _data.find_next(v, wplus, t + _delta);
               n && _data.insert(u, wplus, t, n->tplus, v))
            {
                d.emplace_back(wplus, n->tplus);
            }
        }

        for(uint32_t wminus = 0; wminus < _n; ++wminus)
        {
            if(wminus == v)
            {
                continue;
            }

            if(auto p = _data.find_prev(wminus, u, t);
               p && _data.insert(wminus, v, p->tminus, t + _delta, p->succ))
            {
                for(const auto& [wplus, tplus]: d)
                {
                    if(wplus == wminus)
                    {
                        continue;
                    }

                    _data.insert(wminus, wplus, p->tminus, tplus, p->succ);
                }
            }
        }
    }
}

auto ttc2::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    if(auto n = _data.find_next(u, v, i.left))
    {
        return std::min(n->tplus, _tau + _delta - 1) < i.right;
    }

    return false;
}

auto ttc2::reconstruct_journey(uint32_t u, uint32_t v, interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return {};
    }

    auto n = _data.find_next(u, v, i.left);
    if(!n || std::min(n->tplus, _tau + _delta - 1) >= i.right)
    {
        return {};
    }

    journey j({u, n->succ, n->tminus});

    while(n->succ != v)
    {
        auto n_next = _data.find_next(n->succ, v, n->tplus);
        j.push_back({n->succ, n_next->succ, n_next->tminus});

        n = n_next;
    }

    return j;
}

#ifdef BENCHMARK_ON
auto ttc2::stack_usage() -> uint64_t
{
    return 0;
}

auto ttc2::heap_usage() -> uint64_t
{
    return 0;
}

auto ttc2::binary_searches() -> uint64_t
{
    return 0;
}

auto ttc2::sequential_searches() -> uint64_t
{
    return 0;
}

auto ttc2::tree_inserts() -> uint64_t
{
    return 0;
}

auto ttc2::tree_erases() -> uint64_t
{
    return 0;
}

void ttc2::reset_benchmark()
{
}

#endif

constexpr uint32_t null = std::numeric_limits<uint32_t>::max();

static auto idx(uint32_t i, uint32_t j, uint32_t n) -> uint32_t
{
    return i * n + j;
}

ttc2::data::data(uint32_t n, uint32_t tau, uint32_t delta)
    : _in_v(n * n * tau, {null, null})
    , _out_v(n * n * tau, {null, null})
    , _in_t(n * n * tau, null)
    , _out_t(n * n * tau, null)
    , _n(n)
    , _tau(tau)
    , _delta(delta)
{
}

auto ttc2::data::insert(uint32_t u, uint32_t v, uint32_t t1, uint32_t t2,
                        uint32_t succ) -> bool
{
    if(t2 < out_v(u, v, t1).first)
    {
        out_v(u, v, t1) = {t2, succ};
        in_v(v, u, t2 - _delta) = {t1, succ};
        update_pointers(u, v, t1, t2);
        return true;
    }

    return false;
}

auto ttc2::data::find_prev(uint32_t u, uint32_t v, uint32_t t) const
    -> std::optional<entry>
{
    if(t < _delta)
    {
        return std::nullopt;
    }

    if(uint32_t tt = in_t(v, u, t - _delta); tt != null)
    {
        auto res = in_v(v, u, tt);
        entry e = {
            .tminus = res.first, .tplus = tt + _delta, .succ = res.second};
        return e;
    }

    return std::nullopt;
}

auto ttc2::data::find_next(uint32_t u, uint32_t v, uint32_t t) const
    -> std::optional<entry>
{
    if(uint32_t tt = out_t(u, v, t); tt != null)
    {
        auto res = out_v(u, v, tt);
        entry e = {.tminus = tt, .tplus = res.first, .succ = res.second};
        return e;
    }

    return std::nullopt;
}

void ttc2::data::update_pointers(uint32_t u, uint32_t v, uint32_t t1,
                                 uint32_t t2)
{
    out_t(u, v, t1) = t1;
    uint32_t tt = t1;
    while(tt > 0 && (out_t(u, v, tt - 1) == null || t1 < out_t(u, v, tt - 1)))
    {
        out_t(u, v, tt - 1) = t1;
        --tt;
    }

    tt = t2 - _delta;
    while(tt < _tau && (in_t(v, u, tt) == null || in_t(v, u, tt) < t2 - _delta))
    {
        in_t(v, u, tt) = t2 - _delta;
        ++tt;
    }
}

auto ttc2::data::in_v(uint32_t u, uint32_t v, uint32_t t)
    -> std::pair<uint32_t, uint32_t>&
{
    return _in_v[idx(u * _tau + t, v, _n)];
}

auto ttc2::data::in_v(uint32_t u, uint32_t v, uint32_t t) const
    -> const std::pair<uint32_t, uint32_t>&
{
    return _in_v[idx(u * _tau + t, v, _n)];
}

auto ttc2::data::out_v(uint32_t u, uint32_t v, uint32_t t)
    -> std::pair<uint32_t, uint32_t>&
{
    return _out_v[idx(u * _tau + t, v, _n)];
}

auto ttc2::data::out_v(uint32_t u, uint32_t v, uint32_t t) const
    -> const std::pair<uint32_t, uint32_t>&
{
    return _out_v[idx(u * _tau + t, v, _n)];
}

auto ttc2::data::in_t(uint32_t u, uint32_t v, uint32_t t) -> uint32_t&
{
    return _in_t[idx(u * _tau + t, v, _n)];
}

auto ttc2::data::in_t(uint32_t u, uint32_t v, uint32_t t) const
    -> const uint32_t&
{
    return _in_t[idx(u * _tau + t, v, _n)];
}

auto ttc2::data::out_t(uint32_t u, uint32_t v, uint32_t t) -> uint32_t&
{
    return _out_t[idx(u * _tau + t, v, _n)];
}

auto ttc2::data::out_t(uint32_t u, uint32_t v, uint32_t t) const
    -> const uint32_t&
{
    return _out_t[idx(u * _tau + t, v, _n)];
}

} // namespace tgraph_reachability
