#include <libtgraph-reachability/ttc8.hxx>

#include <queue>
#include <stack>
#include <unordered_set>
#include <algorithm>
#include <optional>

namespace tgraph_reachability
{
const uint32_t null_index = std::numeric_limits<uint32_t>::max();

template<template<typename> class Graph>
ttc8<Graph>::ttc8(uint32_t n, uint32_t tau, uint32_t delta)
    : _adj_in(n * n * tau)
    , _adj_out(n * n * tau)
    , _parents(n * n * tau, null_index)
    , _n(n)
    , _tau(tau)
    , _delta(delta)
{
}

template<template<typename> class Graph>
void ttc8<Graph>::add_contact(contact c)
{
    if(c.u == c.v || c.t >= _tau)
    {
        return;
    }

    uint32_t uu = idx(c.u, c.u, c.t);
    if(auto uv = insert(c.u, c.v, {c.t, c.t + _delta}, uu); uv != uu)
    {
        meld(c.u, c.t, c.v, c.v, c.t + _delta, uv);

        for(auto& [wminus, _]: _adj_in.out_neighbors(c.u))
        {
            if(wminus == c.u || wminus == c.v)
            {
                continue;
            }

            if(uint32_t* tminus = _adj_in.label(idx(wminus, wminus, c.t), c.u);
               tminus != nullptr)
            {
                uint32_t wu = idx(wminus, c.u, c.t);
                if(auto wv = insert(wminus, c.v, {*tminus, c.t + _delta}, wu);
                   wv != wu)
                {
                    meld(wminus, *tminus, c.u, c.v, c.t + _delta, wv);
                }
            }
        }
    }
}

template<template<typename> class Graph>
auto ttc8<Graph>::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    if(const uint32_t* tminus = _adj_in.label(idx(u, u, i.right - 1), v);
       tminus != nullptr)
    {
        return *tminus >= i.left;
    }

    return false;
}

template<template<typename> class Graph>
auto ttc8<Graph>::reconstruct_journey(uint32_t u, uint32_t v, interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return {};
    }

    uint32_t n = idx(u, u, i.right - 1);
    if(const uint32_t* tminus = _adj_in.label(n, v); tminus != nullptr)
    {
        if(*tminus >= i.left)
        {
            std::stack<contact> contacts;
            uint32_t cur = n - _delta;

            while(vertex(cur) != u)
            {
                uint32_t parent =
                    _parents[cur - (vertex(cur) == v ? 0 : _delta)];
                contacts.push({vertex(parent), vertex(cur), timestamp(parent)});
                cur = parent;
            }

            journey j;
            while(!contacts.empty())
            {
                j.push_back(contacts.top());
                contacts.pop();
            }

            return j;
        }
    }

    return {};
}

#ifdef BENCHMARK_ON
template<template<typename> class Graph>
auto ttc8<Graph>::stack_usage() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*, by_departure>>::total_stack_usage */
    /*        + Graph<std::set<node*, by_arrival_rev>>::total_stack_usage;
     */
}

template<template<typename> class Graph>
auto ttc8<Graph>::heap_usage() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*, by_departure>>::total_heap_usage */
    /*        + Graph<std::set<node*, by_arrival_rev>>::total_heap_usage; */
}

template<template<typename> class Graph>
auto ttc8<Graph>::binary_searches() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*,
     * by_departure>>::total_binary_searches */
    /*        + Graph<std::set<node*,
     * by_arrival_rev>>::total_binary_searches;
     */
}

template<template<typename> class Graph>
auto ttc8<Graph>::sequential_searches() -> uint64_t
{
    return 0;
    /* return 0 + Graph<std::set<node*,
     * by_departure>>::total_sequential_searches */
    /*        + Graph<std::set<node*,
     * by_arrival_rev>>::total_sequential_searches; */
}

template<template<typename> class Graph>
auto ttc8<Graph>::tree_inserts() -> uint64_t
{
    return 0;
}

template<template<typename> class Graph>
auto ttc8<Graph>::tree_erases() -> uint64_t
{
    return 0;
}

template<template<typename> class Graph>
void ttc8<Graph>::reset_benchmark()
{
    /* Graph<std::set<node*, by_departure>>::total_sequential_searches = 0;
     */
    /* Graph<std::set<node*, by_departure>>::total_binary_searches = 0; */
    /* Graph<std::set<node*, by_arrival_rev>>::total_sequential_searches =
     * 0; */
    /* Graph<std::set<node*, by_arrival_rev>>::total_binary_searches = 0; */
}

#endif

template<template<typename> class Graph>
auto ttc8<Graph>::insert(uint32_t u, uint32_t v, interval interv,
                         uint32_t parent) -> uint32_t
{
    auto* tree = rttree(u, v);
    if(auto range = tree->none_contained_by_(interv))
    {
        std::vector<std::pair<interval, uint32_t>> collecteds;
        for(auto it = range->first; it != range->second; ++it)
        {
            collecteds.push_back(*it);
        }

        uint32_t ci = idx(u, v, interv.right);
        for(const auto& [key, collected]: collecteds)
        {
            tree->remove_(key);
            _adj.delete_edge(_parents[collected - _delta], v);
            _parents[collected - _delta] = null_index;
            if(key.right < _tau)
            {
                std::vector<std::pair<uint32_t, uint32_t>> children;
                for(auto& child: _adj.out_neighbors(collected))
                {
                    children.emplace_back(child);
                }

                for(auto& [w, t]: children)
                {
                    _adj.delete_edge(collected, w);
                    _adj.insert_or_update_edge(ci, w, t);
                    _parents[idx(u, w, t) - _delta] = ci;
                }
            }
        }

        _adj.insert_or_update_edge(parent, v, interv.right);
        _parents[ci - _delta] = parent;
        tree->insert_(interv, ci);

        return ci;
    }

    return parent;
}

/* template<template<typename> class Graph> */
/* void ttc8<Graph>::meld(uint32_t wminus, uint32_t tminus, uint32_t wplus, */
/*                        uint32_t v, uint32_t tplus, uint32_t parent) */
/* { */
/*     std::stack<ctx> q; */
/*     std::vector<uint8_t> visited(_n, 0U); */
/*     q.push({.parent = parent, .v = v, .tplus = tplus}); */
/*  */
/*     while(!q.empty()) */
/*     { */
/*         auto p = q.top(); */
/*         q.pop(); */
/*  */
/*         if(p.v != wplus && !visited[p.v]) */
/*         { */
/*             p.parent = insert(wminus, p.v, {tminus, p.tplus}, p.parent); */
/*             visited[p.v] = true; */
/*         } */
/*  */
/*         if(p.tplus < _tau) */
/*         { */
/*             for(auto& [w, t]: _adj.out_neighbors(idx(wplus, p.v, p.tplus)))
 */
/*             { */
/*                 q.push({.parent = p.parent, .v = w, .tplus = t}); */
/*             } */
/*         } */
/*     } */
/* } */

template<template<typename> class Graph>
void ttc8<Graph>::meld(uint32_t wminus, uint32_t tminus, uint32_t wplus,
                       uint32_t v, uint32_t tplus, uint32_t parent)
{
    auto next_parent =
        v == wplus ? parent : insert(wminus, v, {tminus, tplus}, parent);

    if(tplus < _tau)
    {
        for(auto& [w, t]: _adj_out.out_neighbors(idx(wplus, v, tplus)))
        {
            meld(wminus, tminus, wplus, w, t, parent);
        }
    }
}

template<template<typename> class Graph>
auto ttc8<Graph>::idx(uint32_t u, uint32_t v, uint32_t t) const -> uint32_t
{
    return (u * _n * _tau) + (v * _tau + t);
}

template<template<typename> class Graph>
auto ttc8<Graph>::vertex(uint32_t index) const -> uint32_t
{
    return (index / _tau) % _n;
}

template<template<typename> class Graph>
auto ttc8<Graph>::timestamp(uint32_t index) const -> uint32_t
{
    return index % _tau;
}

} // namespace tgraph_reachability

#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>

template class tgraph_reachability::ttc8<graph_structures::adj_list>;
template class tgraph_reachability::ttc8<graph_structures::adj_matrix>;
template class tgraph_reachability::ttc8<graph_structures::edge_grid>;
