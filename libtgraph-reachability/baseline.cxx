#include <libtgraph-reachability/baseline.hxx>

#include <unordered_map>
#include <algorithm>

namespace tgraph_reachability
{
baseline::baseline(uint32_t n, uint32_t tau, uint32_t delta)
    : _n(n)
    , _tau(tau)
    , _delta(delta)
{
#ifdef BENCHMARK_ON
    total_stack_usage += 3 * sizeof(uint32_t);
#endif
}

baseline::~baseline()
{
#ifdef BENCHMARK_ON
    total_stack_usage -= 3 * sizeof(uint32_t);
    total_heap_usage -= _contacts.size() * sizeof(contact);
#endif
}

void baseline::add_contact(contact c)
{
    if(c.u == c.v || c.t >= _tau)
    {
        return;
    }

    _contacts.insert(c);
#ifdef BENCHMARK_ON
    total_heap_usage += sizeof(contact);
    ++total_inserts;
    ++total_binary_searches;
#endif
}

auto baseline::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    return construct_trivial(u, v, i).count(v) > 0;
}

auto baseline::reconstruct_journey(uint32_t u, uint32_t v, interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return {};
    }

    auto trivial_journeys = construct_trivial(u, v, i);

    if(trivial_journeys.count(v) == 0)
    {
        return {};
    }

    journey j = trivial_journeys[v].back();

    while(j.from() != u)
    {
        auto& vec = trivial_journeys[j.from()];

        for(auto it = vec.rbegin(); it != vec.rend(); ++it)
        {
            if(it->arrival(_delta) <= j.departure())
            {
                j = concatenate(*it, j);
                break;
            }
        }
    }

    return j;
}

#ifdef BENCHMARK_ON
auto baseline::stack_usage() -> uint64_t
{
    return total_stack_usage;
}

auto baseline::heap_usage() -> uint64_t
{
    return total_heap_usage;
}

auto baseline::binary_searches() -> uint64_t
{
    return total_binary_searches;
}

auto baseline::sequential_searches() -> uint64_t
{
    return total_sequential_searches;
}

auto baseline::tree_inserts() -> uint64_t
{
    return total_inserts;
}

auto baseline::tree_erases() -> uint64_t
{
    return 0;
}

void baseline::reset_benchmark()
{
    total_binary_searches = 0;
    total_sequential_searches = 0;
    total_inserts = 0;
}

#endif

auto baseline::construct_trivial(uint32_t source, uint32_t target,
                                 interval i) const -> journey_map
{
    journey_map map;

    auto is_concatenable = [&map, source,
                            delta = _delta](const journey& candidate) -> bool
    {
        if(auto it = map.find(candidate.from()); it != map.end())
        {
            return std::any_of(it->second.begin(), it->second.end(),
                               [source, candidate, delta](const journey& old)
                               {
                                   return old.arrival(delta)
                                          <= candidate.departure();
                               });
        }

        return false;
    };

#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    auto it = _contacts.lower_bound({source, 0, i.left});
    auto end = _contacts.end();

    while(it != end && it->u != source)
    {
        ++it;
    }

    for(; it != end && it->t + _delta < i.right; ++it)
    {
        journey j(*it);

        if(it->u == source || is_concatenable(j))
        {
            map[it->v].push_back(j);

            if(it->v == target)
            {
                break;
            }
        }
    }
#ifdef BENCHMARK_ON
    ++total_sequential_searches;
#endif

    return map;
}

} // namespace tgraph_reachability
