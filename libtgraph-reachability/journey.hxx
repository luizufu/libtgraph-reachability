#pragma once

#include <vector>
#include <iostream>

namespace tgraph_reachability
{
struct contact
{
    uint32_t u = 0;
    uint32_t v = 0;
    uint32_t t = 0;

    auto operator==(const contact& other) const -> bool
    {
        return std::make_tuple(t, u, v)
               == std::make_tuple(other.t, other.u, other.v);
    }

    auto operator<(const contact& other) const -> bool
    {
        return std::make_tuple(t, u, v)
               < std::make_tuple(other.t, other.u, other.v);
    }
};

struct interval
{
    uint32_t left = 0;
    uint32_t right = 0;

    [[nodiscard]] auto includes(const interval& other) const -> bool
    {
        return left <= other.left && other.right <= right;
    }

    auto operator==(const interval& other) const -> bool
    {
        return std::make_pair(left, right)
               == std::make_pair(other.left, other.right);
    }

    auto operator<(const interval& other) const -> bool
    {
        return std::make_pair(left, right)
               < std::make_pair(other.left, other.right);
    }
};

class journey
{
    std::vector<contact> _contacts;

public:
    journey() = default;

    explicit journey(contact c)
        : _contacts {c}
    {
    }

    [[nodiscard]] auto from() const -> uint32_t
    {
        return _contacts.front().u;
    }

    [[nodiscard]] auto to() const -> uint32_t
    {
        return _contacts.back().v;
    }

    [[nodiscard]] auto departure() const -> uint32_t
    {
        return _contacts.front().t;
    }

    [[nodiscard]] auto arrival(uint32_t delta) const -> uint32_t
    {
        return _contacts.back().t + delta;
    }

    [[nodiscard]] auto duration(uint32_t delta) const -> uint32_t
    {
        return arrival(delta) - departure();
    }

    [[nodiscard]] auto size() const -> size_t
    {
        return _contacts.size();
    }

    [[nodiscard]] auto contacts() const -> const std::vector<contact>&
    {
        return _contacts;
    }

    void push_back(const contact& c)
    {
        _contacts.push_back(c);
    }

    friend auto concatenate(journey lhs, const journey& rhs) -> journey;
    friend auto operator==(const journey& lhs, const journey& rhs) -> bool;
    friend auto operator!=(const journey& lhs, const journey& rhs) -> bool;
    friend auto operator<<(std::ostream& out, const journey& j)
        -> std::ostream&;
};

auto operator<<(std::ostream& out, const contact& c) -> std::ostream&;
auto operator<<(std::ostream& out, const interval& i) -> std::ostream&;

} // namespace tgraph_reachability
