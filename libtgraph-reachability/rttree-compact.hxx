#pragma once

#include <libtgraph-reachability/journey.hxx>

#include <unordered_map>
#include <vector>
#include <dynamic/dynamic.hpp>
#include <optional>

namespace tgraph_reachability
{
template<typename T, typename BitVector>
class rttree_compact
{
    template<typename U>
    friend class rttree_simple;

public:
    rttree_compact();
    ~rttree_compact();

    auto insert(interval interv, T elem) -> bool;

    [[nodiscard]] auto find_next(uint32_t t) const
        -> std::optional<std::pair<interval, T>>;

    [[nodiscard]] auto find_prev(uint32_t t) const
        -> std::optional<std::pair<interval, T>>;

    [[nodiscard]] auto empty() const -> bool;

#ifdef BENCHMARK_ON
    inline static uint64_t total_stack_usage = 0;
    inline static uint64_t total_heap_usage = 0;
    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_erases = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
    inline static std::unordered_map<uint32_t, uint32_t>
        journey_duration_frequency = {};
#endif

private:

    BitVector _departures;
    BitVector _arrivals;
    std::vector<T> _succs;
    uint32_t _ones;
};

template<typename T>
using rttree_succinct = rttree_compact<T, dyn::suc_bv>;

template<typename T>
using rttree_sparse = rttree_compact<T, dyn::gap_bv>;

} // namespace tgraph_reachability

#include <libtgraph-reachability/rttree-compact.ixx>
