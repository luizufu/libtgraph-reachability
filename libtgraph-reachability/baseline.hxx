#pragma once

#include <libtgraph-reachability/journey.hxx>

#include <set>
#include <unordered_map>
#include <type_traits>

namespace tgraph_reachability
{
class baseline
{
    using journey_map = std::unordered_map<uint32_t, std::vector<journey>>;

public:
    baseline(uint32_t n, uint32_t tau, uint32_t delta);

    template<typename It>
    baseline(uint32_t n, uint32_t tau, uint32_t delta, It begin_contacts,
             It end_contacts)
        : baseline(n, tau, delta)
    {
        for(auto it = begin_contacts; it != end_contacts; ++it)
        {
            add_contact(*it);
        }
    }

    ~baseline();

    void add_contact(contact c);

    [[nodiscard]] auto can_reach(uint32_t u, uint32_t v, interval i) const
        -> bool;

    [[nodiscard]] auto reconstruct_journey(uint32_t u, uint32_t v,
                                           interval i) const
        -> std::optional<journey>;

    [[nodiscard]] auto tau() const -> uint32_t
    {
        return _tau;
    }

    [[nodiscard]] auto delta() const -> uint32_t
    {
        return _delta;
    }

#ifdef BENCHMARK_ON
    inline static uint64_t total_stack_usage = 0;
    inline static uint64_t total_heap_usage = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
    inline static uint64_t total_inserts = 0;

    static auto stack_usage() -> uint64_t;
    static auto heap_usage() -> uint64_t;
    static auto binary_searches() -> uint64_t;
    static auto sequential_searches() -> uint64_t;
    static auto tree_inserts() -> uint64_t;
    static auto tree_erases() -> uint64_t;
    static void reset_benchmark();
#endif

private:
    [[nodiscard]] auto construct_trivial(uint32_t source, uint32_t target,
                                         interval i) const -> journey_map;

    std::set<contact> _contacts;
    uint32_t _n;
    uint32_t _tau;
    uint32_t _delta;
};

} // namespace tgraph_reachability
