#include <vector>
#include <algorithm>
#include <type_traits>

namespace tgraph_reachability
{

template<typename BitVector>
auto resize(BitVector* bitvector, uint32_t n) -> void;

template<typename BitVector>
auto set(BitVector* bitvector, uint32_t i) -> void;

template<typename BitVector>
auto clear(BitVector* bitvector, uint32_t i) -> void;

template<typename T, typename BitVector>
rttree_compact<T, BitVector>::rttree_compact()
{
}

template<typename T, typename BitVector>
rttree_compact<T, BitVector>::~rttree_compact()
{
}

template<typename T, typename BitVector>
auto rttree_compact<T, BitVector>::insert(interval interv, T elem) -> bool
{
    resize(&_departures, interv.left + 1);
    resize(&_arrivals, interv.right + 1);
    _succs.resize(interv.left + 1);

    uint32_t i = _departures.rank1(interv.left + 1);
    uint32_t j = _arrivals.rank1(interv.right);

    if(uint32_t jj = j + static_cast<uint32_t>(_arrivals.at(interv.right));
       i < jj || (jj > 0 && interv.left <= _departures.select1(jj - 1)))
    {
        return false;
    }

    for(; i > j; --i)
    {
        clear(&_departures, _departures.select1(j));
        clear(&_arrivals, _arrivals.select1(j));
    }

    set(&_departures, interv.left);
    set(&_arrivals, interv.right);
    _succs[interv.left] = elem;

    return true;
}

template<typename T, typename BitVector>
auto rttree_compact<T, BitVector>::find_prev(uint32_t t) const
    -> std::optional<std::pair<interval, T>>
{
    uint32_t i = _arrivals.rank1(
        std::min(t + 1, static_cast<uint32_t>(_arrivals.size())));
    if(i == 0)
        return {};

    uint32_t departure = _departures.select1(i - 1);
    uint32_t arrival = _arrivals.select1(i - 1);
    return std::make_pair(interval {departure, arrival}, _succs[departure]);
}

template<typename T, typename BitVector>
auto rttree_compact<T, BitVector>::find_next(uint32_t t) const
    -> std::optional<std::pair<interval, T>>
{
    if(t > _departures.size())
        return {};

    uint32_t i = _departures.rank1(t);
    if(_departures.rank1(_departures.size()) == i)
        return {};

    uint32_t departure = _departures.select1(i + 1 - 1);
    uint32_t arrival = _arrivals.select1(i + 1 - 1);
    return std::make_pair(interval {departure, arrival}, _succs[departure]);
}

template<typename T, typename BitVector>
auto rttree_compact<T, BitVector>::empty() const -> bool
{
    return _departures.rank1(_departures.size()) == 0;
}

template<typename BitVector>
auto resize(BitVector* bitvector, uint32_t n) -> void
{
    if constexpr(std::is_same<BitVector, dyn::suc_bv>::value)
    {
        while(bitvector->size() <= n)
        {
            bitvector->push_word(0U, 64);
        }
    }
    else if constexpr(std::is_same<BitVector, dyn::gap_bv>::value)
    {
        if(uint32_t old_n = bitvector->size(); old_n < n)
        {
            bitvector->insert0(old_n, n - old_n);
        }
    }
    else
    {
    }
}

template<typename BitVector>
auto set(BitVector* bitvector, uint32_t i) -> void
{
    if constexpr(std::is_same<BitVector, dyn::suc_bv>::value)
    {
        bitvector->set(i, true);
    }
    else if constexpr(std::is_same<BitVector, dyn::gap_bv>::value)
    {
        bitvector->set(i);
    }
    else
    {
    }
}

template<typename BitVector>
auto clear(BitVector* bitvector, uint32_t i) -> void
{
    if constexpr(std::is_same<BitVector, dyn::suc_bv>::value)
    {
        bitvector->set(i, false);
    }
    else if constexpr(std::is_same<BitVector, dyn::gap_bv>::value)
    {
        bitvector->delete1(i);
        bitvector->insert0(i);
    }
    else
    {
    }
}

} // namespace tgraph_reachability
