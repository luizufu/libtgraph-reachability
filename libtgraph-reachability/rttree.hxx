#pragma once

#include <libtgraph-reachability/detail/btree.hxx>
#include <libtgraph-reachability/detail/interval-ordering.hxx>
#include <libtgraph-reachability/journey.hxx>

#include <unordered_map>
#include <vector>
#include <optional>

namespace tgraph_reachability
{
template<typename T>
class rttree
{
    template<typename U>
    friend class rttree_simple;

public:
    using base_tree = detail::btree<interval, T, detail::by_departure>;
    using const_iterator = typename base_tree::const_iterator;

    rttree();
    ~rttree();

    auto insert(interval interv, T elem) -> bool;

    auto insert_(interval interv, T elem) -> bool;
    auto remove_(interval interv) -> bool;
    auto none_contained_by_(interval interv)
        -> std::optional<std::pair<const_iterator, const_iterator>>;

    [[nodiscard]] auto find_next(uint32_t t) const
        -> std::optional<std::pair<interval, T>>;

    [[nodiscard]] auto find_prev(uint32_t t) const
        -> std::optional<std::pair<interval, T>>;

    [[nodiscard]] auto empty() const -> bool;

#ifdef BENCHMARK_ON
    inline static uint64_t total_stack_usage = 0;
    inline static uint64_t total_heap_usage = 0;
    inline static uint64_t total_inserts = 0;
    inline static uint64_t total_erases = 0;
    inline static uint64_t total_binary_searches = 0;
    inline static uint64_t total_sequential_searches = 0;
    inline static std::unordered_map<uint32_t, uint32_t>
        journey_duration_frequency = {};
#endif

private:
    [[nodiscard]] auto find_next_(uint32_t t) const -> const_iterator;
    [[nodiscard]] auto find_prev_(uint32_t t) const -> const_iterator;
    void remove_range(std::pair<const_iterator, const_iterator> range);

    base_tree _tree;
};

} // namespace tgraph_reachability

#include <libtgraph-reachability/rttree.ixx>
