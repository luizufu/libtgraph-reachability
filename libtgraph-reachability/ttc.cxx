#include <libtgraph-reachability/ttc.hxx>

#include <queue>
#include <algorithm>

namespace tgraph_reachability
{
template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::ttc::tree(uint32_t u, uint32_t v)
    -> Tree<uint32_t>*
{
    if(Tree<uint32_t>* tree = _g.label(u, v))
    {
        return tree;
    }

    _g.insert_edge(u, v, Tree<uint32_t>());
    return _g.label(u, v);
}

template<template<typename> class Graph, template<typename> class Tree>
void ttc<Graph, Tree>::add_contact(contact c)
{
    const auto& [u, v, t] = c;

    if(u == v || t >= _tau)
    {
        return;
    }

    if(tree(u, v)->insert({t, t + _delta}, v))
    {
        std::vector<std::pair<uint32_t, uint32_t>> d;

        for(auto& [wplus, tr]: _g.out_neighbors(v))
        {
            if(wplus == u)
            {
                continue;
            }

            if(auto res = tr.find_next(t + _delta))
            {
                uint32_t tplus = res->first.right;
                if(tree(u, wplus)->insert({t, tplus}, v))
                {
                    d.emplace_back(wplus, tplus);
                }
            }
        }

        for(auto& [wminus, tr]: _g.in_neighbors(u))
        {
            if(wminus == v)
            {
                continue;
            }

            if(auto res = tr.find_prev(t))
            {
                uint32_t tminus = res->first.left;
                uint32_t succ = res->second;
                if(tree(wminus, v)->insert({tminus, t + _delta}, succ))
                {
                    for(const auto& [wplus, tplus]: d)
                    {
                        if(wplus == wminus)
                        {
                            continue;
                        }

                        tree(wminus, wplus)->insert({tminus, tplus}, succ);
                    }
                }
            }
        }
    }
}

template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::can_reach(uint32_t u, uint32_t v, interval i) const -> bool
{
    if(u == v)
    {
        return true;
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return false;
    }

    if(const auto* tree = _g.label(u, v))
    {
        if(i.left == 0 && i.right == _tau)
        {
            return true;
        }

        if(auto res = tree->find_next(i.left))
        {
            uint32_t tplus = res->first.right;
            return std::min(tplus, _tau + _delta - 1) < i.right;
        }
    }

    return false;
}

template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::reconstruct_journey(uint32_t u, uint32_t v, interval i) const
    -> std::optional<journey>
{
    if(u == v)
    {
        return {};
    }

    i.right = std::min(i.right, _tau + _delta);

    if(i.left >= _tau || i.right - i.left < _delta)
    {
        return {};
    }

    const auto* tree = _g.label(u, v);

    if(tree == nullptr)
    {
        return {};
    }

    auto res = tree->find_next(i.left);

    if(!res.has_value() || res->first.right >= i.right)
    {
        return {};
    }

    uint32_t succ = res->second;
    uint32_t tminus = res->first.left;

    journey j({u, succ, tminus});

    while(succ != v)
    {
        auto res2 = _g.label(succ, v)->find_next(tminus + _delta);
        uint32_t next_succ = res2->second;
        uint32_t next_tminus = res2->first.left;

        j.push_back({succ, next_succ, next_tminus});

        succ = next_succ;
        tminus = next_tminus;
    }

    return j;
}

#ifdef BENCHMARK_ON
template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::stack_usage() -> uint64_t
{
    return Tree<uint32_t>::total_stack_usage
           + Graph<Tree<uint32_t>>::total_stack_usage;
}

template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::heap_usage() -> uint64_t
{
    return Tree<uint32_t>::total_heap_usage
           + Graph<Tree<uint32_t>>::total_heap_usage;
}

template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::binary_searches() -> uint64_t
{
    return Tree<uint32_t>::total_binary_searches
           + Graph<Tree<uint32_t>>::total_binary_searches;
}

template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::sequential_searches() -> uint64_t
{
    return Tree<uint32_t>::total_sequential_searches
           + Graph<Tree<uint32_t>>::total_sequential_searches;
}

template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::tree_inserts() -> uint64_t
{
    return Tree<uint32_t>::total_inserts;
}

template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::tree_erases() -> uint64_t
{
    return Tree<uint32_t>::total_erases;
}

template<template<typename> class Graph, template<typename> class Tree>
auto ttc<Graph, Tree>::journey_duration_frequency()
    -> const std::unordered_map<uint32_t, uint32_t>&
{
    return Tree<uint32_t>::journey_duration_frequency;
}

template<template<typename> class Graph, template<typename> class Tree>
void ttc<Graph, Tree>::reset_benchmark()
{
    Tree<uint32_t>::total_sequential_searches = 0;
    Tree<uint32_t>::total_binary_searches = 0;
    Tree<uint32_t>::total_inserts = 0;
    Tree<uint32_t>::total_erases = 0;
    Tree<uint32_t>::journey_duration_frequency.clear();

    Graph<Tree<uint32_t>>::total_sequential_searches = 0;
    Graph<Tree<uint32_t>>::total_binary_searches = 0;
}

#endif

} // namespace tgraph_reachability

#include <libgraph-structures/adj-list.hxx>
#include <libgraph-structures/adj-matrix.hxx>
#include <libgraph-structures/edge-grid.hxx>
#include <libtgraph-reachability/rttree.hxx>
#include <libtgraph-reachability/rttree-simple.hxx>
#include <libtgraph-reachability/rttree-compact.hxx>
#include <libtgraph-reachability/rttree-compact2.hxx>

using tgraph_reachability::rttree;
using tgraph_reachability::rttree_simple;
using tgraph_reachability::rttree_succinct;
using tgraph_reachability::rttree_sparse;
using tgraph_reachability::rttree_succinct2;
using tgraph_reachability::rttree_sparse2;

template class tgraph_reachability::ttc<graph_structures::adj_list, rttree>;
template class tgraph_reachability::ttc<graph_structures::adj_matrix, rttree>;
template class tgraph_reachability::ttc<graph_structures::edge_grid, rttree>;

template class tgraph_reachability::ttc<graph_structures::adj_list, rttree_simple>;
template class tgraph_reachability::ttc<graph_structures::adj_matrix, rttree_simple>;
template class tgraph_reachability::ttc<graph_structures::edge_grid, rttree_simple>;

template class tgraph_reachability::ttc<graph_structures::adj_list, rttree_succinct>;
template class tgraph_reachability::ttc<graph_structures::adj_matrix, rttree_succinct>;
template class tgraph_reachability::ttc<graph_structures::edge_grid, rttree_succinct>;

template class tgraph_reachability::ttc<graph_structures::adj_list, rttree_sparse>;
template class tgraph_reachability::ttc<graph_structures::adj_matrix, rttree_sparse>;
template class tgraph_reachability::ttc<graph_structures::edge_grid, rttree_sparse>;

template class tgraph_reachability::ttc<graph_structures::adj_list, rttree_succinct2>;
template class tgraph_reachability::ttc<graph_structures::adj_matrix, rttree_succinct2>;
template class tgraph_reachability::ttc<graph_structures::edge_grid, rttree_succinct2>;

template class tgraph_reachability::ttc<graph_structures::adj_list, rttree_sparse2>;
template class tgraph_reachability::ttc<graph_structures::adj_matrix, rttree_sparse2>;
template class tgraph_reachability::ttc<graph_structures::edge_grid, rttree_sparse2>;
