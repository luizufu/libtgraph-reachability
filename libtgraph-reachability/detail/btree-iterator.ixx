namespace tgraph_reachability::detail
{
template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
btree<Key, Value, Comp, PAGE_SIZE>::const_iterator::const_iterator(
    const leaf_node* leaf, uint32_t i)
    : _leaf(leaf)
    , _i(i)
{
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_iterator::operator*() const
    -> reference
{
    return {_leaf->keys[_i], _leaf->values[_i]};
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_iterator::operator->() const
    -> pointer
{
    return {{_leaf->keys[_i], _leaf->values[_i]}};
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_iterator::operator++()
    -> const_iterator&
{
    ++_i;

    if(_i < _leaf->size)
    {
        return *this;
    }

    _leaf = _leaf->next;
    _i = 0;

    return *this;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_iterator::operator++(int)
    -> const_iterator
{
    const_iterator old_value(*this);
    ++*this;
    return old_value;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_iterator::operator==(
    const const_iterator& other) const -> bool
{
    return other._leaf == _leaf && other._i == _i;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_iterator::operator!=(
    const const_iterator& other) const -> bool
{
    return other._leaf != _leaf || other._i != _i;
}

// reverse iterator

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
btree<Key, Value, Comp, PAGE_SIZE>::const_reverse_iterator::
    const_reverse_iterator(const leaf_node* leaf, uint32_t i)
    : _leaf(leaf)
    , _i(i)
{
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_reverse_iterator::operator*()
    const -> reference
{
    return {_leaf->keys[_i - 1], _leaf->values[_i - 1]};
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp,
           PAGE_SIZE>::const_reverse_iterator::operator->() const -> pointer
{
    return {{_leaf->keys[_i - 1], _leaf->values[_i - 1]}};
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_reverse_iterator::operator++()
    -> const_reverse_iterator&
{
    --_i;

    if(_i > 0)
    {
        return *this;
    }

    _leaf = _leaf->prev;
    _i = _leaf != nullptr ? _leaf->size : 0;

    return *this;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_reverse_iterator::operator++(int)
    -> const_reverse_iterator
{
    const_reverse_iterator old_value(*this);
    ++*this;
    return old_value;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_reverse_iterator::operator==(
    const const_reverse_iterator& other) const -> bool
{
    return other._leaf == _leaf && other._i == _i;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::const_reverse_iterator::operator!=(
    const const_reverse_iterator& other) const -> bool
{
    return other._leaf != _leaf || other._i != _i;
}

} // namespace tgraph_reachability::detail
