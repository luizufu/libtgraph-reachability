#pragma once

#include <array>
#include <queue>
#include <stack>
#include <algorithm>
#include <iostream>
#include <memory>
#include <optional>


namespace tgraph_reachability
{
template<typename T>
class rttree;

template<typename T>
class rttree_simple;
} // namespace tgraph_reachability

namespace tgraph_reachability::detail
{
template<typename Key, typename Value, typename Comp = std::less<Key>,
         size_t PAGE_SIZE = 256>
class btree
{
    template<typename T>
    friend class tgraph_reachability::rttree;

    template<typename T>
    friend class tgraph_reachability::rttree_simple;

    struct inner_node;
    struct leaf_node;

    enum node_type : uint32_t
    {
        inner,
        leaf,
    };

    union node
    {
        explicit node(node_type type);
        ~node();
        node(const node& other);
        node(node&& other) noexcept = default;
        auto operator=(const node& other) -> node&;
        auto operator=(node&& other) noexcept -> node& = default;

        struct
        {
            union
            {
                inner_node inner;
                leaf_node leaf;
            };
            node_type type;
        };
        std::array<std::byte, PAGE_SIZE> bytes = {};
    };

    struct inner_node
    {
        static constexpr uint32_t N_KEYS = (PAGE_SIZE - 16) / (sizeof(Key) + 8);

        inner_node() = default;
        ~inner_node() = default;
        inner_node(const inner_node& other);
        inner_node(inner_node&& other) noexcept = default;
        auto operator=(const inner_node& other) -> inner_node&;
        auto operator=(inner_node&& other) noexcept -> inner_node& = default;

        void insert(uint32_t i, const Key& key, std::unique_ptr<node> pointer);
        auto remove(uint32_t i) -> std::unique_ptr<node>;
        auto split(inner_node* s) -> Key;
        void share_to(inner_node* s, Key* p_key, uint32_t to_pass,
                      bool no_merge = false);
        void share_from(inner_node* s, Key* p_key, uint32_t to_pass,
                        bool no_merge = false);

        std::array<Key, N_KEYS> keys = {};
        std::array<std::unique_ptr<node>, N_KEYS + 1> pointers = {};
        uint32_t size = 0;
    };

    struct leaf_node
    {
        static constexpr uint32_t N_KEYS =
            (PAGE_SIZE - 24) / (sizeof(Key) + sizeof(Value));

        leaf_node() = default;
        ~leaf_node() = default;
        leaf_node(const leaf_node& other) = default;
        leaf_node(leaf_node&& other) noexcept = default;
        auto operator=(const leaf_node& other) -> leaf_node& = default;
        auto operator=(leaf_node&& other) noexcept -> leaf_node& = default;

        void insert(uint32_t i, const Key& key, const Value& value);
        auto remove(uint32_t i) -> Value;
        auto split(leaf_node* s) -> Key;
        void share_to(leaf_node* s, Key* p_key, uint32_t to_pass,
                      bool no_merge = false);
        void share_from(leaf_node* s, Key* p_key, uint32_t to_pass,
                        bool no_merge = false);

        std::array<Key, N_KEYS> keys = {};
        std::array<Value, N_KEYS> values = {};
        leaf_node* prev = nullptr;
        leaf_node* next = nullptr;
        uint32_t size = 0;
    };

    struct allocator
    {
        allocator() = default;
        ~allocator()
        {
            while(!_free_nodes.empty())
            {
                ::operator delete(_free_nodes.back());
                _free_nodes.pop();
            }
        }

        [[nodiscard]] auto create(node_type type) -> node*
        {
            node* n;
            if(!_free_nodes.empty())
            {
                n = _free_nodes.back();
                _free_nodes.pop();

                if(n->type == node_type::inner)
                {
                    for(uint32_t i = 0; i < n->inner.size; ++i)
                    {
                        _free_nodes.push(n->inner.pointers[i]);
                    }
                }
            }
            else
            {
                n = static_cast<node*>(::operator new(sizeof(node)));
            }

            *n = node(type);
            return n;
        }

        void destroy(node* p) noexcept
        {
            ::operator delete(p);
        }

        void destroy_lazy(node* p) noexcept
        {
            _free_nodes.push(p);
        }

    private:
        std::queue<node*> _free_nodes;
    };

    using destructive_context =
        std::stack<std::pair<std::unique_ptr<node>, uint32_t>>;
    using context = std::stack<std::pair<node*, uint32_t>>;
    using const_context = std::stack<std::pair<const node*, uint32_t>>;

    auto insert_inner(context* ctx, const Key& key,
                      std::unique_ptr<node> pointer)
        -> std::optional<std::unique_ptr<node>>;
    auto remove_inner(context* ctx, inner_node* n, uint32_t n_i)
        -> std::optional<std::unique_ptr<node>>;

    template<typename CompareFunc>
    auto destructive_find_leaf(std::unique_ptr<node> n, const Key& key,
                               CompareFunc comp) -> destructive_context;

    template<typename CompareFunc>
    auto find_leaf(node* n, const Key& key, CompareFunc comp) -> context;

    template<typename CompareFunc>
    auto find_leaf(const node* n, const Key& key, CompareFunc comp) const
        -> const_context;

    auto find_first(node* n,
                    uint32_t max_depth = std::numeric_limits<uint32_t>::max())
        -> context;
    auto find_last(node* n,
                   uint32_t max_depth = std::numeric_limits<uint32_t>::max())
        -> context;

    auto join_nodes(std::unique_ptr<node> n1, std::unique_ptr<node> n2,
                    uint32_t* n1_height, uint32_t n2_height, Key n2_smallest,
                    leaf_node** n1_first_leaf, leaf_node** n1_last_leaf,
                    std::pair<leaf_node**, leaf_node**>* n2_leaves)
        -> std::unique_ptr<node>;

public:
    class const_iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = std::pair<Key, Value>;
        using reference = value_type;
        struct pointer
        {
            value_type t;
            auto operator->() -> value_type*
            {
                return &t;
            }
        };
        using iterator_category = std::forward_iterator_tag;

        const_iterator(const leaf_node* leaf, uint32_t i);
        auto operator*() const -> reference;
        auto operator->() const -> pointer;
        auto operator++() -> const_iterator&;
        auto operator++(int) -> const_iterator;
        auto operator==(const const_iterator& other) const -> bool;
        auto operator!=(const const_iterator& other) const -> bool;

    private:
        const leaf_node* _leaf;
        uint32_t _i;
    };

    class const_reverse_iterator
    {
    public:
        using difference_type = std::ptrdiff_t;
        using value_type = std::pair<Key, Value>;
        using reference = value_type;
        struct pointer
        {
            value_type t;
            auto operator->() -> value_type*
            {
                return &t;
            }
        };
        using iterator_category = std::forward_iterator_tag;

        const_reverse_iterator(const leaf_node* leaf, uint32_t i);
        auto operator*() const -> reference;
        auto operator->() const -> pointer;
        auto operator++() -> const_reverse_iterator&;
        auto operator++(int) -> const_reverse_iterator;
        auto operator==(const const_reverse_iterator& other) const -> bool;
        auto operator!=(const const_reverse_iterator& other) const -> bool;

    private:
        const leaf_node* _leaf;
        uint32_t _i;
    };

    btree();
    ~btree();
    btree(const btree& other);
    btree(btree&& other) noexcept = default;
    auto operator=(const btree& other) -> btree&;
    auto operator=(btree&& other) noexcept -> btree& = default;

    auto insert(const Key& key, const Value& value) -> bool;
    auto insert_or_update(const Key& key, const Value& value)
        -> std::optional<Value>;
    auto remove(const Key& key) -> std::optional<Value>;
    void join(btree&& other);
    auto split(const Key& key) -> btree;

    auto find(const Key& key) const -> const_iterator;
    auto begin() const -> const_iterator;
    auto end() const -> const_iterator;
    auto rbegin() const -> const_reverse_iterator;
    auto rend() const -> const_reverse_iterator;

    template<typename CompareFunc = Comp>
    auto lower_bound(const Key& key, CompareFunc comp = Comp()) const
        -> const_iterator;

    template<typename CompareFunc = Comp>
    auto upper_bound(const Key& key, CompareFunc comp = Comp()) const
        -> const_iterator;

    [[nodiscard]] auto size() const -> uint32_t;
    [[nodiscard]] auto empty() const -> bool;
    [[nodiscard]] auto check_validity() const -> bool;

    void print_root()
    {
        std::cout << "root: " << _root.get() << ", " << _root->type
                  << std::endl;

        if(_root->type == node_type::inner)
        {
            for(uint32_t i = 0; i < _root->inner.pointers.size(); ++i)
            {
                if(_root->inner.pointers[i])
                {
                    std::cout << "child #" << i << ": "
                              << _root->inner.pointers[i].get() << std::endl;
                }
            }
        }
    }

private:
    allocator _alloc;
    std::unique_ptr<node> _root;
    leaf_node* _first_leaf;
    leaf_node* _last_leaf;
    uint32_t _size;
    uint32_t _height;
};

template<typename Node, typename Key, typename CompareFunc>
auto lb(const Node& n, const Key& key, CompareFunc comp) -> uint32_t
{
    return std::lower_bound(kbegin(&n), kend(&n), key, comp) - kbegin(&n);
}

template<typename Node, typename Key, typename CompareFunc>
auto ub(const Node& n, const Key& key, CompareFunc comp) -> uint32_t
{
    return std::upper_bound(kbegin(&n), kend(&n), key, comp) - kbegin(&n);
}

} // namespace tgraph_reachability::detail

#include <libtgraph-reachability/detail/btree-iterator.ixx>
#include <libtgraph-reachability/detail/btree-node.ixx>
#include <libtgraph-reachability/detail/btree.ixx>
