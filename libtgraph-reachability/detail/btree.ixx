#include <queue>
#include <algorithm>
#include <iostream>

namespace tgraph_reachability::detail
{
template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
btree<Key, Value, Comp, PAGE_SIZE>::btree()
    : _alloc()
    , _root(std::make_unique<node>(node_type::leaf))
    , _first_leaf(&_root->leaf)
    , _last_leaf(&_root->leaf)
    , _size(0)
    , _height(0)
{
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
btree<Key, Value, Comp, PAGE_SIZE>::~btree()
{
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
btree<Key, Value, Comp, PAGE_SIZE>::btree(const btree& other)
    : _root(std::make_unique<node>(*other._root))
    , _first_leaf(&find_first(_root.get()).top().first->leaf)
    , _last_leaf(&find_last(_root.get()).top().first->leaf)
    , _size(other._size)
    , _height(other._height)
{
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::operator=(const btree& other) -> btree&
{
    if(this != &other)
    {
        _root = std::make_unique<node>(*other._root);
        _first_leaf = &find_first(_root.get()).top().first->leaf;
        _last_leaf = &find_last(_root.get()).top().first->leaf;
        _size = other._size;
        _height = other._height;
    }

    return *this;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::insert(const Key& key,
                                                const Value& value) -> bool
{
    Comp comp;
    auto ctx = find_leaf(_root.get(), key, comp);
    leaf_node* n = &ctx.top().first->leaf;
    ctx.pop();
    uint32_t i = lb(*n, key, comp);

    if(i < n->size && n->keys[i] == key)
    {
        return false;
    }

    n->insert(i, key, value);
    ++_size;

    if(n->size >= leaf_node::N_KEYS)
    {
        auto sibling = std::make_unique<node>(node_type::leaf);
        Key pkey = n->split(&sibling->leaf);
        if(_last_leaf->keys[0] < pkey)
        {
            _last_leaf = &sibling->leaf;
        }

        if(auto new_root = insert_inner(&ctx, pkey, std::move(sibling)))
        {
            (*new_root)->inner.pointers[0] = std::move(_root);
            _root = std::move(*new_root);
            ++_height;
        }
    }

    return true;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::insert_or_update(const Key& key,
                                                          const Value& value)
    -> std::optional<Value>
{
    Comp comp;
    auto ctx = find_leaf(_root.get(), key, comp);
    leaf_node* n = &ctx.top().first->leaf;
    ctx.pop();
    uint32_t i = lb(*n, key, comp);

    if(i < n->size && n->keys[i] == key)
    {
        Value old = n->values[i];
        n->values[i] = value;
        return old;
    }

    n->insert(i, key, value);
    ++_size;

    if(n->size >= leaf_node::N_KEYS)
    {
        auto sibling = std::make_unique<node>(node_type::leaf);
        Key pkey = n->split(&sibling->leaf);
        if(_last_leaf->keys[0] < pkey)
        {
            _last_leaf = &sibling->leaf;
        }

        if(auto new_root = insert_inner(&ctx, pkey, std::move(sibling)))
        {
            (*new_root)->inner.pointers[0] = std::move(_root);
            _root = std::move(*new_root);
            ++_height;
        }
    }

    return {};
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::remove(const Key& key)
    -> std::optional<Value>
{
    Comp comp;
    auto ctx = find_leaf(_root.get(), key, comp);
    leaf_node* n = &ctx.top().first->leaf;
    ctx.pop();
    uint32_t i = lb(*n, key, comp);

    if(i >= n->size || n->keys[i] != key)
    {
        return {};
    }

    Value old = n->remove(i);
    --_size;

    if(ctx.empty())
    {
        return old;
    }

    inner_node* p = &ctx.top().first->inner;
    uint32_t p_i = ctx.top().second;
    ctx.pop();
    if(i == 0 && p_i > 0)
    {
        p->keys[p_i - 1] = n->keys[0];
    }

    if(n->size < leaf_node::N_KEYS / 2)
    {
        leaf_node* left = p_i > 0 ? &p->pointers[p_i - 1]->leaf : nullptr;
        leaf_node* right =
            p_i < p->size ? &p->pointers[p_i + 1]->leaf : nullptr;

        if(left != nullptr && left->size > leaf_node::N_KEYS / 2)
        {
            left->share_to(n, &p->keys[p_i - 1],
                           left->size
                               - std::ceil((left->size + n->size) / 2.F));
        }
        else if(right != nullptr && right->size > leaf_node::N_KEYS / 2)
        {
            n->share_from(right, &p->keys[p_i],
                          right->size
                              - std::ceil((n->size + right->size) / 2.F));
        }
        else if(left != nullptr)
        {
            left->share_from(n, &p->keys[p_i - 1], n->size);
            if(n == _last_leaf)
            {
                _last_leaf = left;
            }

            if(auto new_root = remove_inner(&ctx, p, p_i - 1))
            {
                _root = std::move(*new_root);
                --_height;
            }
        }
        else if(right != nullptr)
        {
            n->share_from(right, &p->keys[p_i], right->size);
            if(right == _last_leaf)
            {
                _last_leaf = n;
            }

            if(auto new_root = remove_inner(&ctx, p, p_i))
            {
                _root = std::move(*new_root);
                --_height;
            }
        }
    }

    return old;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
void btree<Key, Value, Comp, PAGE_SIZE>::join(btree&& other)
{
    auto other_leaves = std::make_pair(&other._first_leaf, &other._last_leaf);
    _root = join_nodes(std::move(_root), std::move(other._root), &_height,
                       other._height, other._first_leaf->keys[0], &_first_leaf,
                       &_last_leaf, &other_leaves);
    _size += other._size;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::split(const Key& key) -> btree
{
    Comp comp;
    auto ctx = destructive_find_leaf(std::move(_root), key, comp);
    ctx.top().second = lb(ctx.top().first->leaf, key, comp);

    auto l = std::move(ctx.top().first);
    auto r = std::make_unique<node>(node_type::leaf);
    uint32_t i = ctx.top().second;
    ctx.pop();

    uint32_t size = l->leaf.size;
    std::move(&l->leaf.keys[0] + i, &l->leaf.keys[0] + size, &r->leaf.keys[0]);
    std::move(&l->leaf.values[0] + i, &l->leaf.values[0] + size,
              &r->leaf.values[0]);
    l->leaf.size -= size - i;
    r->leaf.size += size - i;
    r->leaf.next = l->leaf.next;
    l->leaf.next = nullptr;

    Key l_small = l->leaf.keys[0];
    Key r_small = r->leaf.keys[0];
    uint32_t l_h = 0;
    uint32_t r_h = 0;

    leaf_node* l_last_leaf = l->leaf.size > 0 ? &l->leaf : l->leaf.prev;
    leaf_node* r_first_leaf = &r->leaf;
    leaf_node* _1 = nullptr;
    leaf_node* _2 = nullptr;
    uint32_t h = 1;
    while(!ctx.empty())
    {
        auto l2 = std::move(ctx.top().first);
        auto r2 = std::make_unique<node>(node_type::inner);
        uint32_t i = ctx.top().second;
        ctx.pop();

        uint32_t size = l2->inner.size;
        std::move(&l2->inner.keys[0] + i + (i < size),
                  &l2->inner.keys[0] + size, &r2->inner.keys[0]);
        std::move(&l2->inner.pointers[0] + i + 1,
                  &l2->inner.pointers[0] + size + 1, &r2->inner.pointers[0]);
        l2->inner.size -= size - i + (i > 0);
        r2->inner.size += size - i - (i < size);

        if(i > 0)
        {
            l_small = l2->inner.keys[i - 1];
        }

        if(i < size)
        {
            r_small = l2->inner.keys[i];
        }

        if(i == 1)
        {
            uint32_t l2_h = h - 1;
            l = join_nodes(std::move(l2->inner.pointers[0]), std::move(l),
                           &l2_h, l_h, l_small, &_1, &l_last_leaf, nullptr);
            l_h = l2_h;
        }
        else if(i > 1)
        {
            uint32_t l2_h = h;
            l = join_nodes(std::move(l2), std::move(l), &l2_h, l_h, l_small,
                           &_1, &l_last_leaf, nullptr);
            l_h = l2_h;
        }

        if(i == size - 1)
        {
            r = join_nodes(std::move(r),
                           std::move(r2->inner.pointers[r2->inner.size]), &r_h,
                           h - 1, r_small, &r_first_leaf, &_2, nullptr);
        }
        else if(i < size - 1)
        {
            r = join_nodes(std::move(r), std::move(r2), &r_h, h, r_small,
                           &r_first_leaf, &_2, nullptr);
        }
        h++;
    }

    leaf_node* r_last_leaf = _last_leaf;
    _root = std::move(l);
    _last_leaf = l_last_leaf;
    _last_leaf = _last_leaf != nullptr ? _last_leaf : _first_leaf;
    _last_leaf->next = nullptr;
    _height = l_h;

    btree other;
    other._root = std::move(r);
    other._first_leaf = r_first_leaf;
    other._first_leaf->prev = nullptr;
    other._last_leaf = r_last_leaf;
    other._height = r_h;
    return other;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::find(const Key& key) const
    -> const_iterator
{
    auto it = lower_bound(key, Comp());
    return (it != end() && it->first == key) ? it : end();
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::begin() const -> const_iterator
{
    return const_iterator(_first_leaf, 0);
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::end() const -> const_iterator
{
    return const_iterator(nullptr, 0);
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::rbegin() const
    -> const_reverse_iterator
{
    return const_reverse_iterator(_last_leaf, _last_leaf->size);
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::rend() const -> const_reverse_iterator
{
    return const_reverse_iterator(nullptr, 0);
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
template<typename CompareFunc>
auto btree<Key, Value, Comp, PAGE_SIZE>::lower_bound(const Key& key,
                                                     CompareFunc comp) const
    -> const_iterator
{
    auto ctx = find_leaf(_root.get(), key, comp);
    const leaf_node* n = &ctx.top().first->leaf;
    size_t i = lb(*n, key, comp);
    return i < n->size ? const_iterator(n, i) : const_iterator(n->next, 0);
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
template<typename CompareFunc>
auto btree<Key, Value, Comp, PAGE_SIZE>::upper_bound(const Key& key,
                                                     CompareFunc comp) const
    -> const_iterator
{
    auto ctx = find_leaf(_root.get(), key, comp);
    const leaf_node* n = &ctx.top().first->leaf;
    size_t i = ub(&n, key, comp);
    return i < n->size ? const_iterator(n, i) : const_iterator(n->next, 0);
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::size() const -> uint32_t
{
    return _size;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::empty() const -> bool
{
    return _size == 0;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::check_validity() const -> bool
{
    if(_root->type == node_type::inner)
    {
        std::queue<const node*> nodes;
        for(uint32_t i = 0; i < _root->inner.size + 1; ++i)
        {
            nodes.push(_root->inner.pointers[i].get());
        }

        while(!nodes.empty())
        {
            const node* n = nodes.front();
            nodes.pop();

            if(n->type == node_type::leaf)
            {
                if(n->leaf.size < leaf_node::N_KEYS / 2 - 1)
                {
                    return false;
                }
            }
            else
            {
                if(n->inner.size < inner_node::N_KEYS / 2 - 1)
                {
                    return false;
                }

                for(uint32_t i = 0; i < n->inner.size + 1; ++i)
                {
                    nodes.push(n->inner.pointers[i].get());
                }
            }
        }
    }

    return true;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
template<typename CompareFunc>
auto btree<Key, Value, Comp, PAGE_SIZE>::destructive_find_leaf(
    std::unique_ptr<node> n, const Key& key, CompareFunc comp)
    -> destructive_context
{
    destructive_context ctx;

    while(n->type == node_type::inner)
    {
        uint32_t i = ub(n->inner, key, comp);
        ctx.push({std::move(n), i});
        n = std::move(ctx.top().first->inner.pointers[i]);
    }

    ctx.push({std::move(n), 0});
    return ctx;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
template<typename CompareFunc>
auto btree<Key, Value, Comp, PAGE_SIZE>::find_leaf(node* n, const Key& key,
                                                   CompareFunc comp) -> context
{
    context ctx;

    while(n->type == node_type::inner)
    {
        uint32_t i = ub(n->inner, key, comp);
        ctx.push({n, i});
        n = n->inner.pointers[i].get();
    }

    ctx.push({n, 0});
    return ctx;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
template<typename CompareFunc>
auto btree<Key, Value, Comp, PAGE_SIZE>::find_leaf(const node* n,
                                                   const Key& key,
                                                   CompareFunc comp) const
    -> const_context
{
    const_context ctx;

    while(n->type == node_type::inner)
    {
        uint32_t i = ub(n->inner, key, comp);
        ctx.push({n, i});
        n = n->inner.pointers[i].get();
    }

    ctx.push({n, 0});
    return ctx;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::find_first(node* n, uint32_t max_depth)
    -> context
{
    context ctx;

    while(max_depth > 0 && n->type == node_type::inner)
    {
        ctx.push({n, 0});
        n = n->inner.pointers[0].get();
        --max_depth;
    }

    ctx.push({n, 0});
    return ctx;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::find_last(node* n, uint32_t max_depth)
    -> context
{
    context ctx;

    while(max_depth > 0 && n->type == node_type::inner)
    {
        ctx.push({n, n->inner.size});
        n = n->inner.pointers[n->inner.size].get();
        --max_depth;
    }

    ctx.push({n, n->leaf.size});
    return ctx;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::insert_inner(
    context* ctx, const Key& key, std::unique_ptr<node> pointer)
    -> std::optional<std::unique_ptr<node>>
{
    Key pkey = key;
    while(!ctx->empty())
    {
        inner_node* n = &ctx->top().first->inner;
        uint32_t n_i = ctx->top().second;
        ctx->pop();
        n->insert(n_i, pkey, std::move(pointer));

        if(n->size < inner_node::N_KEYS)
        {
            return {};
        }

        pointer = std::make_unique<node>(node_type::inner);
        pkey = n->split(&pointer->inner);
    }

    auto new_root = std::make_unique<node>(node_type::inner);
    new_root->inner.insert(0, pkey, std::move(pointer));
    return new_root;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::remove_inner(context* ctx,
                                                      inner_node* n,
                                                      uint32_t n_i)
    -> std::optional<std::unique_ptr<node>>
{
    n->remove(n_i);

    while(!ctx->empty() && n->size < inner_node::N_KEYS / 2 - 1)
    {
        inner_node* p = &ctx->top().first->inner;
        uint32_t p_i = ctx->top().second;
        ctx->pop();
        inner_node* left = p_i > 0 ? &p->pointers[p_i - 1]->inner : nullptr;
        inner_node* right =
            p_i < p->size ? &p->pointers[p_i + 1]->inner : nullptr;

        if(left != nullptr && left->size > inner_node::N_KEYS / 2)
        {
            left->share_to(n, &p->keys[p_i - 1],
                           left->size
                               - std::ceil((left->size + n->size) / 2.F));
        }
        else if(right != nullptr && right->size > inner_node::N_KEYS / 2)
        {
            n->share_from(right, &p->keys[p_i],
                          right->size
                              - std::ceil((n->size + right->size) / 2.F));
        }
        else if(left != nullptr)
        {
            left->share_from(n, &p->keys[p_i - 1], n->size);
            p->remove(p_i - 1);
        }
        else if(right != nullptr)
        {
            n->share_from(right, &p->keys[p_i], right->size);
            p->remove(p_i);
        }

        n = p;
    }

    return ctx->empty() && n->size == 0 && n->pointers[0]
               ? std::make_optional(std::move(n->pointers[0]))
               : std::nullopt;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::join_nodes(
    std::unique_ptr<node> n1, std::unique_ptr<node> n2, uint32_t* n1_height,
    uint32_t n2_height, Key n2_smallest, leaf_node** n1_first_leaf,
    leaf_node** n1_last_leaf, std::pair<leaf_node**, leaf_node**>* n2_leaves)
    -> std::unique_ptr<node>
{
    auto is_empty = [](node* n)
    {
        return (n->type == node_type::leaf && n->leaf.size == 0)
               || (n->type == node_type::inner && n->inner.size == 0);
    };

    if(is_empty(n2.get()))
    {
        if(n1->type == node_type::leaf)
        {
            n1->leaf.next = nullptr;
            *n1_first_leaf = &n1->leaf;
            *n1_last_leaf = &n1->leaf;
        }
        return n1;
    }

    if(is_empty(n1.get()))
    {
        *n1_height = n2_height;
        if(n2->type == node_type::leaf)
        {
            n2->leaf.next = nullptr;
            *n1_first_leaf = &n2->leaf;
            *n1_last_leaf = &n2->leaf;
        }
        else if(n2_leaves != nullptr)
        {
            *n1_first_leaf = *n2_leaves->first;
            *n1_last_leaf = *n2_leaves->second;
        }
        return n2;
    }

    auto run_equal = [&, this](auto* r1, auto* r2)
    {
        std::optional<std::unique_ptr<node>> new_root;

        constexpr uint32_t max_keys =
            std::remove_pointer_t<decltype(r1)>::N_KEYS;
        constexpr bool is_leaf = std::is_same_v<decltype(r1), leaf_node*>;
        if(r1->size + r2->size + (is_leaf ? 0 : 1) < max_keys)
        {
            r1->share_from(r2, &n2_smallest, r2->size);
            if constexpr(is_leaf)
            {
                *n1_first_leaf = r1;
                *n1_last_leaf = r1;
            }
            else if(n2_leaves != nullptr)
            {
                (*n1_last_leaf)->next = *n2_leaves->first;
                (*n2_leaves->first)->prev = *n1_last_leaf;
                *n1_last_leaf = *n2_leaves->second;
            }
        }
        else
        {
            uint32_t even = std::ceil((r1->size + r2->size) / 2.F);
            if(even > r2->size)
            {
                r1->share_to(r2, &n2_smallest, even - r2->size);
            }
            else if(even < r2->size)
            {
                r1->share_from(r2, &n2_smallest, r2->size - even);
            }

            context empty_ctx;
            if((new_root =
                    insert_inner(&empty_ctx, n2_smallest, std::move(n2))))
            {
                (*new_root)->inner.pointers[0] = std::move(n1);
            }

            if constexpr(is_leaf)
            {
                r1->next = r2;
                r2->prev = r1;
                *n1_first_leaf = r1;
                *n1_last_leaf = r2;
            }
            else if(n2_leaves != nullptr)
            {
                (*n1_last_leaf)->next = *n2_leaves->first;
                (*n2_leaves->first)->prev = *n1_last_leaf;
                *n1_last_leaf = *n2_leaves->second;
            }
        }

        return new_root;
    };

    auto run_greater =
        [&, this](auto* r1, auto* r2, auto* c1, auto* c2, context* ctx)
    {
        std::optional<std::unique_ptr<node>> new_root;

        constexpr uint32_t max_keys =
            std::remove_pointer_t<decltype(r2)>::N_KEYS;
        constexpr bool is_leaf = std::is_same_v<decltype(r2), leaf_node*>;
        if(c1->size + c2->size + r2->size + 1 < 2 * max_keys - 1)
        {
            uint32_t even =
                std::ceil((c1->size + c2->size + r2->size + 1) / 2.F);
            if(even > c1->size)
            {
                c1->share_from(c2, &r1->keys[r1->size - 1], even - c1->size,
                               true);
            }
            else if(even < c1->size)
            {
                c1->share_to(c2, &r1->keys[r1->size - 1], c1->size - even,
                             true);
            }
            if(c2->size == 0 && is_leaf)
            {
                r1->keys[r1->size - 1] = n2_smallest;
            }
            c2->share_from(r2, &n2_smallest, r2->size);
            if constexpr(is_leaf)
            {
                *n1_last_leaf = c2;
            }
            else if(n2_leaves != nullptr)
            {
                (*n1_last_leaf)->next = *n2_leaves->first;
                (*n2_leaves->first)->prev = *n1_last_leaf;
                *n1_last_leaf = *n2_leaves->second;
            }
        }
        else
        {
            if(r2->size < max_keys / 2 - 1)
            {
                uint32_t even =
                    std::ceil((c1->size + c2->size + r2->size) / 3.F);
                if(even > c1->size)
                {
                    c1->share_from(c2, &r1->keys[r1->size - 1], even - c1->size,
                                   true);
                }
                else if(even < c1->size)
                {
                    c1->share_to(c2, &r1->keys[r1->size - 1], c1->size - even,
                                 true);
                }
                c2->share_to(r2, &n2_smallest, even - r2->size);
            }

            if((new_root = insert_inner(ctx, n2_smallest, std::move(n2))))
            {
                (*new_root)->inner.pointers[0] = std::move(n1);
            }

            if constexpr(is_leaf)
            {
                c2->next = r2;
                r2->prev = c2;
                *n1_last_leaf = r2;
            }
            else if(n2_leaves != nullptr)
            {
                (*n1_last_leaf)->next = *n2_leaves->first;
                (*n2_leaves->first)->prev = *n1_last_leaf;
                *n1_last_leaf = *n2_leaves->second;
            }
        }

        return new_root;
    };

    auto run_lesser =
        [&, this](auto* r1, auto* r2, auto* c1, auto* c2, context* ctx)
    {
        std::optional<std::unique_ptr<node>> new_root;

        constexpr uint32_t max_keys =
            std::remove_pointer_t<decltype(r1)>::N_KEYS;
        constexpr bool is_leaf = std::is_same_v<decltype(r1), leaf_node*>;
        if(r1->size + c1->size + c2->size + 1 < 2 * max_keys - 1)
        {
            uint32_t even =
                std::ceil((r1->size + c1->size + c2->size + 1) / 2.F);
            if(even > c2->size)
            {
                c1->share_to(c2, &r2->keys[0], even - c2->size, true);
            }
            else if(even < c2->size)
            {
                c1->share_from(c2, &r2->keys[0], c2->size - even, true);
            }
            r1->share_to(c1, &n2_smallest, r1->size);
            if constexpr(is_leaf)
            {
                *n1_first_leaf = c1;
            }
            else if(n2_leaves != nullptr)
            {
                (*n1_last_leaf)->next = *n2_leaves->first;
                (*n2_leaves->first)->prev = *n1_last_leaf;
                *n1_last_leaf = *n2_leaves->second;
            }
        }
        else
        {
            if(r1->size < max_keys / 2 - 1)
            {
                uint32_t even =
                    std::ceil((r1->size + c1->size + c2->size) / 3.F);
                if(even > c2->size)
                {
                    c1->share_to(c2, &r2->keys[0], even - c2->size, true);
                }
                else if(even < c2->size)
                {
                    c1->share_from(c2, &r2->keys[0], c2->size - even, true);
                }
                r1->share_from(c1, &n2_smallest, even - r1->size);
            }

            std::swap(ctx->top().first->inner.pointers[0], n1);
            if((new_root = insert_inner(ctx, n2_smallest, std::move(n1))))
            {
                (*new_root)->inner.pointers[0] = std::move(n2);
            }

            if constexpr(is_leaf)
            {
                r1->next = c1;
                c1->prev = r1;
                *n1_first_leaf = r1;
            }
            else if(n2_leaves != nullptr)
            {
                (*n1_last_leaf)->next = *n2_leaves->first;
                (*n2_leaves->first)->prev = *n1_last_leaf;
                *n1_last_leaf = *n2_leaves->second;
            }
        }

        return new_root;
    };

    uint32_t diff = *n1_height > n2_height ? *n1_height - n2_height
                                           : n2_height - *n1_height;
    if(*n1_height == n2_height)
    {
        if(auto new_root = *n1_height == 0 ? run_equal(&n1->leaf, &n2->leaf)
                                           : run_equal(&n1->inner, &n2->inner))
        {
            n1 = std::move(*new_root);
            ++*n1_height;
        }
    }
    else if(*n1_height > n2_height)
    {
        auto ctx1 = find_last(n1.get(), diff);
        ctx1.pop();
        inner_node* r1 = &ctx1.top().first->inner;
        if(auto new_root =
               n2_height == 0
                   ? run_greater(r1, &n2->leaf,
                                 &r1->pointers[r1->size - 1]->leaf,
                                 &r1->pointers[r1->size]->leaf, &ctx1)
                   : run_greater(r1, &n2->inner,
                                 &r1->pointers[r1->size - 1]->inner,
                                 &r1->pointers[r1->size]->inner, &ctx1))
        {
            n1 = std::move(*new_root);
            ++*n1_height;
        }
    }
    else // if(*n1_height < n2_height)
    {
        auto ctx2 = find_first(n2.get(), diff);
        ctx2.pop();
        inner_node* r2 = &ctx2.top().first->inner;
        if(auto new_root =
               *n1_height == 0
                   ? run_lesser(&n1->leaf, r2, &r2->pointers[0]->leaf,
                                &r2->pointers[1]->leaf, &ctx2)
                   : run_lesser(&n1->inner, r2, &r2->pointers[0]->inner,
                                &r2->pointers[1]->inner, &ctx2))
        {
            n2 = std::move(*new_root);
            ++n2_height;
        }

        n1 = std::move(n2);
        *n1_height = n2_height;
    }

    return n1;
}

} // namespace tgraph_reachability::detail
