#pragma once

#include <libtgraph-reachability/journey.hxx>

namespace tgraph_reachability::detail
{
struct by_departure
{
    auto operator()(const interval& lhs, const interval& rhs) const -> bool
    {
        return lhs.left < rhs.left;
    }
};

struct by_arrival
{
    auto operator()(const interval& lhs, const interval& rhs) -> bool
    {
        return lhs.right < rhs.right;
    }
};

struct reverse_by_arrival
{
    auto operator()(const interval& lhs, const interval& rhs) -> bool
    {
        return lhs.right > rhs.right;
    }
};

} // namespace tgraph_reachability::detail
