#include <cmath>

namespace tgraph_reachability::detail
{
template<typename Node>
auto kbegin(Node* n)
{
    return n->keys.begin();
}

template<typename Node>
auto kend(Node* n)
{
    return n->keys.begin() + n->size;
}

template<typename Node>
auto vbegin(Node* n)
{
    return n->values.begin();
}

template<typename Node>
auto vend(Node* n)
{
    return n->values.begin() + n->size;
}

template<typename Node>
auto pbegin(Node* n)
{
    return n->pointers.begin();
}

template<typename Node>
auto pend(Node* n)
{
    return n->pointers.begin() + n->size + 1;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
btree<Key, Value, Comp, PAGE_SIZE>::node::node(node_type t)
    : bytes {}
{
    switch(t)
    {
    case node_type::leaf:
        leaf = leaf_node();
        break;
    case node_type::inner:
        inner = inner_node();
        break;
    }
    type = t;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
btree<Key, Value, Comp, PAGE_SIZE>::node::~node()
{
    switch(type)
    {
    case node_type::leaf:
        leaf.~leaf_node();
        break;
    case node_type::inner:
        inner.~inner_node();
        break;
    }
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
btree<Key, Value, Comp, PAGE_SIZE>::node::node(const node& other)
{
    switch(other.type)
    {
    case node_type::leaf:
        leaf = other.leaf;
        break;
    case node_type::inner:
        inner = other.inner;
        break;
    }
    type = other.type;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::node::operator=(const node& other)
    -> node&
{
    if(this != &other)
    {
        switch(other.type)
        {
        case node_type::leaf:
            leaf = other.leaf;
            break;
        case node_type::inner:
            inner = other.inner;
            break;
        }

        type = other.type;
    }

    return *this;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
btree<Key, Value, Comp, PAGE_SIZE>::inner_node::inner_node(
    const inner_node& other)
    : size(other.size)
{
    std::copy(kbegin(&other), kend(&other), kbegin(this));
    for(size_t i = 0; i < other.size + 1; ++i)
    {
        pointers[i] = std::make_unique<node>(*other.pointers[i]);
    }
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::inner_node::operator=(
    const inner_node& other) -> inner_node&
{
    if(this != &other)
    {
        std::copy(kbegin(&other), kend(&other), kbegin(this));
        for(size_t i = 0; i < other.size + 1; ++i)
        {
            pointers[i] = std::make_unique<node>(*other.pointers[i]);
        }
        size = other.size;
    }
    return *this;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
void btree<Key, Value, Comp, PAGE_SIZE>::leaf_node::insert(uint32_t i,
                                                           const Key& key,
                                                           const Value& value)
{
    std::move_backward(kbegin(this) + i, kend(this), kend(this) + 1);
    std::move_backward(vbegin(this) + i, vend(this), vend(this) + 1);
    keys[i] = key;
    values[i] = value;
    ++size;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
void btree<Key, Value, Comp, PAGE_SIZE>::inner_node::insert(
    uint32_t i, const Key& key, std::unique_ptr<node> pointer)
{
    std::move_backward(kbegin(this) + i, kend(this), kend(this) + 1);
    std::move_backward(pbegin(this) + i + 1, pend(this), pend(this) + 1);
    keys[i] = key;
    pointers[i + 1] = std::move(pointer);
    ++size;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::leaf_node::remove(uint32_t i) -> Value
{
    Value value = values[i];
    std::move(kbegin(this) + i + 1, kend(this), kbegin(this) + i);
    std::move(vbegin(this) + i + 1, vend(this), vbegin(this) + i);
    --size;
    return value;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::inner_node::remove(uint32_t i)
    -> std::unique_ptr<node>
{
    auto pointer = std::move(pointers[i + 1]);
    std::move(kbegin(this) + i + 1, kend(this), kbegin(this) + i);
    std::move(pbegin(this) + i + 2, pend(this), pbegin(this) + i + 1);
    --size;
    return pointer;
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::leaf_node::split(leaf_node* s) -> Key
{
    uint32_t mid = size / 2;
    std::move(kbegin(this) + mid, kend(this), kbegin(s));
    std::move(vbegin(this) + mid, vend(this), vbegin(s));
    s->size = size - mid;
    size = mid;
    s->next = next;
    s->prev = this;
    next = s;
    if(s->next != nullptr)
    {
        s->next->prev = s;
    }
    return s->keys[0];
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
auto btree<Key, Value, Comp, PAGE_SIZE>::inner_node::split(inner_node* s) -> Key
{
    uint32_t mid = this->size / 2;

    std::move(kbegin(this) + mid + 1, kend(this), kbegin(s));
    std::move(pbegin(this) + mid + 1, pend(this), pbegin(s));
    s->size = size - mid - 1;
    size = mid;

    return keys[size];
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
void btree<Key, Value, Comp, PAGE_SIZE>::leaf_node::share_to(leaf_node* s,
                                                             Key* p_key,
                                                             uint32_t to_pass,
                                                             bool no_merge)
{
    bool is_merge = to_pass == size;
    if((to_pass = std::min({to_pass, N_KEYS - s->size - 1, size})) > 0)
    {
        std::move_backward(kbegin(s), kend(s), kend(s) + to_pass);
        std::move_backward(vbegin(s), vend(s), vend(s) + to_pass);
        std::move(kend(this) - to_pass, kend(this), kbegin(s));
        std::move(vend(this) - to_pass, vend(this), vbegin(s));
        size -= to_pass;
        s->size += to_pass;
        if(!no_merge && is_merge)
        {
            next = nullptr;
            s->prev = prev;
            prev = nullptr;
            if(s->prev != nullptr)
            {
                s->prev->next = s;
            }
        }
        *p_key = s->keys[0];
    }
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
void btree<Key, Value, Comp, PAGE_SIZE>::inner_node::share_to(inner_node* s,
                                                              Key* p_key,
                                                              uint32_t to_pass,
                                                              bool no_merge)
{
    if((to_pass = std::min({to_pass, N_KEYS - s->size - 1, size})) > 0)
    {
        bool is_merge =
            !no_merge && size == to_pass && s->size + to_pass < N_KEYS - 1;
        keys[size] = *p_key;
        *p_key = keys[size - to_pass];

        std::move_backward(kbegin(s), kend(s), kend(s) + to_pass + is_merge);
        std::move_backward(pbegin(s), pend(s), pend(s) + to_pass + is_merge);
        std::move(kend(this) + 1 - to_pass - is_merge, kend(this) + 1,
                  kbegin(s));
        std::move(pend(this) - to_pass - is_merge, pend(this), pbegin(s));

        size -= to_pass;
        s->size += to_pass + is_merge;
    }
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
void btree<Key, Value, Comp, PAGE_SIZE>::leaf_node::share_from(leaf_node* s,
                                                               Key* p_key,
                                                               uint32_t to_pass,
                                                               bool no_merge)
{
    bool is_merge = to_pass == s->size;
    if((to_pass = std::min({to_pass, N_KEYS - size - 1, s->size})) > 0)
    {
        std::move(kbegin(s), kbegin(s) + to_pass, kend(this));
        std::move(vbegin(s), vbegin(s) + to_pass, vend(this));
        std::move(kbegin(s) + to_pass, kend(s), kbegin(s));
        std::move(vbegin(s) + to_pass, vend(s), vbegin(s));
        size += to_pass;
        s->size -= to_pass;
        if(!no_merge && is_merge)
        {
            next = s->next;
            s->next = nullptr;
            s->prev = nullptr;
            if(next != nullptr)
            {
                next->prev = this;
            }
        }
        *p_key = s->keys[0];
    }
}

template<typename Key, typename Value, typename Comp, size_t PAGE_SIZE>
void btree<Key, Value, Comp, PAGE_SIZE>::inner_node::share_from(
    inner_node* s, Key* p_key, uint32_t to_pass, bool no_merge)
{
    if((to_pass = std::min({to_pass, N_KEYS - size - 1, s->size})) > 0)
    {
        bool is_merge =
            !no_merge && s->size == to_pass && size + to_pass < N_KEYS - 1;
        keys[size] = *p_key;
        *p_key = s->keys[to_pass - 1];

        std::move(kbegin(s), kbegin(s) + to_pass - 1 + is_merge,
                  kend(this) + 1);
        std::move(pbegin(s), pbegin(s) + to_pass + is_merge, pend(this));
        std::move(kbegin(s) + to_pass, kend(s), kbegin(s));
        std::move(pbegin(s) + to_pass + is_merge, pend(s), pbegin(s));

        size += to_pass + is_merge;
        s->size -= to_pass;
    }
}

} // namespace tgraph_reachability::detail
