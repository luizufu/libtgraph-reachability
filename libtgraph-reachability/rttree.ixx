#include <vector>
#include <algorithm>

namespace tgraph_reachability
{
template<typename T>
rttree<T>::rttree()
{
#ifdef BENCHMARK_ON
    total_stack_usage += 20;
#endif
}

template<typename T>
rttree<T>::~rttree()
{
#ifdef BENCHMARK_ON
    total_heap_usage -= (_tree.size() * sizeof(uint32_t) * 3);
#endif
}

template<typename T>
auto rttree<T>::insert(interval interv, T elem) -> bool
{
    if(auto range = none_contained_by_(interv))
    {
        if(range->first != range->second)
        {
            remove_range(*range);
        }
        return insert_(interv, elem);
    }

    return false;
}

template<typename T>
auto rttree<T>::insert_(interval interv, T elem) -> bool
{
#ifdef BENCHMARK_ON
    ++total_binary_searches;
    ++total_inserts;
#endif
    return _tree.insert(interv, elem);
}

template<typename T>
auto rttree<T>::none_contained_by_(interval interv)
    -> std::optional<std::pair<const_iterator, const_iterator>>
{
    if(empty())
    {
        return std::make_pair(_tree.end(), _tree.end());
    }

    auto it1 = find_prev_(interv.right);
    if(it1 != _tree.end() && interv.left <= it1->first.left)
    {
        return {};
    }

    auto it2 = find_next_(interv.left);
    if(it2 != _tree.end() && interv.right >= it2->first.right)
    {
        return {};
    }

    return std::make_pair(it1 == _tree.end()                ? _tree.begin()
                          : it1->first.right < interv.right ? std::next(it1)
                                                            : it1,
                          it2 == _tree.end()              ? it2
                          : it2->first.left > interv.left ? it2
                                                          : std::next(it2));
}

template<typename T>
auto rttree<T>::find_prev(uint32_t t) const
    -> std::optional<std::pair<interval, T>>
{
    auto it = find_prev_(t);
    return it != _tree.end() ? std::make_optional(*it) : std::nullopt;
}

template<typename T>
auto rttree<T>::find_next(uint32_t t) const
    -> std::optional<std::pair<interval, T>>
{
    auto it = find_next_(t);
    return it != _tree.end() ? std::make_optional(*it) : std::nullopt;
}

template<typename T>
auto rttree<T>::empty() const -> bool
{
    return _tree.size() == 0;
}

template<typename T>
auto rttree<T>::find_prev_(uint32_t t) const -> const_iterator
{
    interval key = {0, t};
    detail::by_arrival comp;

    auto ctx = _tree.find_leaf(_tree._root.get(), key, comp);
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    const auto* n = ctx.top().first;
    size_t n_i = ub(n->leaf, key, comp);
    return n_i == 0 ? const_iterator(nullptr, 0)
                    : const_iterator(&n->leaf, n_i - 1);
}

template<typename T>
auto rttree<T>::find_next_(uint32_t t) const -> const_iterator
{
    interval key = {t, 0};
    detail::by_departure comp;

    auto ctx = _tree.find_leaf(_tree._root.get(), key, comp);
#ifdef BENCHMARK_ON
    ++total_binary_searches;
#endif
    const auto* n = ctx.top().first;
    size_t n_i = lb(n->leaf, key, comp);

    return n_i < n->leaf.size ? const_iterator(&n->leaf, n_i)
                              : const_iterator(n->leaf.next, 0);
}

template<typename T>
void rttree<T>::remove_range(std::pair<const_iterator, const_iterator> range)
{
    auto first = range.first != _tree.end()
                     ? std::make_optional(range.first->first)
                     : std::nullopt;

    auto last = range.second != _tree.end()
                    ? std::make_optional(range.second->first)
                    : std::nullopt;

    if(first)
    {
#ifdef BENCHMARK_ON
        ++total_binary_searches;
#endif
        auto mid = _tree.split(*first);
        if(last)
        {
#ifdef BENCHMARK_ON
            ++total_binary_searches;
#endif
            _tree.join(mid.split(*last));
        }
    }
}

} // namespace tgraph_reachability
